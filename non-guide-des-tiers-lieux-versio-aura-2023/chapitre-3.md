# CHAPITRE 3 : CONSOLIDER ET PERENNISER
**EN CROISANT LES EXPERIENCES**

### PRATIQUES SENSEES POUR CHEVILLES OEUVRIERES

* Une fois votre communauté lancée, des services et événements établis, le ou les lieux investis, viennent se poser d’autres questions transversales. 
* Comment pérenniser la dynamique sans épuiser les membres actif·ves ? Comment ne pas réinventer l’eau chaude et s’appuyer sur des expériences existantes ?Pourquoi choisir (ou pas) un modèle ouvert pour ses actions et projets ?
* Des « morceaux choisis » de pratiques salutaires en Tiers-Lieux vous sont ici esquissées. À vous de les adapter, d’en chercher d’autres, de vous rapprocher de praticien·nes expérimentées, et de réseaux d’entraide locaux.


## IDENTIFIER DES ROLES
* L’animation joue un rôle central dans le processus des Tiers-Lieux. Afin de donner vie à une dynamique de Tiers-Lieux dans une communauté, il ne suffit pas de disposer d’un « mobilier design » entre quatre murs et d’une personne assurant les tâches administratives. Un Tiers-Lieux non animé est une coquille vide.
* Quelques rôles type sont détaillés après dans l’objectif de matérialiser comment l’animation de cette communauté peut avoir lieu. Ces rôles ne sont pas des fiches de poste : ce sont des compétences à repérer et développer au sein de la communauté, et de préférence à faire vivre de manière tournante.

Ces quelques rôles permettent de conscientiser les différentes tâches et interdépendances entre chacun·e. Ils invitent simplement à se placer dans un lieu, une communauté et un processus. 
**Redéfinissez-les librement** par rapport aux spécificités de votre groupe / lieu / structure

#### CONCIERGE
Le/la concierge met en relation les compétences, les ressources et les volontés de chacun·e au sein d’une communauté d’action. Il ou elle facilite et provoque les rencontres et les interactions. Pour un·e usager·e, la conciergerie permet l’appropriation progressive des compétences, outils, expériences et du réseau. En quelque sorte, elle accélère l’entrée en intimité à différents niveaux. Concierge tend à devenir un métier emblématique à l’ère du développement urbain et du passage à l’économie de la contribution.
-> https://movilab.org/wiki/La_conciergerie

#### FACILITATEUR·ICE
Le ou la facilitateur·ice s’inté-resse au lien entre la communauté et ses différents projets. Il ou elle n’a pas besoin d’être expert·e et pas besoin non plus d’une grande légitimité.
Proactif·ve, il ou elle prend l’initiative pour chercher et donner accès à l’information pertinente. Il ou elle pourvoit aux informations manquantes, anticipe l’essoufflement d’un groupe. Son rôle est critique pour maintenir les actions, les projets, ainsi que la bonne santé du groupe et des personnes. La conciergerie est le processus d’accueil et d’animation d’un Tiers-Lieu. 

#### DOCUMENTALISTE 
C’est une personne qui fait vivre la documentation, la rend accessible à celles et ceux qui sont en recherche d’informations. On peut même découper ce rôle en deux sous catégorie, Scribe qui se concentre uniquement sur les contenus, les traces, captations, images, son pour faire archives des moments vécus. Jardinier·ère, chargé·e d’actualiser le contenu existant, de le peaufiner, le détailler et l’adapter aux différentes époques et temporalités, et de faire connaître et dialoguer les différentes documentations entre elles.

#### FACTEUR·ICE
Ou colporteur·euse. C’est la personne relativement itinérante qui apporte les messages, les actualités des un·es et des autres. Il/elle permet de faire prendre du recul en apportant des morceaux d’autres endroits, d’autres horizons et tente de faire se mutualiser les pratiques et réflexions. Certainement un rôle modeste mais primordial dans la chaîne de circulation de l’information.

#### MAKEUR·EUSE
Aussi porteur·euse de projet, geeks, nerds, codeur·euses, hacker·euses. Ce sont les faiseur·euses qui donnent la matière et bidouillent à tout va pour passer de l’idée à la preuve de concept dans un Tiers-Lieu.

#### VEILLEUSE
La veilleuse est une structure relai d’information : médiatrice et animatrice de quartier, mais aussi et surtout une force de proposition. Elle s’inscrit au cœur de la vie d’un quartier pour soutenir les projets d’habitant·es, stimuler et habiliter les citoyen·nes à construire ensemble l’avenir de leur quartier. C’est un espace autonome détaché de l’image véhiculée par les services de la ville, mais en lien et en articulation avec eux.
-> https://laveilleuse.wordpress.com/

:::warning
**Aller plus loin :**
D’autres ressources sur les rôles :
* https://www.aequitaz.org/wp-content/uploads/2022/02/boiteoutils-roles-az_2021.pdf
* https://solutionslocales.fr/comment-organiser-starhawk/#Les_principes_de_laction_collective
:::

## DOCUMENTER

La documentation est l’art de produire, de sélectionner, de classifier, d’utiliser, puis de diffuser des documents. Plusieurs personnes sont en réalité impliquées tout au long du processus : il est important de savoir qui, où et quand ! Il est également primordial de veiller à faire vivre ce patrimoine informationnel commun pour que chaque membre de la communauté s’en saisisse.

> “Les paroles s’envolent les écrits restent” 

De par son objectif de transformation (des personnes, des organisations), la vie d’un Tiers-Lieux n’est pas sans rebondissements : après les grandes euphories que procure la phase de conception et de création, il vous arrivera sûrement de douter et de remettre en cause la manière dont le projet fonctionne et se déroule. C’est là que vous vous remercierez d’avoir pris le temps de retracer tout votre parcours sur des “parchemins”. Au lieu de vivre un effondrement de votre projet, vous serez alors libre de revenir en arrière et de choisir une ou plusieurs autres directions avez celles et ceux qui vous suivront.

> “ Le problème c’est qu’on ne s’y intéresse qu’après... Alors que l’on devrait commencer par ça.”

:::info
**Zoom sur :**
* Bien qu’elle englobe le plus souvent une démarche collective, la documentation part avant tout d’une démarche singulière et personnelle. Comme un journal de bord, votre documentation vous accompagne tout au long de vos projets en vous permettant (personnellement et collectivement) de constater à tout moment le chemin parcouru et les épreuves rencontrées.
:::

### POURQUOI DOCUMENTER ?
**Reprenons la DÉFINITION SOCIOLOGIQUE DU TIERS-LIEUX :**
>  “ Des individus sans liens hiérarchiques se rencontrent et administrent ensemble quelque chose. Ce quelque-chose peut être autant une recette de cuisine, un programme informatique qu’un texte de loi ! ”

Documenter et rendre disponibles les ressources peut permettre aux personnes de la communauté de se saisir réellement et graduellement des sujets qu’elles travaillent.
Pour agir en communauté apprenante, les actions de documentation sont donc indispensables.

> “ « Un Tiers-Lieux documenté ne meurt jamais. 
Avec votre documentation vous pouvez certes arriver à la fin d’un parcours mais sans pour autant avoir à remettre en cause ce que vous avez fait (et personne ne pourra vous le retirer) ! Actuellement nous vivons une période aisée pour celles et ceux qui veulent ouvrir des dits “Tiers-Lieux” (avec une politique publique où beaucoup de financements ont été mis sur la table). Mais sans démarche de documentation proposée, les premières fermetures de lieux vont faire beaucoup de dégâts: seuls celles et ceux qui auront pris soin de documenter leurs actions pourront sereinement tourner une page pour en écrire une autre.” - Yoann DURIAUX

-> https://movilab.org/wiki/Réflexions_sur_la_toxicité_et_les_Tiers_Lieux

### COMMENT DOCUMENTER ?
**IDENTIFIER QUI S’Y MET :**
Cibler des compétences en rédaction, synthèse, illustration, facilitation graphique, journalisme, montage audio... 

**CHOISIR DES OUTILS NUMÉRIQUES :**
Framapad, Hedgedoc, Nextcloud permettent d’éditer collectivement et stocker des documents partagés à tous·tes, Audacity est accessible pour le montage audio.

### ANCRER DES PRATIQUES :
**Au quotidien :**
* Partagez votre documentation - même imparfaite - cela pourra donner envie de mettre la main à la pâte.
* Ancrez ces pratiques dans une routine de réunions - prise de note graphique, numérique, captation audio...
* Recensez vos ressources dans une «gare centrale» sur votre site web ou rappelez-là dans vos échanges mails.

**Ponctuellement :**
* Réunissez vos compétences autour d’un sujet à documenter en 2h.
* Éditez un document papier à faire circuler dans votre communauté et au-delà.
* Organisez un Movicamp ou un DiscoDoc pour vous focaliser sur un temps de convivialité et de production !

:::info
**Zoom sur : MOVICAMP ET DISCODOC**
* Un MoviCamp est un format d’événement destiné à la fois à faire comprendre les enjeux de la documentation dans, par et avec les Tiers Lieux, et prototyper des formats de documentation en atelier. «Movi» étant la contraction de Movilab, et «Camp» celle de BarCamp, un format de co-création que l’on retrouve régulièrement dans l’univers du logiciel libre assimilable à une «non programmation». Ce format a été conçu et déployé en France entre 2010 et 2017 par l’équipe du Comptoir Numérique. Un MoviCamp peut se dérouler sur une ou plusieurs journées selon les attentes des participants. Il peut aussi être intégré dans un autre événement en tant que format d’animation.
-> https://movilab.org/wiki/MoviCamp 

* Un Discodoc est un format court et festif qui permet de produire des documents finalisés (utilisables et diffusables, révisables et enrichissables) autour d’une trame commune et personnalisable. Inspiré du DiscoSoupe, un Discodoc consiste à créer une trame d’animation festive de trois heures à deux jours pour produire des documentations utiles et qui donnent envie !
Organisez un Discodoc quand vous avez :
1. identifié des sujets critiques sur lesquels récolter des retours d’expériences
2. une idée des formats de documents à travailler.
-> https://movilab.org/wiki/DiscoDoc

Ces formats d’animation, faites-les vous-même avec les compétences de votre communauté, ou faites appel à une équipe expérimentée qui animera les ateliers et permettra de concrétiser les contenus souhaités!
:::

## IN VIVO DISCODOCS 2023 : DE ARLES A LYON EN PASSANT PAR LILLE.

Ces moments d’animation et de création de contenu collectif ont permis:
- De collecter des «souvenirs du houblon» sous forme d’un livret imprimé
- De réaliser un mur d’accueil du Tiers-Lieux associatif
- De formaliser des fiches projets à remplir par les membres
- De s’approprier la documentation sur Movilab au sein d’un collectif 

> “ Dans une vie professionnelle précédente j’ai animé une communauté de 28 pays sur les questions d’écologie. La documentation répondait à un besoin de partage d’expériences, pour permettre de répliquer les projets et chercher des financeurs. En automne dernier, bien ancrée en région PACA et à Sud Tiers-Lieux, je tombe sur une annonce de la Compagnie des Tiers-Lieux : « on cherche des contributeur.ices ». 
> Ça m’amène sur Movilab et je découvre les budgets contributifs. Quelques temps après j’en mettais en place à Sud Tiers-Lieux ! Pour moi, c’est une manière de repenser profondément le travail, en appuyant sur la reconnaissance mutuelle des apports et la liberté de contribution. 
> Sur Movilab, j’ai trouvé un mélange de retours d’expériences et de données précises, ça va au delà d’un support de présentation ! La mise en récit assez « cash » des explorations menées permet vraiment de comprendre comment des collectifs avancent sur des nouveaux sujets. 
> Changer la société à petite échelle, partager ce qui marche, ce qui frotte, de manière précise et sensible, ça ouvre un champ pour trouver des modèles équilibrés pour demain ! ” - Maïlis - Équipe Discodoc


## TIERS-LIEUX ET MODELES OUVERTS
**MOUVEMENT DU LIBRE ET DE L’OPEN SOURCE**

* Un logiciel Open Source utilise un code source conçu pour être accessible au public : n’importe qui peut voir, modifier et distribuer ce code source à sa convenance. 
* Le libre est devenu un mouvement à part entière et une méthode de travail qui dépasse la simple création de logiciels. Ce mouvement s’appuie aujourd’hui sur les pratiques du partage, de l’utilisation et de la modification libre, et sur un modèle de production décentralisé pour résoudre des problèmes sociaux, environnementaux, organisationnels, techniques, auxquels font face des communautés et secteurs d’activité.
* La philosophie du libre s’appuie sur des licences appliquées à des publications, qui concernent la création matérielle ou immatérielle, notamment les licences Creative Commons. Ces licences permettent de choisir le degré d’ouverture et d’utilisation par exemple la possibilité de commercialisation ou non, la nécessité de citer les auteurs, la possibilité de partager les modifications...
* Cette philosophie infuse dans les Tiers-Lieux, et s’applique soit à leur organisation globale, soit dans les projets développés et a donné lieu à l’appellation TILIOS (Tiers-Lieux Libres et Open Source). L’objectif est bien que les Tiers-Lieux soient des laboratoires de modes de vie durables et souhaitables, et le partage sous licence libre des projets développés permet dans ce cas la diffusion virale et adaptable (essaimage).


:::info
**Zoom sur : Without Model**

Actif entre 2012 et 2019, WithoutModel est un think tank collaboratif dont la mission est de contribuer à généraliser les modèles ouverts, collaboratifs et responsables à travers des publications, des événements et des méthodes.


* Ouverts : open data, open knowledge, open science, open source, ... Bref, tous ces modèles où on ne monetise pas l’actif, où on ne vend pas le contenu, qui construisent un bien commun.
* Collaboratifs : les modèles qui sont basés sur des relations en pair à pair (sans intermédiaire) auto organisés : consommation collaborative, production collaborative, crowdsourcing, open innovation. Certains de ces modèles se retrouvent aussi dans les modèles ouverts (ex : open source ou open manufacturing) mais tous les modèles collaboratifs ne contribuent pas à la construction d’un commun (ex : co-voiturage)
* Responsables : les modèles qui se donnent pour premier objectif la construction de valeur sociale en apportant à un défi sociétal (accès à l’eau, éducation...) et qui dans le même temps construisent leur activité autour de principes de création de valeur économique (des clients, des actionnaires, du chiffre d’affaires)

* Voir : 
    * https://www.withoutmodel.com/fr/recherches/
    * https://www.slideshare.net/WithoutModel/open-models-for-sustainability
:::


## IN VIVO : TILIOS CAMP

Fin juin 2023, la Plume à Loup, un lieu qui accueille des activités de coworking, café & cuisine partagée à Attin, héberge une rencontre TILIOS de plusieurs jours pour prendre la température de ce qui se joue actuellement dans l’univers Tiers-Lieux, refaire histoire et culture commune par le biais d’ateliers, débats, projection. Plusieurs productions sortent de ce temps fort: une feuille de route, un panel des rôles dans les communautés, le début d’un second manifeste, une visualisation d’archipel rattaché aux valeurs de TILIOS, ainsi qu’une chronologie d’événements rattachés à ce qui fait mouvement des Tiers-Lieux.

:::info
**Zoom sur : le mouvement TILIOS**
Le mouvement des **TI**ers-lieux **LI**bres & **O**pen **S**ource) vise à porter la culture du logiciel libre pour penser et agir à la fois dans les Tiers-Lieux et sur l’infrastructure nécessaire aux Tiers Lieux, qu’elle soit numérique ou non numérique. Il explore les atouts et limites de cette culture du libre appliquée à des lieux, et se confronte à d’autres cultures (architecturale, artistique, d’éducation populaire, économique, etc...) dans le but de faire tiers-lieux au plus près des enjeux de son temps.
-> Voir la page movilab : https://movilab.org/wiki/Tiers_Lieux_Libres_et_Open_Source
:::

:::warning
**Aller plus loin**
**RESSOURCES DU TILIOS CAMP 2023**
* https://movilab.org/wiki/Rencontre_Tilios_du_26_au_30_juin_à_La_Plume_à_Loup
* BZZZZ zine #1 & #2 via POC édition: https://www.helloasso.com/associations/poc-foundation/boutiques/poc-edition
* Manifeste V2 : https://pad.lamyne.org/manifeste-tilios-2023
* Chronologie située de ce qui fait mouvement Tiers-Lieux : https://tinyurl.com/464jyn5h
:::

## **FAIRE RESEAU** 

Pour aller plus loin et entrer en contact avec les membres de cet univers, l’une des premières portes d’entrée locale est le Réseau Régional. En région Rhône Alpes, il s’appelle Relief, pour le RÉseau des LIeux EFfervescent.

:::info
Pour contacter le réseau : contact@relief-aura.fr
:::

### Le fonctionnement de Relief
Le Réseau Relief est un réseau de Tiers-Lieux qui à la particularité de ne pas avoir de salarié.es. Il fonctionne avec une équipe de contributeur.ice.s impliquée dans les Tiers Lieux. Ces différentes contributions s'organisent en commissions thématiques de travail animées par deux copilotes et composés de contributeur.ice.s bénévoles ou rétribués. Ces commissions se réunissent au sein d'un comité de pilotage opérationnel tous les mois ou tous les deux mois.

:::info
**Zoom sur :**
Vous aussi, vous pouvez mettre en place un forum, un chat, une newsletter, pour permettre à votre communauté d’échanger plus facilement ! Il faudra alors être attentif·ve à l’architecture de l’information qui se construira au fur et à mesure, afin de permettre à chacun·e de retrouver ce qu’elle ou il cherche, d’être tenu·e au courant des actualités, sans se noyer dans trop de flux d’informations. En bref : soigner l’hygiène numérique et faire évoluer ses outils en fonction des besoins avérés, dans une dynamique de communauté apprenante.
-> Voir un exemple du système d’information et des outils sur le portail des outils du réseau Relief (portail.relief-aura.fr)
:::

### PORTES D'ENTREES DU RESEAU
Pour en savoir un peu plus sur le réseau et rejoindre la dynamique différentes portes d’entrée sont possible :
* **La lettre d’information :**
Elle permet de se tenir au courant des actualités et des actions proposées par le réseau, pour s’inscrire, c’est ici :
https://mailchi.mp/a45d3cc2a4cc/inscription-la-newsletter

* **Le site internet** *(en construction - nov 2023)*
La future vitrine du réseau, pour comprendre comment ça marche, à quoi il sert et bien plus :
relief-aura.fr

* **Le chat**
Un espace en ligne pour discuter, échanger, demander, rencontrer des praticien.nes de la région :
https://chat.tiers-lieux.org/channel/RELIEF_reseau_TL_AuRA

* **Le portail des outils**
Pour accéder, comprendre et prendre en main les outils déployés et commencer à contribuer au réseau :
portail.relief-aura.fr

* **Les réunions d’accueil :**
Tout les mois, un session d’accueil est organisée en ligne afin de souhaiter la bienvenue au nouvelles et nouveaux et de répondre au question : 
https://airtable.com/appAtaiMZGaORPTAk/shrwhjVpSUAY1ocJU

* **Les formations et autres évènements :**
Différentes actions sont aussi organisées en physique sur la région comme des formations, des séminaires… Une bonne occasion pour rencontrer celles et ceux qui font vivre le réseau et rejoindre l’équipe !



### D'AUTRES RESEAUX :
**Quelques autres communautés et ressources ami.es à même de vous accompagner dans votre projet sur votre territoire.**

#### Les réseaux départementaux
* En Isère - Cotlico
https://fr.linkedin.com/in/cotlico-r%C3%A9seau-des-tiers-lieux-de-l-is%C3%A8re-6a3029294
* En Allier - Cerf
https://tierslieux-allier.fr/
* En Drôme - Cédille
https://www.cedille.pro/
* En Ardèche - la Trame
https://latrame07.fr/

#### Les réseaux thématiques
* Le réseau des Tiers-Lieux à but non lucratif - Les Tiers-Lieuses
https://les-tiers-lieuses.org/

* Les lieux intermédiaire et indépendants
http://cnlii.org/
https://autresparts.org/

* Le Réseau Français des Fablab – RFFLabs
https://rfflabs.fr/

* Le réseaux des Centres Sociaux locaux
https://auracs.centres-sociaux.fr/


#### Les acteur.ices public.ques
* La Chambre d’Economie Sociale et Solidaire de la région - Cress Aura
https://www.cress-aura.org/

* Le Dispositif Local d'Accompagnement de l'ess - DLA AUra
https://www.info-dla.fr/coordonnees/auvergne-rhone-alpes/

#### Les initiatives et ressources locales

* L'Association pour le Développement en Réseau des Territoires et des Services - L'ADRETS
https://adrets-asso.fr/?Accueil

* Le Hub d’Inclusion Numérique local - Hinaura
https://www.hinaura.fr/q

* L'association pour la formation et l'éducation populaire - CREFAD
https://www.crefadauvergne.org/

* La pépinière des Tiers-Lieux Ruraux Aura - Bardane
https://bardane.org/pepiniere-tiers-lieux-ruraux-aura-bfc/

* La coopérative foncière immobilière , rurale et solidaire - Village Vivants
https://villagesvivants.com/

* Familles Rurales
https://www.famillesrurales.org/loire/outre-mer/loire/outre-mer/loire/FamillesRurales-developpement-tiers-lieux-dynamismeterritoires

* APf France Handicap
https://www.apf-francehandicap.org/



