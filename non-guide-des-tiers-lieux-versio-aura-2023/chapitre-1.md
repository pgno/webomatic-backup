# CHAPITRE 1 : JE DECOUVRE LES TIERS-LIEUX
***VOUS AVEZ-DIT «TIERS-LIEUX» ?***

### OU JE METS LES PIEDS ?

La principale ambiguïté lorsque l’on parle de Tiers-Lieux, c’est que les Tiers-Lieux ne sont pas forcément un lieu au sens « quatre murs et un toit » ! Un Tiers-Lieux ne possède pas UNE définition, ni une recette magique, et c’est tant mieux car c’est à chacun·e de s’approprier ce drôle de concept.

## POUR COMMENCER
*Entendu ici et là*
> "Une nouvelle famille !”
> “Des rencontres improbables à cheval entre ma maison et mon travail.”
> “Un refuge pour nerds, geeks, hackers, et individus hors normes.”
> “Un lieu pas cher et cool pour créer ma startup”
> “Des compétences au service de mon super projet”
> “La MJC à l’ère d’internet”...

* Le Tiers-Lieux est - et doit rester - avant tout un processus humain, en lien avec des outils (technologiques ou non) au service de la communauté. Les intéractions et le processus de co-création vont permettre de contribuer, brique après brique, à un patrimoine informationnel commun réutilisable par l’ensemble des contributeur·ices et au-delà.
* Au même titre que nous avons vu arriver les ordinateurs il y a une trentaine d’années dans notre environnement socioprofessionnel, le Tiers-Lieux devient peu à peu un “méta-outil” transversal qui nous permet de travailler, vivre et entreprendre autrement... à l’ère du numérique !
* Cela signifie de passer individuellement au travers de différents stades : 1/ Je viens pousser la porte, je visite, je regarde, 2/ Je fabrique quelque chose, 3/ Je contribue : je range, documente, explique, enseigne, partage 4/ J’organise et je gère.
* On parle de communauté apprenante. On y prône l’entraide, la transmission, la libre et égale participation. Et pour autant, on peut mieux faire ! La moitié des Fablabs accueille au moins 75% d’hommes cisgenre. 


## DEFINITIONS :
**LA DÉFINITION VUE PAR LE SOCIOLOGUE :**

“ Le Tiers-Lieux désigne explicitement, et par un simple mot, une situation en somme toute assez ordinaire : plusieurs personnes indépendantes les unes des autres se rencontrent pour concevoir et administrer ensemble quelque chose – qu’il s’agisse d’une recette de cuisine, d’un service informatique ou d’un texte de loi. ” - *Antoine BURRET, Étude de la configuration en Tiers-Lieux -
La repolitisation par le service, 2017.

> “ Le « Tiers-Lieu(x) » est une « configuration sociale » qui se matérialise - le plus souvent par un « lieu physique et/ou numérique » dans lequel est activé par l’action du « concierge » (ou facilitateur) un « processus » permettant à des « personnes venues d’univers différents » - voire contradictoires - de se rencontrer, se parler et créer ainsi un « langage commun » leur permettant de réaliser ensemble des projets .”

**L’INTENTION DES PIONNIERS :**
“ Créer de l’abondance (intelligence) là où il y a de la rareté (matière). ”

**DU CÔTÉ DES PRATICIENS :**
“ Le Tiers-Lieux au final ce n’est qu’un outil. Le Tiers-Lieux en lui-même n’apporte pas de solution. Il apporte juste la possibilité aux gens de s’approprier ces nouveaux modèles et d’essayer d’en faire quelque chose de positif. ” – Pierre TRENDEL, Co-fondateur du Mutualab à Lille.

**DU CÔTÉ DES DESIGNER·EUSES :**
“ Les Tiers-Lieux sont des promesses aux-quelles travaillent et vivent des individus, aventuriers d’une nouvelle forme de partage. Ils sont les enfants de Linux, du logiciel libre et de l’open source mais aussi des écoles de design, des libertariens, des “startupers” mais aussi des “punks à chiens”, des anarchistes. Ils sont l’hybridation des marges technologiques et des marges sociales, là où s’inventent sans cesse des organisations du collectif et par extension les nouvelles organisations du travail... ” – Olivier PEYRICOT, Directeur Scientifique de la Biennale de Design et Directeur du Pôle Recherche de la Cité Du Design, Déc. 2016 [https://world-trust-foundation.gitbook.io/fork-the-world/intro/intention

:::warning
**Aller Plus loin :**

* le «Repère» régional des Tiers-Lieux de Centre Val de Loire 
 https://www.cresscentre.org/wp-content/uploads/2022/02/Reperes-TIERS-LIEUX-web-1.pdf
* Comprendre les Tiers-Lieux (EU) : https://pourlasolidarite.eu/sites/default/files/cck-news-files/tiers-lieux_mertens_22_11_2022_conference.pdf
:::

:::info
**Zoom sur :**

* Le concept de Tiers-Lieux s'inspire de celui de "Tiers Paysage"
Notion créée par le paysagiste français Gilles Clément pour désigner l’ensemble des espaces qui, négligés ou inexploités par l’homme, présentent davantage de richesses naturelles sur le plan de la biodiversité que les espaces sylvicoles et agricoles. Dans son livre, il le situe dans le prolongementde ses théories du jardin en mouvement et du jardin planétaire.

> “ Le Tiers-Paysage – fragment indécidé du Jardin Planétaire – désigne la somme des espaces où l’homme abandonne l’évolution du paysage à la seule nature.” – Gilles CLÉMENT, Manifeste pour le Tiers paysage

* Les portions de territoire relevant du Tiers paysage (concept issu du travail de Gilles Clément) – « friches, marais, landes, tourbières, mais aussi les bords de route, rives, talus de voies ferrées, etc.» – sont, en effet, également en marge mais présentent en réalité une diversité biologique bien supérieure aux espaces davantage soumis à l’anthropisation, en d’autres termes : plus entretenus et maîtrisés par les activités humaines, comme le sont actuellement les champs et les forêts.
* Le Tiers-Lieux s’inscrit ainsi dans une dynamique de réappropriation d’espaces (urbains, ruraux...), et permet à minima des rencontres plus riches et variées que partout ailleurs. Mais on l’espère aussi fertile pour y voir émerger de nouvelles idées, y développer des activités, des projets, qui pourront s’inscrire ensuite plus largement dans le faire société autrement.
:::

:::warning
**Aller plus loin :**

* Hyperliens, la websérie de l’ANCT pour découvrir des Tiers-Lieux inspirants dans toute la France : action sociale, alimentation, santé, ruralité, numérique, culture, etc. Voir également les podcasts.
-> https://tierslieux.anct.gouv.fr/fr/videos/
-> https://tierslieux.anct.gouv.fr/fr/podcast/

* Movilab, le wiki des Tiers-Lieux. Des centaines d’articles en libre accès, et bientôt vos propres contributions à la communauté !
-> https://movilab.org/wiki/Accueil
:::

### Passer à la pratique
* Rien de mieux que d’entrer dans la pratique pour ancrer les concepts - parfois complexes - du «faire Tiers-Lieux». Découvrez l’Atelier Terminologie et chronologie des Tiers-Lieux sur Movilab. Retracez collectivement les trajectoires du mouvement dit « Tiers-Lieux » : les participant·es - initié·es ou moins - établissent les filiations et chronologies d’apparition des différents concepts et mouvements pré-existants ou liés.
Retrouvez la recette d’animation complète ici :
-> https://movilab.org/wiki/Fresque_des_Tiers-Lieux

    
## INCLUSIVITE ET ACCUEIL
**QUELQUES PROPOSITIONS À METTRE EN ŒUVRE POUR L’ACCUEIL DU PUBLIC EN RESPECT DES INDIVIDUALITÉS**
* Avoir une équipe diversifiée, incluant des personnes de tous genres. 
    * La représentation et l’inspiration sont les leviers de «l’effet scully» (les femmes qui ont regardé X-Files ont 50% plus de chance de travailler dans le domaine des sciences et de la techniques). 
* Lors de l’accueil de toute nouvelle personne, présenter le lieu de la même façon et ne pas présumer du niveau de compétence des visiteur·ses.
* Verbaliser les règles explicites de vie en collectivité pour pouvoir agir en cas de comportement discriminatoire et oppressif. 
* Mettre à disposition un espace d’expression assez bienveillant pour que tous lesporteurs et porteuses de projets - quelque soit leur genre ou origine ethnique - se sentent accepté·es, accompagné·es et valorisé·es dansleur projet.
* Si possible, prévoir un espace sécurisé et ludique pour les enfants (avec des jeux fabriqués au fablab?), ou des partenariats avec lieux de garde, pour permettre l’inclusion des parents.
* Éviter les cases “sexe” ou “genre” dans les questionnaires et formulaires, sinon prévoir une case “ne préfère pas répondre”.

:::warning
**Aller plus loin :**
* Découvrez et affichez la chartede bienvenu·e·s MakeHerSpace et sa version sur Movilab
-> https://www.makeherspace.fr/manuel-pratique-inclusion-genre/
-> https://movilab.org/wiki/Manuel_MakeHerSpace

* Se former à l’usage des pronoms :
-> https://wikitrans.co/2019/02/25/quels-pronoms-utiliser-pour-une-personne-quand-je-ne-suis-pas-sure/
:::