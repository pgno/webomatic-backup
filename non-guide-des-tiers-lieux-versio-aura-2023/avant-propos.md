Ce document est une œuvre collective et itérative. L’ambition est de faire évoluer ce guide à chaque édition en lien avec des praticien·nes en Tiers Lieux et des réseaux ou collectifs. Un premier travail a été soutenu en 2022 par la région du Centre Val-de-Loire dans le cadre du réseau de préfiguration “Ambition Tiers-Lieux”, puis une première édition est parue par ce réseau en juin 2023. L’édition que vous lisez (AURA - déc. 2023) est assemblée et retravaillée en lien entre autre avec des contributeur·ices du réseau Relief.

## Pourquoi ce guide ?
Ce guide s’adresse aux porteuses et porteurs de projet, aux contributrices et contributeurs, curieuses et curieux de l’action en Tiers-Lieux. Il transmet des modes d’actions qui font la culture du mouvement, et pointe des pièges à éviter. Ce guide partage des méthodes qui ont fait leur preuves, et aiguille vers différentes ressources. Ce guide se découpe en trois chapitres. Ceux-ci donnent à voir l’émergence de projet de Tiers-Lieux la consolidation du modèle socio-économique et l’hybridation de modèles d’action sur le territoire. Ce guide assemble des ressources issues à la fois de plateformes comme Movilab, autant que des informations locales glanées auprès d’acteur·ices et de réseaux.

:::info
**Zoom sur :**
La partie émergée de Movilab est un centre de ressources numérique, fait par les pratiquant·es de communautés apprenantes, des Tiers-Lieux aux communs de territoires. Chacun·e peut en modifier le contenu, sur le principe du wiki.
:::

Historique de Movilab : https://movilab.org/wiki/Historique_de_Movilab

## Notice d'utilisation de ce livret
***Bienvenue en Tiers-Lieux !***

> Quelles sont les différentes réalités derrière ce concept que l’on croise maintenant un peu partout ? Et vous, que venez-vous y chercher ?

* Ce guide est pensé comme une histoire dont vous êtes l’héroïne ou le héro. Comme dans les dynamiques en Tiers-Lieux, plusieurs portes correspondent à vos envies, besoins et attentes ! 
* Vous pouvez donc parcourir ce guide comme vous le souhaitez, de manière linéaire, par chapitre, en butinant par thématique, ou en suivant les redirections proposées tout au long du document.
* Ce guide vous invite aussi à explorer au-delà de ce simple livret en suivant les ressources numériques référencées.