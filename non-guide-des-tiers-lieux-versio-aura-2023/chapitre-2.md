# CHAPITRE 2 : SE LANCER DANS L'AVENTURE 
**S'ENTOURER POUR CHEMINER**

### INGREDIENTS CLES
* Le mouvement des Tiers-Lieux s’appuie sur des manières de faire défrichées par d’autres mouvements, en adapte certaines à ses besoins, et en invente de nouvelles.
* Année après année, des cultures Tiers-Lieux émergent et se consolident ainsi. Vous pouvez piocher ici et là “des recettes de cuisine” et diverses documentations qui font partie de ce patrimoine informationnel commun.

## DES RENCONTRES
**ET CROISEMENTS IMPROMPTUS**
* Les rencontres sont un préalable à un projet de lieu, afin de travailler ce qui deviendra la communauté.
* Les lieux ancrent la communauté et le faire ensemble, et permettent de développer les activités souhaitées. 
* Les services sont le point d’entrée de futur·es pratiquant·es du Tiers-Lieux : la part émergée de l’iceberg, et aussi un des moyens de contribuer à l’économie de l’ensemble.

* Une ou plusieurs identités peuvent ressortir de l’articulation des rencontres et services mis en place sur le lieu. Ces identités forment le socle de la communauté des utilisateur·ices et contri- buteur·ices, qui se reconnaîtront des dynamiques sociales créées, et donc de la manière de faire Tiers-Lieux, en dehors de l’aménagement du lieu lui-même.


#### DISCO SOUPE :
Les Disco Soupe sont des moments de cuisine participatifs festifs utilisant des fruits et légumes invendus. Les soupes, salades, jus de fruits ou smoothies ainsi fabriqués sont proposés au public gratuitement ou à prix libre. Les Disco Soupe permettent l’éducation à une cuisine saine, goûteuse et anti-gaspi, dans une ambiance conviviale.
-> https://movilab.org/wiki/Disco_Soupe

#### BARCAMP :
Un BarCamp est une rencontre, une non- conférence ouverte qui prend la forme d’ateliers-événements où le contenu est proposé par les participant·es.
-> https://movilab.org/wiki/BarCamp


#### OPEN BIDOUILLE CAMP:
L’Open Bidouille Camp est une expérience ouverte d’ateliers de bidouille en tout genre (prototypage, réparation, création...), animée par des acteurs locaux. C’est un évènement gratuit pour les visiteur·ices et accueillant des publics non initiés. Il permet de découvrir des savoirs- faire d’utilité sociale et environnementale tout en s’amusant.

#### RENCONTRES/ATELIERS:
Différents outils permettent d’animer des rencontres en fonction du public visé et des objectifs. Le savoir-faire d’animateur·ice se développe en faisant, et permet aux participant·es d’agir dans un cadre motivant et serein. Des formations d’intelligence collective permettent d’aller plus loin.
-> https://www.openbidouille.net/Faire-un-Open-Bidouille-Camp/

### Proposition de regles d'animation de rencontres

* Chacun·e a le droit de penser ce qu’il pense et de l’exprimer.
* Non jugement et bienveillance. Personne n’a raison, chacun·e possède un point de vue situé : souveraineté individuelle.
* Viser une prise de parole concise et claire centrée sur le sujet abordé.
* Vigilance, prise en compte des inégalités de situation, de perception, d’expression...
* Les personnes présentes sont les bonnes personnes, au bon endroit, au bon moment. Si l’on n’apporte plus rien ou n’apprend plus rien, on est libre de quitter la rencontre.
* Principe de désaccord fécond : il ne s’agit pas d’écraser les différences mais de construire la richesse issue de ces points de vue différents (trouver une troisième voie). 
* Co-responsabilité : Le/la facilitateur·ice propose un processus et une direction mais le voyage se fait ensemble et on peut revoir les étapes, créer des détours...

:::warning
**Aller plus loin :**
* Organiser un hackathon
https://movilab.org/wiki/Recette_frugale_d’hackathon_citoyen_open_source:_en_32_jours_et_sans_budget
* Organiser des portes ouvertes
https://movilab.org/wiki/Kit_pour_réaliser_des_portes_ouvertes_de_tiers-lieux
* Organiser un évènement
https://movilab.org/wiki/Organiser_un_événement_en_2.0
* Animer des temps collectifs à distance
https://movilab.org/wiki/Petit_guide_d%E2%80%99organisation_et_d%E2%80%99animation_d%E2%80%99une_r%C3%A9sidence_contributive_de_travail_%C3%A0_distance
* D’autres formats d’animation
https://movilab.org/wiki/Catégorie:Formats_d’animation
:::

## DES SERVICES POUR DES BESOINS
**UNE APPROCHE SYSTEMIQUE**

La systémique est une manière de définir, étudier ou expliquer tout type de phénomène, elle consiste avant tout à considérer ce phénomène comme un système : un ensemble complexe d’interactions, souvent entre sous-systèmes, le tout au sein d’un système plus grand. 
Le système représenté par le Tiers-Lieu est donc un ensemble de coopérations fertiles qu’il va pouvoir tisser à l’intérieur et à l’extérieur de ses murs. De ce fait, le modèle économique des activités que le Tiers-Lieux accueille est à la fois bénéficiaire et contributeur de la dynamique micro-locale engagée. Les activités naissent de l’initiative citoyenne et se nourrissent de l’émulation collective.

### Quelques exemples :

#### MATÉRIAUTHÈQUE RECYCLERIE RESSOURCERIE :
La matériauthèque et la recyclerie ont pour vocation de récupérer, valoriser et/ou réparer, respectivement des matériaux et des objets d’occasion, en vue de la revente au grand public. Ils sont contrôlés, nettoyés, réparés, avant d’être mis en rayon pour la vente. Le terme Ressourcerie® est une marque déposée. Les déchets collectés (encombrants, Déchet Industriel Banal (DIB)...), sont prioritairement par réemployés ou réutilisés, sinon recyclés. > Voir la ressourcerie culturelle :
-> https://www.laressourcerieculturelle.com/

#### ESPACE DE PRATIQUES NUMÉRIQUES :
L’Espace de Pratiques Numériques donne accès à des équipements et à un accompagnement aux usages numériques. Évolution de l’Espace Public Numérique (“EPN 2.0”), il organise des ateliers autour des Technologies de l’Information et de la Communication (TIC) en mettant les projets au cœur du processus. Différentes structures peuvent développer ce service: Centre Social, Médiathèque, Bibliothèque, Association, Centre de formation... Voir aussi la déclinaison en EPNCamp.
-> https://movilab.org/wiki/Espace_de_Pratiques_Numériques
-> https://movilab.org/wiki/EpnCamp 

#### HACKERSPACE & MAKERSPACE :
Le hacking est l’art et la manière de modifier un élément physique ou virtuel de sorte à ce que celui-ci n’ait pas le comportement prévu initialement par ses créateur·ices. Un HackerSpace rassemble des passionné·es lors d’ateliers où chacun·e est tour-à-tour apprenant·e ou transmetteur·ice, dans l’esprit «école mutuelle». Le Makerspace ajoute la dimension fabrication et prototypage. Il se décline en Fablab, Repair Café, et permet via ces activités de (re)développer des savoir-faire artisanaux, au moyen de technologies maîtrisables (petit à petit) par les usager·es du lieu.
-> https://movilab.org/wiki/HackerSpace
-> https://movilab.org/wiki/Fablab_-_Laboratoire_de_fabrication
-> https://movilab.org/wiki/Repair’Café

### LAVERIE MUTUALISÉE
En 2020 à Lyon, La Washerie© réinvente la laverie de quartier en proposant de laver ou faire laver son linge dans un lieu agréable et convivial et solidaire (principe de la « lessive suspendue »). C’est une entreprise de service attentive à son impact environnemental, pour toutes celles et ceux qui souhaitent mettre en accord leurs principes et leur mode de vie.
-> https://www.facebook.com/lawasherie

#### RÉSIDENCES ARTISTIQUES
Le Chauffoir à Chateauroux mutualise la capacité de travail de deux compagnies et développe la transmission, l’échange de savoirs et l’accompagnement de projets. Ouvert à différentes pratiques artistiques telles que le théâtre, la musique, la danse, le Chauffoir permet l’organisation d’Ateliers de Pratiques Artistiques, de stages et de rencontres thématiques.
-> https://www.lechauffoir.com/

#### JARDIN PARTAGÉ
Le jardin communautaire partagé se veut avant tout être un espace urbain de rencontre et de partage. Généralement géré par un groupe d’habitant·es, il offre à la fois la possibilité de jardiner comme de créer des liens sociaux. Véritable bouffée d’air frais en plein cœur de la vie citadine, il est également parfois un lieu de balade ou d’événements locaux.
https://demarchesadministratives.fr/demarches/mettre-en-place-un-jardin-communautaire-partage

:::warning
**Aller plus loin**
*Au delà des services, il s'agit d'aborder la question politique des Tiers-Lieux*
* Étude de la configuration en Tiers-Lieu - La repolitisation par le service :
-> https://movilab.org/wiki/Etude_de_la_configuration_en_Tiers-Lieu_-_La_repolitisation_par_le_service
:::


## AGIR EN COMMUN
Un autre concept revient aussi souvent dans cet univers Tiers-Lieux, celui du “Commun”.

* Les Communs ne sont pas des ressources (on parle sinon de “biens communs”). Le Commun est le partage de ces ressources, gérées et maintenues collectivement par une communauté ; celle-ci établit des règles dans le but de préserver et pérenniser ces ressources. Elles peuvent être naturelles (une forêt, une rivière), matérielles (une machine-outil, une maison, une centrale électrique) ou immatérielles (une connaissance, un logiciel). La gestion “en commun” représente donc une troisième voie, complémentaire à la gestion publique et à la gestion privée.
* Agir en commun dans les Tiers-Lieux permet de mutualiser à la fois les ressources et les efforts pour les maintenir ou les développer. Cela demande un changement d’état d’esprit et un accompagnement auprès des utilisateur·ices afin que celles ou ceux-ci puissent petit à petit devenir contributeur·ices des ressourcescommunes.
* Agir en commun permet donc de pérenniser dans le temps, par l’action collective et l’engagement individuel.

:::info
**Zoom sur :**
* La démarche requestionne et replace ainsi les relations de travail, en redonnant aux « commoneur·euses » une capacité à expérimenter, en articulant les enjeux ...
* À mi-chemin entre le commun des mortels et l’expert·e, entre l’amateur·ice et le/la professionnel·le, entre l’ignorant·e et le/la sachant·e, entre le/la citoyen·ne et la famille politique, chacun·e va pouvoir prendre une place dans l’arborescence de la communauté. 
* Le pas de côté ? Chaque participant·e se place ainsi dans une posture apprenante propre et située, qui évolue dans le temps en fonction de là où iel en est.
* Finis les organigrammes figés, la priorité est donnée à l’alchimie collective-individuelle dans ces trajectoires d’apprentissage!
:::



## UN LIEU POUR FAIRE
**Ensemble ou à côté !**

Une problématique semble ressurgir des expériences actuelles :
> COMMENT PASSER D'UN PROJET PILOTE MARGINALISÉ À UN PROCESSUS INTÉGRÉ DANS LES POLITIQUES D’URBANISME ?

* La sécurisation du « terrain » d’un projet est l’un des aspects sensibles du développement communautaire. De la location à un propriétaire public ou privé, à l’achat d’un terrain par un membre ou par la communauté comme propriété indivisible, en passant par l’occupation temporaire d’un espace inutilisé ou d’une zone d’intérêt... De chaque type d’option choisie découle une orientation des usages par la communauté. Chacun·e doit donc être au fait de comment est gérée la propriété du lieu où il ou elle s’investit. 
* Il est aussi possible de développer une/des activités pour “faire Tiers-Lieu” en lien avec une/des structures existantes en logique partenariale pour accéder à un/des espaces, relevant du « donnant-donnant ». L’action développée amène ainsi de la valeur via la communauté et ce qu’elle met en œuvre (dynamisation, services, rencontres...). Selon la structure partenaire, la logique de développement mutuel peut nécessiter la formalisation via un cadre de réciprocité, outre une éventuelle convention d’utilisation de locaux.

:::info
**Zoom sur :**
Pour sécuriser un bien, trois actions peuvent être entreprises collectivement :
* Réunir les fonds pour l’achat et la rénovation d’un bien
* Modéliser un statut juridique qui garantisse une propriété et/ou un usage collectif du terrain.
* Construire un modèle économique qui intègre pleinement la gestion du foncier comme Commun
:::

:::warning
**Aller plus loin :**
* Le guide «MURSPORTEURS» de la CRESS des Pays de la Loire et porté par le collectif Cap Tiers-Lieux :
-> https://www.cap-tierslieux.org/wp-content/uploads/2023/02/GUIDE_MURS_PORTEURS.pdf
* Guide pratique Foncier de la CRESS Centre-Val de Loire :
https://www.cresscentre.org/wp-content/uploads/2022/11/CRESS-guide-Foncier-18x24cm-web-2.pdf
:::


### Accéder au foncier

* Les acteurs publics et privés qui s’occupent des politiques de la ville ne sont pas nécessairement en lien avec les habitant·es, et la fabrique de la ville est maintenue dans une complexité difficile à s’approprier.
* La gentrification est souvent favorisée (consciemment ou non), par le souhait de rentabilisation commerciale d’espaces «attractifs». L’intérêt récent pour «l’urbanisme transitoire» en est aussi malheureusement souvent aussi une manifestation.
* Du côté habitant·es, différentes initiatives tentent de mettre en place d’autres manières de vivre, de s’organiser, luttant pour inventer et maintenir des modes d’occupation en accord avec leur raison d’être et leur modèle économique...

#### CONVENTION D’OCCUPATION INTERCALAIRE
Une convention à durée indéterminée qui dépend de la survenance effective du projet du propriétaire. La convention prend fin au « premier coup de pioche », avec une période de préavis dépendant de la durée d’occupation. La durée est au maximum de 3 ans entre un opérateur (par exemple une association d’hébergement d’urgence), un·e propriétaire, un·e résident·es et l’État.
-> https://www.reseaunationalamenageurs.logement.
gouv.fr/IMG/pdf/article_conventions_occupation.pdf

#### PRÊT À USAGE OU (ANCIENNEMENT) COMMODAT
Ce contrat permet au propriétaire d’un immeuble, d’un terrain, d’une parcelle de terre, d’un local, d’une maison, d’un appartement ou de tout autre bien immobilier de prêter celui-ci gracieusement pour son usage. L’entretien est à charge de l’occupant·e. Aucune rémunération ne peut être effectuée, sans quoi le contrat prêt à usage est automatiquement requalifié.

:::warning
**Aller plus loin :**
* Suivez ou appuyez le développement d’un laboratoire européen d’entraide juridique.
Trois pôles pressentis: protection des communs, recherche-action immersive, expertise juridique !
-> https://www.babalex.org
:::


## IN VIVO : ASSEMBLEE DES COMMUNS SUR L'ACCES AU FONCIER

Le 11 novembre 2023, une journée autour des modes d’accès au foncier s’organise à la Friche Lamartine (Lyon), sous la forme d’Assemblée des Communs. Une manière de s’approprier collectivement des retours d’expériences et des modalités juridiques.

**Les passeurs (Grenoble):**
> “Sur 20 ans, la question du logement temporaire à été amenée avec la question de la précarité choisie et subie. Se réapproprier la précarité, alléger les dépenses par la solidarité permet aussi d’être en recherche sur tout un tas de sujets.”
* Fanzine “retours sensibles” de la journée :
https://nuage.pocf.fr/s/NRDdjiikTYQXN8C
* Journal de bord de la journée :
https://pad.lamyne.org/assemblee-des-communs-lyon-2023

:::info
**Zoom sur :**
**VERS UNE CHARTE DES COMMUNS DANS LE RHONE A VILLEURBANNE ?**


Le Collectif Solidarités Cusset, qui occupe depuis l'hiver 2020 le bâtiment renommé l'Île Égalité, tente une expérimentation juridique. Il propose à la mairie une charte d’usage civique, inspirée du squat artistique italien l'Asilio basé à Naples.

> “On insiste plus sur le mode de fonctionnement (autogéré, démocratique, ouvert) et sur le projet politique, que sur le type de rapport juridique. Le bâtiment est géré par une assemblée des usages hebdomadaire, qui permet à toutes les personnes qui participent au lieu de s'informer et de faire évoluer les règles d'usage. Après la décision d'expulsion par le tribunal, la mairie a décidé de racheter le lieu et, depuis, nous essayons d'établir une relation différente des conventionnements classiques - courts et asymétriques. Jusqu'ici, les élu·es de Villeurbanne avec qui nous discutons sont ouvert·es à notre proposition de mettre en commun le lieu pour articuler différents modes de fonctionnements, non-contraints dès le départ.”

:::