# A PROPOS
**UN PROJET CONTRIBUTIF**

## UN PROJET COLLABORATIF
* Ce non-guide est un projet qui émerge en 2021 dans le cadre du réseau de préfiguration « Ambition Tiers-Lieux » et soutenu par la CRESS Centre Val-de-Loire. La version que vous lisez est soutenue par Relief en Auvergne Rhône-Alpes. La visée est de multiplier les adaptations locales de ce document, en libre appropriation.
* Ce projet s’inscrit dans une preuve de concept plus large qui a pour objectif de référencer et diffuser à plus large échelle les bonnes pratiques des Tiers-Lieux. D’une pratique de documentation collaborative (pad), à l’organisation de cettedocumentation (wiki), jusqu’à la diffusion papier (print) pour toucher un plus large public.
* Ainsi, vous aussi pouvez apporter votre pierre à cet édifice en évolution permanente. La version actuelle de l’ouvrage que vous avez sous les yeux n’est peut-être même pas sa dernière version.
* Pour ce faire, la modification et l’amélioration des contenus se réalise via les pages Movilab en lien, ou bien sur les pads (notes collaboratives en ligne) qui composent ce guide.

## LICENCES
Sauf mention contraire, les ressources présentes dans ce document sont sous licence Creative Commons CC BY NC SA.
**Illustrations : Vincent Burille**, responsable de la communication à la CRESS Centre-Val de Loire.
**Contenu texte :** Movilab, les différent·es contributeur·ices du projet. 
**Merci** à elles et eux, ainsi qu’aux commoneur·euses qui font vivre les ressources et outils permettant d’aboutir à des projets comme celui-ci.

:::info
**Zoom sur : Contribuer**
* La documentation du projet sur Movilab :
https://movilab.org/wiki/Anti-manuel_du_Tiers_Lieux
* Le pad du contenu de la version AURA 2023 :
https://pad.pocf.fr/non-guide-relief#

:::

:::warning
**Aller plus loin :**
* Pour des questions plus spécifiques ou une proposition de contribution plus particulière au projet de guide des Tiers-Lieux, ou si vous souhaitez l’adapter à un autre territoire, vous pouvez contacter :
-> baptiste.nomine@oxamyne.coop
:::

## CREDITS
#### REDACTION
* Organisations contributrices aux contenus : La POC, CRESS Centre-Val de Loire, Réseau Relief
* Contenu texte : les différent·es contributeur·ices au projet, Movilab

#### EDITION
* Association la POC
* Coordination éditorial : Baptiste NOMINE
* Relecture : Timothé JEANNE
* Suivi de projet : Yoann DURIAUX

#### GRAPHISME
* Mise en page : Laurianne GERVASONI
* Création graphique du Réseau : Mexiiico
* Illustrations : Vincent BURILLE
* Illustrations p. 24-25 : Conrtibeur·ice Movilab

-- **Édition AURA 2023** --
