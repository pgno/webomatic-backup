---
title: Livret 3 - Impact social et économique
licence : CC-BY-SA
author : Centres Sociaux Connectés
cover: https://www.ecoco2.com/wp-content/uploads/2022/04/AdobeStock_584394617-2048x1463.jpeg
collection : Projet de pierre
---

- [Livret 3 - L'impact des technologies (2/2) : impact social et économique](https://pad.lescommuns.org/formation-cs-low-tech-impact-socio-eco)
