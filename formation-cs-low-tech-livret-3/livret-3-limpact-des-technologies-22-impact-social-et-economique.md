---
title: L'impact des technologies (2/2), impact social et économique
---

# L'impact des technologies (2/2) : impact social et économique

Dans ce module, nous allons nous intéresser à la question des transformations sociales et économiques induites par le numérique. C'est une très vaste thématique car elle touche la quasi totalité de nos existences : travail, loisir, santé, information, et d'une manière plus large notre perception du monde.

Il est aussi très difficile d'avoir un propos équilibré car il existe de nombreux exemples qui démontrent à la fois les améliorations sociétales induites par les technologies du numérique que leurs effets néfastes à court et long terme.

Approcher cette complexité par le prisme des low-tech permet toutefois de mettre l'accent autour de critères d'évaluation tel que le développement de la capacité d'agir, la cohésion sociale, la prise en charge des vulnérabilités..., critères qui sont plus généralement mis en arrière plan du débat.

Nous allons donc parcourir très largement les grands secteurs impactés par le numérique et les réponses low-tech à ces enjeux et vous laissant une large place pour construire collectivement une représentation de l'ensemble de ce que nous avons vu jusqu'à maitenant.

:::success
Rappel : notre plan de formation
- [x] : Journée de lancement, introduction
- [x] : L'accessibilité, un numérique simple pour tous
- [x] : L'impact des technologies (1/2) : impact écologique
- [x] : **L'impact des technologies (2/2) : impact social et économique**
- [ ] : Faire soi-même, avec les autres
- [ ] : La durabilité technologique
- [ ] : L'utilité technologique 
- [ ] : Journée de clôture
:::

###### 

# Introduction

L'ère numérique, caractérisée par l'ascension rapide des technologies de l'information et de la communication, a transformé de manière fondamentale nos sociétés et nos économies. Cette révolution, souvent comparée à la révolution industrielle en termes d'impact, a redéfini non seulement la manière dont nous communiquons, mais aussi la façon dont nous travaillons, apprenons, et interagissons avec le monde qui nous entoure.

D'un point de vue social, l'impact du numérique est manifeste dans presque tous les aspects de la vie quotidienne. La connectivité mondiale, permise par Internet et les smartphones, a rapproché les gens géographiquement éloignés, et également créé de nouvelles formes de communautés sociales et professionnelles. Les réseaux sociaux, les blogs, les forums en ligne et les plateformes de partage de vidéos ont redéfini les interactions humaines, abolissant les frontières traditionnelles et créant de nouvelles opportunités pour l'expression personnelle et la mobilisation collective.

Dans le domaine de l'éducation, les MOOCs (Massive Open Online Courses), les tutoriels vidéo et les plateformes éducatives interactives ont considérablement modifié l'enseignement, pour permettre à des personnes de tous âges et de toutes origines d'apprendre à leur propre rythme, indépendamment de leur situation géographique ou économique.
> La RSE se décline aussi en RNE, Responsabilité Numérique des Entreprises. Retrouvez quelques données chiffrées dans la [synthèse 2021 de France Stratégie](https://www.strategie.gouv.fr/publications/responsabilite-numerique-entreprises-synthese).  
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/a340af0b-fa72-4697-9f05-7f383d4280f7.png =100x)


Sur le plan économique, l'impact du numérique est tout aussi profond. La digitalisation des entreprises a conduit à des gains substantiels en termes de productivité et d'efficacité, en transformant les processus de production, de distribution et de marketing. Les technologies émergentes comme l'intelligence artificielle, la robotique, et le big data sont en train de remodeler les industries, de la fabrication à la finance, promettant des niveaux d'efficacité et d'innovation sans précédent.

Cependant, cette transformation digitale n'est pas sans défis. L'un des plus importants est l'inégalité croissante, tant au sein des pays qu'entre eux. Alors que certains bénéficient pleinement des avantages de la révolution numérique, d'autres sont laissés pour compte, créant un fossé numérique qui exacerbe les inégalités sociales et économiques existantes. De plus, la perturbation du marché du travail, avec l'automatisation menaçant de remplacer de nombreux emplois traditionnels, pose des questions urgentes sur l'avenir du travail et la sécurité de l'emploi.

Les technologies et le numérique sont des moteurs puissants du changement social et économique. Si elles offrent des opportunités immenses pour l'innovation et la croissance, elles posent également des défis importants en termes d'inégalité et de transformation du marché du travail. La manière dont les sociétés s'adaptent à ces changements déterminera largement leur avenir économique et social.

# L'impact socio-économique des technologies

On ne peut réellement parler d'approche sectorielle pour l'application de la low-tech. Ses principes sont de nature transverses et portent des principes applicables à l'ensemble des secteurs socio-économique. Nous souhaitons juste ici mettre en regard la possibilité d'intégrer une démarche low-tech à travers quelques thématiques où domine de l'usage des technologies.

## Accessibilité économique

L'accessibilité économique du numérique en France, comme dans de nombreux pays développés, est une question clé qui influence l'inclusion et la participation active dans la société numérique moderne. Bien que la France bénéficie d'une infrastructure numérique relativement avancée, des disparités persistantes en matière d'accès et de compétences numériques existent, en particulier entre les zones urbaines et rurales, ainsi qu'entre différents groupes sociaux et économiques.

> Plusieurs opérateurs disposent de tarifs adaptés pour favoriser l'accès au numérique. Par exemple en france, avec [l'offre Coup de pouce d'orange](https://boutique.orange.fr/informations/offre-sociale/)
> [Offre coup de pouce](https://boutique.orange.fr/informations/offre-sociale/#qrcode)

:::success
**Coût de l'accès du numérique**

On peut tenter d'estimer le coût global d'accès au numérique à partir de montants moyens incluant le matériel et les abonnements. Cette estimation inclut les coûts moyens pour un abonnement Internet (30€ par mois), un abonnement mobile (20€ par mois), ainsi que l'amortissement du coût d'un ordinateur (environ 500 euros répartis sur 5 ans) et d'un téléphone mobile (environ 300 euros répartis sur 3 ans). C'est donc au total environ 67€ par mois de budget.

En france, Orange propose un [tarif à 15,99€](https://boutique.orange.fr/informations/offre-sociale/) pour l'accès à internet, le téléphone fixe et la TV, ainsi que l'accès à un ordinateur portable reconditionné à 169€ et à des ateliers numériques gratuit.

En belgique, à partir de mars 2024, plus d'un demi-million de Belges en situation de pauvreté [payeront maximum 19 euros](https://desutter.belgium.be/fr/en-2024-plus-dun-demi-million-de-belges-en-situation-de-pauvret%C3%A9-payeront-maximum-19-euros-pour-l) pour l’Internet. 

:::

## Le travail, emploi et formation

Il est extrêmement difficile d'évaluer l'impact sur le travail et l'emploi du numérique. Il existe pourtant de très nombreuses études qui s'y sont attelées, mais toutes sont à relativiser ou à regarder attentivement car les choix de la méthode d'évaluation est déterminante. 

D'une manière générale, dans le rapport entre la technologie et l'emploi, on constate fréquement que :  
- l'apparition d'une technologie "déplace" un certain nombre d'emplois, c'est à dire que certains secteurs subiront des destructions d'emplois quand d'autres seront créés. Les personnes touchées par les créations et les destructions ne sont généralement pas les même
- les transformations liées aux changements technologiques font souvent appel à un important besoin de formation. C'est souvent par le décalage entre l'apparition d'une technologie et la capacité à former à ces technologies que les décrochages en terme d'offre et de demande en emploi sont les plus visibles
- la technologie est bien souvent un facteur d'automatisation et c'est par ce biais qu'il touche le secteur du travail, c'est le cas depuis la révolution industrielle, principalement autour des métiers dit manuels. Toutefois, avec l'émergence de l'intelligence artificielle, on peut voir ce phénomène d'automatisation impacté le secteur des métiers dit intellectuels.

> Consultez l'[étude de l'OCDE](https://www.oecd.org/perspectives-de-l-emploi/2023/) sur l'impact de l'IA sur le travail
> [Etude OCDE](https://www.oecd.org/perspectives-de-l-emploi/2023/#qrcode)


:::success
**IA et emplois**

Le secteur de l'intelligence artificielle est en pleine explosion et les études relatives sont très nombreuses. Vous pouvez parcourir l'étude récente de l'OCDE sur la question à cette adresse : https://www.oecd.org/perspectives-de-l-emploi/2023/.

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/fa826532-3141-4638-8f60-9c43b0fce485.png)

_Rappelez-vous, une étude ne dit pas la vérité, elle en donne une vision. A la lecture de ce document, quel est votre sentiment ? vous sentez vous en phase avec ses conclusions ?_
:::

###### 

### L'exemple des plateformes numériques

Il est un exemple qui éclaire pleinement le sujet du rapport ambigu des technologies et du développement socio-économique.
Apparu il y a maintenant plusieurs années, les plateformes de service qui organisent la livraison à vélo (et plus globalement ce que l'on peut appeler la cyclo-logistique) ont montrées très rapidement le paradoxe tchnologique : d'un côté on a vu se développer à une vitesse très rapide tout un secteur d'activité considéré comme ayant un impact environnemental et économique positif, de l'autre sont apparus presque simultanément des effets catatrophiques sur les conditions de travail des livreurs de ses plateformes.

Parallèllement se sont développé des réseaux alternatifs aux acteurs majeurs du secteurs dont l'usage numérique incite à produire des effets diamétralement opposés : protection sociale des travailleurs, mise en commun et solidarité professionnelle...

> Découvrez un projet porté par l'APES, la plateforme [Nos déclics](https://nosdeclics.org/)
> [Nos déclics](https://nosdeclics.org/#qrcode)

:::success
**Nos déclics, local et solidaire**
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/7f771455-82fa-4835-a510-2ef3a796d455.png)

L'APES, Acteurs Pour une Economie Solidaire Hauts-de-France porte un projet européen autour des plateformes coopérative. Ce projet à donner naissance à un portail des plateformes coopératives, https://nosdeclics.org/, qui recensent les acteurs et producteurs locaux.
Ces plateformes touchent de nombreux secteurs et utilisent un éventail de solutions numériques à destination d'usages responsables.
:::

######  

## Cohésion sociale 

L'impact du digital sur la cohésion sociale est complexe et à double tranchant. D'une part, le digital favorise la création de communautés en ligne et facilite la communication et le partage d'informations, transcendendant les frontières géographiques et culturelles. Ces plateformes numériques permettent aux individus de se connecter avec d'autres qui partagent des intérêts similaires, renforçant ainsi les liens sociaux et la solidarité au sein de groupes diversifiés. De plus, les réseaux sociaux et les forums en ligne offrent des espaces pour la sensibilisation et la mobilisation autour de causes sociales, politiques et environnementales, contribuant ainsi à une plus grande participation civique et à un engagement communautaire.

Cependant, l'impact du numérique peut également être source de fragmentation et de polarisation sociale. La personnalisation des flux d'information et l'effet de chambre d'écho sur les plateformes de médias sociaux peuvent entraîner une homogénéisation des opinions au sein des groupes, excluant des perspectives divergentes et renforçant les préjugés existants. Cette situation peut mener à une polarisation des opinions et à une diminution de la tolérance envers les différences, affaiblissant le dialogue intercommunautaire et exacerbant les clivages sociaux. De plus, la fracture numérique peut accentuer les inégalités existantes et laisser certaines communautés marginalisées, érodant ainsi la cohésion sociale globale.

> L'ENS Lyon a publié en 2023 un rapport sur les monnaies complémentaires à consulter sur [ce lien](https://triangle.ens-lyon.fr/spip.php?article12027)
> [Etude ENS Lyon](https://triangle.ens-lyon.fr/spip.php?article12027#qrcode) 

:::success
**Low-tech et monnaie local complémentaire**

Une monnaie n'est pas une technologie, toutefois la plupart de nos moyens de paiements aujourd'hui sont rattachés à des outils high-tech : banque en ligne, terminal de paiement,... et ne sont évidemment pas neutre en terme d'impact socio-économique, voire environnemental. Une monnaie locale complémentaire quant a elle peut très bien être implémenté en "mode low-tech" et être associé à des valeurs proches comme l'échange de savoir ou la consommation de produits locaux. On en recensait jusqu'à 73 en circulation fin 2022.
- [Définition d'une monnaie locale complémentaire](https://www.economie.gouv.fr/particuliers/monnaie-locale)
- [Rapport 2023 sur les monnaies locales](https://triangle.ens-lyon.fr/spip.php?article12027)


![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/984b718f-588f-4490-8dc9-fced83924c28.jpeg)
_Le Golan, monnaie locale de la commune de Golancourt dans l'Oise à été initiée par le maire dans le but de sauver le bar devenu unique commerce de la ville et en difficulté suite à la crise du COVID-19 [En savoir plus - Article France3](https://france3-regions.francetvinfo.fr/hauts-de-france/oise/a-golancourt-dans-l-oise-une-monnaie-locale-pour-sauvegarder-le-seul-commerce-du-village-2416738.html)_
:::



## L'approche low-tech

La low-tech ne traite pas à proprement parlé de la question du travail, d'emploi ou de cohésion sociale. Mais c'est une volonté inscrite dans les principes même de la low-tech que d'avoir un impact positif sur la société. Ansi, on peut imaginer que :  
- l'acquisition et la transmission des savoirs et savoirs faire au centre de la démarche
- la simplicité dans la conception et la réalisation des technologies produites rendant possible l'auto-production et la réparation (cet aspect sera plus détaillé dans le module suivant)
- la mise à disposition des technologies à moindre coût à même de les rendre accessibles au plus grand nombre
- la volonté de priviligier les filières locales et de développer le tissu économique de proximité

sont autant de positions qui sont à même de générer des effects socio-économique positifs et de réduire les inégalités associées lors de l'introduction d'une technologie basée sur la low-tech. 

Globalement, il y a sur ce point une réelle différence avec les high-tech. Ces dernières seront principalement évaluées sur des critères d'efficacité et de rentabilité économique, c'est à dire d'un bénéfice direct lié à leurs usages et leurs commercialisations. Les questions à long terme de durabilité et d'impact sociétal sont évaluées dans un second temps comme des effets induits (qu'ils soient positifs ou négatifs) et ne remettent donc pas en cause leurs développements. A l'inverse, la synergie entre réduction de l'impact écologique, développement du lien social et l'accessibilité économique étant au centre du modèle low-tech, elles conditionnent l'émergence même des technologies issues de ce modèle.

Il est clair qu'à l'heure actuelle, le faible taux de déploiement et d'adoption de ces technologies ne permet pas de conclure sur les résultats d'un passage à l'échelle plus important. Comme toute technologie, la low-tech est soumise à l'effet réseau : son impact augmente exponentiellement avec le nombre d'usagers.

> Des plateformes d'emploi comme celle de Makesens répertorie des [emplois liés aux low-tech](https://jobs.makesense.org/fr/s/jobs/all?s=low-tech&sortBy=relevance)
> [Emploi low-tech](https://jobs.makesense.org/fr/s/jobs/all?s=low-tech&sortBy=relevance#qrcode)

:::success
**Low-tech : des métiers qui ont du sens**
L'association [Makesens](https://france.makesense.org) est une association développant de nombreux projets ayant pour vocation de développer le pouvoir d'agir citoyen. 
Elle a mis en place une plateforme d'emploi pour permettre aux associations, entreprises sociales, institutions de l'éco-système ESS de recruter facilement. 

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/9d0d938e-500e-47dc-9fc8-26f2cd4fdb5b.png)

_Ci dessus, les [offres identifiées low-tech](https://jobs.makesense.org/fr/s/jobs/all?s=low-tech&sortBy=relevance) sur la plateforme_

:::


# En pratique

:::warning
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/f3e92cbd-1ce6-4a29-a02d-a2448f1c3f92.png =20x)
Durée de la séquence : 30mn
:::

Pour cet exercice, nous vous proposons de choisir un usage technologique pariticipant à votre quotidien. Reprenez le fil de votre journée par exemple, vous y trouverez certainement une pratique associées à une "high-tech". 
A partir de cet usage, listez en quelques mots :  
- [ ] Quel à été le bénéfice pour moi de cet usage, qu'est-ce que cela m'a apporté, de quoi ai-je pu bénéficier ?
- [ ] Cet usage a-t-il eu en contrepartie un impact négatif direct ?
- [ ] Quelles sont potentiellement les conséquences cet usage ? A-t-il impacter d'autres personnes que moi ?  Est-ce un impact positif, négatif ? Si oui à quelle échelle ?
- [ ] Quel bilan tirez-vous de cet usage ? Y avait-il une alternative qui aurait pu rendre ce bilan plus positif ?

Si vous ne trouvez d'exemple facilement dans votre quotidien, vous pouvez cet exercice avec l'usage d'une visioconférence (pour une réunion profesionnelle par exemple)

## Travaux

:::warning
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/f3e92cbd-1ce6-4a29-a02d-a2448f1c3f92.png =20x)
Durée de la séquence : 50mn
:::

## Glossaire collaboratif

Ajoutez ces deux entrées au glossaire collaboratif de la session :
- Plateforme coopérative
- Intelligence artificielle

Vous êtes le premier ? Ajouter le terme à la suite du glossaire et tenter une définition en une ou deux phrases maximum.

Le terme est déjà présent dans le glossaire ? Proposer des commentaires et des modifications sur la définition proposée. Nous discuterons en session de visioconférence d'une version qui conviendrait à tous.

En complément de ces 2 termes, ajoutez-en d'autres qui vous paraitrait important. 

## Préprarez votre interview

Pour alimenter le dossier collectif d'évaluation de cette formation, nous vous proposons de réaliser l'interview d'un acteur dont les activités sont rattachées aux principes de la low-tech. 

Vous êtes libre de choisir cet interlocuteur comme vous le souhaitez. Si vous n'avez pas d'idée ou de contact qui puisse correspondre, nous pouvons vous mettre en relation avec un acteur identifié par nos soins en fonction de la thématique qui vous intéresse.

A défaut d'interlocuteur, vous pouvez réalisez un micro-trottoir, soit au sein de votre structure, soit à la rencontre de passant...

A ce stade, nous vous proposons : 
- d'identifier la personne ressource à interviewer et de prendre contact avec elle pour planifier l'interview. Ne tardez pas de sorte à ce qu'il vous reste suffisament de temps pour réaliser la restitution avant la fin de la formation

- de réfléchir à la trame de votre interview : quel angle ? quelles questions ? quels formats ? En bref, que souhaitez-vous apprendre ?

- de réfléchir à la restitution que vous souhaitez en faire. Le format de restitution vous appartient totalement. Cela peut-être tout simplement une retranscription texte, audio à la manière d'un podcast, d'une carte mentale, d'un dessin, d'une synthèse écrite...

Pour vous aider, voici quelques ressources guides pour la réalisation d'une interview : 
- [Réaliser une interview - ressource détaillée](https://bop.fipf.org/faire-une-interview/#)
- [Mener à bien son interview -  tableau de synthèse](https://occitanie-canope.canoprof.fr/eleve/audio/webradio/res/mener_interview.pdf)

## Préparez votre portfolio

Toujours dans le but de réaliser la restitution finale de votre participation à cette formation, nous vous proposons de réaliser un portfolio personnel (voir livret d'introduction).

Nous allons commencer à mettre en oeuvre ce travail. Pour cela, nous vous proposons de réfléchir à votre projet pour la prochaine visioconférence  :  
- Quel thématique parmi les principes du numérique responsable et de la low-tech souhaitez-vous aborder ?  
_Par exemple : les e-déchêts- 
- Quel angle souhaitez-vous retenir pour aborder cette thématique ?  
_Par exemple : le recyclage des e-déchêts-
- Quelles ressources comptez-vous mobiliser pour traiter ce sujet ?  
_Par exemple : visite d'une déchetterie_
- Quelle forme souhaitez-vous donner à votre restitution ?  
_Par exemple : un reportage photo et une infographie_

Nous sommes à mi-parcours de la formation, il vous reste donc plusieurs semaines pour réaliser ce dossier personnel. A ce stade, vous devez identifier votre problématique, et la restitution déterminée. Ainsi vous pouvez commencer à planifier les ressources et les moyens à mettre en oeuvre pour aboutir à sa réalisation. 

######

## Ressources

Continuez à enrichir le recueil du Labo Low-tech soit en ajoutant une ressource, soit en améliorant sa documentation (ajouter une photo, une capture d'écran, un petit résumé, etc..)

Pour y contribuer, c'est ici : https://pad.lescommuns.org/Labo-Low-Tech
