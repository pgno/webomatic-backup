# Evacuer et collecter

Aujourd'hui, les témoignages nous montrent plusieurs usages à Bois Blancs : on composte individuellement dans sa cour ou son jardin familial, on jette à la poubelle.

**Quelles autres solutions ? Quelles voies vers un compostage collectif ?**