# Premiers témoignages

Suite à l'invitation lancée par Emilie et Damien, plusieurs habitant.e.s des Bois Blancs ont témoigné de leur usage de toilettes sèches chez eux ou elles ou de leur envie d'en installer.


> Bonjour Émilie.
> Je te rassure : rien de compliqué à faire des toilettes sèches (on les appelle aussi toilettes à litiere bio maîtrisée). Il y a plein de modèles sur Pinterest. J'en utilise au quotidien, j'en ai même fait  avec une ancienne chaise percée, c'est le plus simple. Ensuite je te conseille un seau en inox que tu trouves sur des sites spécialisés. Je n'ai jamais utilisé de bavette et je ne sépare pas les matières. Ces sites proposent des toilettes toutes faites également. 
> Le seau rempli est vidé dans le compost au fond de ton jardin. Le plus compliqué est peut être de trouver de la sciure ou des copeaux pour recouvrir les excréments. À Bois blancs il y a la menuiserie Lallemant à qui tu pourrais demander. Actuellement je ne connais pas d'autres solutions que de recycler individuellement dans son compost perso, il faut donc une cour ou un jardin. 
> Dans un jardin ou une cour, le composteur individuel ne nécessite que 1m2 d'occupation au sol. C'est à savoir : pas besoin d'une grande propriété pour cela.
Sans composteur individuel je n'ai pas d'idée de solution. Si vous en avez une, ça m'intéresse 👍🏽
> Voilà pour mon expérience. Tu verras, tu ne pourras plus t'en passer 👍🏽

*Corinne, 23 octobre 2023*


> Nous on a des toilettes sèches depuis une quinzaine d'année. J'ai construit le siège moi-même (très simple) et on s'approvisionne en sciure au TechShop. On est partant pour partager notre expérience et pour voir si on peut trouver un approvisionnement collectif. 

*Juliette et Denis, 4 novembre 2023*


> Je ne te cache pas que cette annonce m'intéresse au plus au point. Ça fait tellement longtemps que j'y pense mais encore plus ces dernières semaines.
> Je pense faire de la place chez moi et essayer de construire mes toilettes sèches à côté des toilettes humides, pour faire un test. 
> Je manque de temps bien entendu mais la perspective de se rencontrer sur ce sujet relance mon enthousiasme et ma motivation 🤗

*Farah, 5 novembre 2023*


> Pour mon retour d'expérience, je suis ravie de mes toilettes sèches. Que j'utilise plus niveau pipi par contre j'aimerais trouver des copeaux moins chers. Hélas c'est de la litière à lapin. J'en trouve moins cher a magik dépôt a Lomme. J'ai fabriqué le toilette car c'est hors de prix pour ce que c'est.

*Emilie, 30 novembre 2023*