
# Culture de la merde – La sainte merde
© Hundertwasser Archiv, Wien
Source : https://www.agrarinfo.ch/nature/sainte-merde/


Le royaume des plantes a travaillé durement
pendant des millions d’années
pour couvrir les poisons
et les gaz malodorants
avec une couche d’humus
une couche d’oxygène
pour que l’homme peut vivre sur cette terre

Et ce même homme ingrat
extrait aujourd’hui du sous-sol
ces mêmes substances malsaines
ces mêmes poisons oubliés
qui ont été enfouis
par un long et laborieux travail cosmique
et les remet à nouveau
à la surface de la terre

Ainsi, par le méfait de l’homme irresponsable
la fin des temps rejoint le début des temps
et ce monde va finir comme il a commencé

Nous nous suicidons
Nos villes sont des formations cancéreuses
Tout œuvre de l’homme moderne
est devenue cancer sur cette terre
Du ciel, cela est clairement visible

Nous ne mangeons pas
ce qui pousse près de nous
nous cherchons notre nourriture très loin
en Afrique, en Amérique, en Chine
et en Nouvelle Zélande

Nous ne gardons pas notre merde pour nous
Notre merde est emportée très loin
polluant les rivières, les lacs, les océans
ou envoyée dans des stations d’épuration
sophistiquées et coûteuses
très rarement dans les
usines de compostage centralisées
ou bien notre merde est détruite

Notre merde ne revient jamais dans nos champs
et ne retourne jamais à l’endroit
d’où notre nourriture provient

LE CYCLE DE LA NOURRITURE A LA MERDE MARCHE BIEN
MAIS LE CYCLE DE LA MERDE A LA NOURRITURE EST INTERROMPU

Nous avons une fausse conception
de notre merde et de nos déchets

Chaque fois qu’on tire la chasse d’eau
croyant que c’est un geste d’hygiène
c’est un geste de rupture avec le monde
un geste immoral
un geste de la mort

Quand nous utilisons les toilettes
c’est comme si nous nous rendions
à une frontière qu’on ne doit pas franchir
Nous nous enfermons à clef
et nous éliminons notre merde
en la faisant disparaître
de notre vue et de notre existence

Pourquoi avons-nous honte?
De quoi avons-nous peur?

Ce que devient notre merde
nous le supprimons
comme si nous voulions effacer la mort
Le trou des toilettes
dans lequel disparaît une partie de nous-même
est comme la porte de la mort
on tire la chasse d’eau
on s’éloigne vite de ce lieu malsain
oublions vite pourriture et décomposition
Mais la vérité est justement le contraire
Avec la merde, la vie commence

LA MERDE EST BEAUCOUP PLUS IMPORTANTE
QUE LA NOURRITURE

La nourriture soutient le genre humain
qui croît en quantité
et diminue en qualité
et qui est devenu un danger de mort
pour cette terre, pour le royaume végétal
pour les animaux, pour l’eau, pour l’air
et pour la couche d’humus

La merde est la voie de notre résurrection

Depuis que l’homme pense
il essaye d’être immortel
L’homme veut avoir une âme éternelle

LA MERDE EST NOTRE AME ETERNELLE
PAR LA MERDE NOUS SURVIVRONS
PAR LA MERDE NOUS DEVENONS IMMORTEL

Pourquoi avoir peur de la mort?
Celui qui utilise des toilettes à humus
n’a pas peur de la mort
parce que notre merde prépare une vie future
et rend notre renaissance possible

SI NOUS NE VENERONS PAS NOTRE MERDE
ET SI NOUS NE LA TRANSFORMONS PAS EN HUMUS
EN L’HONNEUR DE DIEU ET LE MONDE
NOUS PERDONS LE DROIT
D’ETRE PRESENTS SUR CETTE TERRE

AU NOM DES LOIS SANITAIRES PERIMEES ET FAUSSES
NOUS PERDONS NOTRE SUBSTANCE COSMIQUE
NOUS ASSASSINONS NOS VIES FUTURES

LA SALETE EST LA VIE
LA PROPRETE STERILE EST LA MORT

« Vous ne tuerez pas »
mais nous stérilisons toute vie
avec du poison et une couche de béton
c’est un meurtre

L’homme est un tube
d’un côté il insère des choses
de l’autre côté elles sortent
plus ou moins digérées
La bouche est devant, l’anus derrière
pourquoi est-ce ainsi ?
Pourquoi le manger est-t-il positif
pourquoi la merde est-t-elle négative?
Ce devrait être le contraire
Ce qui sort de notre corps
ce ne sont pas des déchets
mais la base, le fondement de notre monde
notre or. C’est notre sang
Nous saignons à mort
notre civilisation saigne à mort
par la folle interruption du cycle
celui qui saigne longtemps
qui perd son sang longtemps
et qui ne le renouvelle pas périra

Sigmund Freud avait raison
dans son interprétation des rêves :
la merde veut dire : de l’or
Maintenant nous devons apprendre
que ce n’est pas un rêve
mais une réalité

Si Pasolini dans un film
laisse les acteurs manger leur merde
cette action doit être comprise
comme un symbole désespéré
de refermer le cycle
comme une tentative dramatique
d’accélérer la survivance cosmique

Le même amour, le même temps
et la même attention
doivent être employés
pour ce qui sort par derrière
comme pour ce qui entre par devant

La même cérémonie
lorsque nous dînons solennellement
avec couteaux, fourchettes, cuillères
baguettes chinoises
service de table en argent et candélabres

NOUS PRIONS AVANT DE MANGER
ET NOUS RECITONS LES GRACES
MAIS PERSONNE NE PRIE QUAND NOUS CHIONS

NOUS REMERCIONS DIEU
POUR NOTRE PAIN QUOTIDIEN
QUI VIENT DE LA TERRE
MAIS NOUS NE PRIONS PAS
POUR QUE NOTRE MERDE REDEVIENNE TERRE
LE DECHET EST BEAU
TRIER LES DIFFERENTS TYPES DE DECHETS
ET LES REINTEGRER DANS LE CYCLE
EST UNE OCCUPATION PLEINE DE JOIE

Cette occupation ne se fait pas
dans les caves, dans les cours sombres
dans les fosses puantes et des toilettes étroites
mais là où nous habitons
là où il y a la lumière et le soleil
dans notre séjour
dans notre salle à prestige

Il n’y a pas de déchets
Les déchets n’existent pas
Les toilettes à humus sont un « status symbol »

NOUS AVONS LE PRIVILEGE D’ETRE TEMOINS
COMMENT PAR NOTRE PROPRE SAGESSE
NOTRE PROPRE MERDE, NOS PROPRES DECHETS
SE TRANSFORMENT EN HUMUS
C’EST COMME UN ARBRE QUI GRANDIT
CHEZ NOUS A LA MAISON
COMME SI C’ETAIT NOTRE PROPRE FILS

Homo humus humanitas
trois mots clefs, une racine, un destin

L’humus est le véritable or noir

L’humus a une bonne odeur
l’odeur de l’humus est plus sacrée
plus près de dieu que l’encens
celui qui se promène dans la forêt
après la pluie
connait bien cette odeur

Bien sûr, cela semble monstrueux
lorsque notre poubelle
devient le centre de notre maison
et que les toilettes à humus
devient notre siège d’honneur

Mais c’est justement le tournant
que notre société, notre civilisation
doivent prendre, si elles veulent survivre

L’odeur de l’humus est l’odeur de dieu
l’odeur de la résurrection
l’odeur de la vie éternelle

Régidé en 1979/1980 à Algajola, Venise et Neuseeland

(texte original en allemand, traduit en français par Hundertwasser)
Schurian, Walter (Hg.): Hundertwasser – Schöne Wege, Gedanken über Kunst und Leben. Langen Müller Verlag: München 2004, S. 102-104
© 2012 Hundertwasser Archiv, Wien