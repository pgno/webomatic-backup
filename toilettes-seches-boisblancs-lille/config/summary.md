---
title: Toilettes Sèches à Bois Blancs
author : Bois Blancs en Transition
cover : https://hot-objects.liiib.re/pad-lescommuns-org/uploads/86cd2439-3462-400d-a13e-fd2728aeb889.jpg

---

- [Vue d'ensemble](https://pad.lescommuns.org/toilettes-seches-boisblancs-intro)
- [Novembre 2023 : Invitation à nous relier à Bois Blancs](https://pad.lescommuns.org/toilettes-seches-boisblancs-invitation)
- [Premiers témoignages](https://pad.lescommuns.org/toilettes-seches-boisblancs-temoignages)
- [30 novembre 2023 : Première rencontre](https://pad.lescommuns.org/toilettes-seches-boisblancs-30novembre2023)
- [S'équiper de toilettes sèches](https://pad.lescommuns.org/toilettes-seches-boisblancs-equipement)
- [S'approvisionner en sciure/copeaux](https://pad.lescommuns.org/toilettes-seches-boisblancs-approvisionnement)
- [Evacuer et collecter](https://pad.lescommuns.org/toilettes-seches-boisblancs-evacuation-collecte)
- [Valoriser](https://pad.lescommuns.org/toilettes-seches-boisblancs-valorisation)
- [Sites ressources sur les toilettes sèches](https://pad.lescommuns.org/toilettes-seches-boisblancs-ressources)
- [Culture de la merde – la sainte merde : Le manifeste d'Hunderwasser](https://pad.lescommuns.org/toilettes-seches-boisblancs-sainte-merde)


