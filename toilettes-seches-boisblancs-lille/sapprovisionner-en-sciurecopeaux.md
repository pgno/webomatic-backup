# S'approvisionner

Usages observés aujourd'hui :
Sciure et copeaux de bois récupéré au TechShop
Litière de lapin achetée en magasin

Possibilité d'utiliser des copeaux de lin (le Nord est le plus gros producteur de lin, qui est une culture durable)

**Auprès de quel(s) fournisseur(s) organiser des achats groupés ? Quel produit privilégier ?**