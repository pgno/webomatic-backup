# Première invitation à se relier

Le samedi 4 novembre 2023 à 15:08:15 UTC+1, Emilie B a écrit sur la liste Passe à ton voisin (PATV) :

> Bonjour à toutes et tous,
> 
> Peut-être utilisez-vous des toilettes sèches chez vous ? Ou, plus probablement, aimeriez-vous le faire sans savoir comment ? Damien et moi relançons notre projet de relier les personnes désireuses d'utiliser des toilettes sèches à Bois Blancs. On a envie de trouver des solutions pour rendre ça possible et -pourquoi pas- facile ! Motivé.e ou juste curieux .se, on t'invite au Tok'Ici le jeudi 30 novembre à partir de 18h pour une première rencontre.
> 
> Ça va prendre du temps tout ça, mais on te promet une aventure créative et poétique ! Pour tout dire, on a envie de s'engager dans un véritable processus de co-conception et de prendre le temps qu'il faudra pour créer petit à petit tous les maillons de la chaîne. Fais nous signe si tu as envie de faire quelque chose avec nous. Tu peux nous envoyer ton témoignage (que tu en utilises déjà ou que tu aies le souhait de t'y mettre), nous partager des ressources, nous mettre en lien avec des personnes qui pourront faciliter la démarche... Et n'hésite pas à faire suivre ce message à qui pourrait être sensible à cette proposition.
> 
> Et d'ici là, que tu sois sceptique ou impatient.e, tu peux écouter une récente émission de La Terre au Carré sur les toilettes sèches. Merci à Sam de l'avoir repérée ! Peut-être les choses vont-elles bouger plus vite qu'on ne croit ;-)
> 
> -Émilie & Damien
   
    
Rappel le 29 novembre 2023 : 

> *Faire caca dans l'eau potable, est-ce bien raisonnable ?*
> 
> Retrouvez-nous au Tok'Ici ce jeudi 30 entre 18h et 20h avec vos rimes riches, vos questions, vos ressources et pour répondre à cette question (pas facile).
> 
> -Emilie et Damien