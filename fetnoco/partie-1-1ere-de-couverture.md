# Un Festival de nos Communs dans la vallée de la Drôme


![](https://hot-objects.liiib.re/pad-pocf-fr/uploads/5291cc14-e4c0-476d-b4b8-e7ec93e95bab.jpg)


https://movilab.org/wiki/Le_Festival_de_nos_Communs

On souhaite mieux se connaître et fluidifier la collaboration, depuis nos vécus des Communs et des Tiers-Lieux. Quels outils, quelles méthodes, quels acteurs, comment naviguer dans tout ça?

Convivialité, Interconnaissance, Stimulation !