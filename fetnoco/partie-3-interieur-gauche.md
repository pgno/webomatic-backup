# Du "Quoi" au "Comment"

:::danger
ici attendu deux mots sur le format d'animation proposé le premier jour
:::


## 1 - Un panorama des acteur·ices & outils

### Ce à quoi nous sommes relié·es en deux mots

:::danger
un mini-texte de synthèse?
- -
des citations en visuel
:::


![50](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/a68504c3-8cd6-4f6b-8513-5f1b161940e2.jpg)

:::danger
Premier audio - tour de table
:::
:::danger
Second audio - détails
:::

### Focus sur...

* Les communs de locaux & territoriaux
* De la philosophie du libre aux licences libres dans le logiciel
* Des communs étatiques



:::danger
textes à nettoyer
:::

:::info
**Les licences libres**
4 grandes libertés 

* la liberté d’utiliser le logiciel, pour quelque usage que ce soit — liberté 0
* la liberté d’étudier le fonctionnement du programme, et de l’adapter à vos propres besoins — liberté 1
* la liberté de redistribuer des copies à tout le monde — liberté 2
* la liberté d’améliorer le programme et de publier vos améliorations — liberté 3.
:::

:::success
**Copyright / Copyleft**
Droit de common law anglosaxon plus adapté pour protéger les communs. 
Outil - Les trusty 
Open law est surtout sur la législation de l'intelligence artificielle pour protéger les intérêts des acteurs du libre. Ils tentent de garantir un maximum d'ouverture auprès des ministères. Orange avait été condamnée pour parasitisme suite à un non respect de licence de réciprocité. Une haute juridiction avait tranché sur la notion de parasitisme, le non respect de la licence entraînement un concurrence déloyale, etc.

Gros mouvement de mises en relation de fabriques de communs, d'intérêt général. Hub data hub qui rencontre des difficultés de mise en oeuvre.
Plateforme des données écologiques, mais elle sert plutôt d'espace d'eploitation plutôt que de mise en communs. Open data france essaye de créer des espaces ambiance communautaires.
:::

:::warning



**Des exemples de communs historiques organisés par l'état :**
* Etablissement national du sang - le sang n'appartient pas à quelqu'un. Produit sensibile. Administration d'état pour s'en occuper. Pas commercialisable. CA de l'EFS - ministères, etc. Collège de 3 représentants, 2 donneurs, un receveur. Les contributeur·ices valorisé·es.
* Les haras nationaux - veillent à la pureté des races équines. Avant les auto, les chevaux. Certification, pour être sûr·es d'avoir des bonnes bêtes. 
* Domaine national de Chambord. Etablissement public géré par un Conseil d'administration qui gère tout les aspects du territoire. 
* Parc nationaux, parc naturel régionaux, chartes s'imposent aux acteur·ices. Négociation avec les habitant·es (ex. du cirque de Mafate).
:::

## 2 - Détails sur des projets en cours


![70](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/014ccb9c-b163-4aa8-8a3d-5b374f74a0c5.jpg)

:::danger
mise à jour du visuel avec les ajouts de légende (Mailis)?
:::

:::danger
un audio ou pas
:::

**Un inventaire des communs naturels**

![50](https://hot-objects.liiib.re/pad-pocf-fr/uploads/210191d2-d2d3-4332-9bd0-2c2b1c077ba6.png)

à découvrir sur: https://yeswiki.lescommuns.org/communsnaturels/PrincipalE



## 3 - Collaborations entre Communs et Tiers-Lieux & les outils

:::danger
ici attendu le "quoi" des collaborations en 2 mots
:::

![70](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/4371b2f0-3021-44fa-9ad2-39296b865ecf.jpg)

:::danger
audio de 30'
:::
:::danger
citations en visuel
:::