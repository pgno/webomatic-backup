# Les Intentions
[page 2]

:::success
++D'où ça part:++
La [Fabrique à communs des tiers-lieux](https://movilab.org/wiki/Fabrique_%C3%A0_communs_des_tiers-lieux) est un collectif. Elle rassemble et fédère les invididus qui cherchent à produire des solutions et des services aux commoners et commoneuses des tiers-lieux avec une approche centrée sur les usages, productive et pragmatique. Le collectif se met au service des acteurs des tiers-lieux qui souhaitent favoriser le développement des communs. Le collectif peut être interpellé et ses membres établissent comment mobiliser leurs compétences de conception, d'animation communautaire et d'ingénierie de projets pour répondre aux sollicitations. Le fonctionnement et les projets sont cofinancés par les acteurs des tiers-lieux qui soutiennent et s’impliquent dans le déploiement de la feuille de route. La feuille de route et la stratégie sont réinterrogées et mises à jour tous les ans avec les autres acteurs de l’écosystème tiers-lieux.
:::

++Objectif des deux jours++

Rassembler les personnes impliquées autour des Communs et des Tiers-Lieux pour échanger, mieux se connaître, et faciliter le partage et le travail collaboratif.

L’événement vise à améliorer la collaboration entre les Tiers-Lieux et les Communs en expliquant les concepts des Communs, en identifiant les acteurs de l’écosystème, et en définissant des méthodes de collaboration efficaces. 

![50](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/61b6e642-2c45-4df2-8d81-2f7cd5bc7091.jpg)

## Le programme en bref

:::danger
le qui / Quoi / Comment / La suite en un visuel ou paragraphe
- -
le qui / Quoi / Comment / La suite en un visuel ou paragraphe
- -
le qui / Quoi / Comment / La suite en un visuel ou paragraphe
:::



# Elles et ils en étaient
[page 3]

:::danger
3-4 citations avec petits visuels
éventuellement une sythèse écrite des présentations?
:::

![50](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/1c8922ab-2b06-4588-979b-30acb76fe352.jpg)

:::danger
un audio avec les présentation et les interviews?
:::