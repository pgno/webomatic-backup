# Vers une intention commune


:::danger
Audio de 8 minutes d'intro
:::

++Intention:++ passer de la collaboration (en temps réel à plusieurs) à la coopération (chacun·e de son côté). Pour ça on a besoin de se fixer des objectifs communs. Ou au moins des points de convergence entre collectifs, entre acteurs.

++Proposition:++
2050 comme ligne de mire qui permette d'impulser des morceaux de vision depuis nos activités/vies. Se partager une ligne de mire collective, puis revenir sur des objectifs dans les prochaines années et dizaines d'années. Inclure des mot-clés pour les valeurs partagées.

:::success
Autour des pratiques de fiction collective: https://usbeketrica.com/fr/article/design-fiction-le-futur-c-est-plus-ce-que-c-etait
:::

## Restitution par groupe

:::danger
Audio de 7 min en resssource
:::

**Groupe 1**
La relocalisation de la production, énergie, alimentation s'est faite. Des communautés s'organisent autour de ces productions, via un maillage de lieux plus ou moins connectés les uns aux autres. L'écoles est plus orientée vers les métiers manuels. Et aussi dans le processus éducatif, la gestion des conflits, la co-construction, l'empathie... Le mode de gestion collaboratif est agile fonctionne!

**Groupe 2**
Les communs (...) notamment dans l'éducation nationale (...) revenu fixe, revenu contribué. Communs de la régénération (de l'écosystème, de la société) via le "given" comme monnaie locale. Les CAE auraient un rôle de garde-fou socialo-économique pour évité l'ubérisation des travailleureuses. Des nouvelles métriques pour la création de valeur, afin de pérenniser les actions de communs orientés vers des modèles sociaux et écologiques. En réduisant le coût du capital

# On s'appelle?

:::danger
des contacts des personnes présentes
- -
des contacts des personnes présentes
- -
des contacts des personnes présentes
- -
des contacts des personnes présentes
- -
des contacts des personnes présentes
- -

:::