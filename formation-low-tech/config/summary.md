---
title: Numérique éthique, en route vers les low-tech
licence : CC-BY-SA
author : Centres Sociaux Connectés
cover: https://hot-objects.liiib.re/pad-lescommuns-org/uploads/54ff4b99-ca28-4b02-8f68-1213dc19d963.png
collection : Projet de pierre
---

- [Livret 1 - Introdution](https://pad.lescommuns.org/formation-cs-low-tech-intro)
- [Livret 2 - Accessibilité : un numérique pour tous](https://pad.lescommuns.org/formation-cs-low-tech-accessibilite)
- [Livret 3 - L'impact des technologies (1/2) : impact écologique](https://pad.lescommuns.org/formation-cs-low-tech-impact-ecologique)
- [Livret 4 - L'impact des technologies (2/2) : impact social et économique](https://pad.lescommuns.org/formation-cs-low-tech-impact-socio-eco)
- [Livret 5 - Faire soi-même avec les autres](https://pad.lescommuns.org/formation-cs-low-tech-diywo)
- [Livret 6 - La durabilité technologique](https://pad.lescommuns.org/formation-cs-low-tech-durabilite)
- [Livret 7 - L'utilité technologique](https://pad.lescommuns.org/formation-cs-low-tech-utilite)