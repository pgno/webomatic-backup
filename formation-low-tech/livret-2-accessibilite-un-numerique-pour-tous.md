---
title: Un numérique simple pour tous
licence : CC-BY-SA
author : Centres Sociaux Connectés
cover: https://hot-objects.liiib.re/pad-lescommuns-org/uploads/f046ef83-68d5-42a4-b697-448029fdd9fe.png
---

# L'accessibilité : un numérique simple pour tous

Dans ce premier module, nous allons nous intéresser à la question de l'accessibilité. Si elle recouvre des notions légérement différentes selon que l'on s'intéresse particulièrement au numérique ou aux low-tech plus globalement, l'accessibilité est la porte d'entrée vers une technologie plus inclusive et respectueuse de chacun. 

C'est pourtant parfois le "parent pauvre" du numérique. Si chacun peut en être globalement conscient, il est plus rare de voir les projets intégrer pleinement la question de l'accessibilité. Cette dernière est plus souvent vu comme une contrainte pesant sur la faisabilité d'un projet que comme une opportunité d'accueillir un public plus large.

Nous examinerons d'abord les grands axes liés à la question de l'accessibilité avant d'étudier un ensemble d'exemple qui vous permettront de vous approprier cette notion.

:::success
Rappel : notre plan de formation
- [x] : Journée de lancement, introduction
- [x] : **L'accessibilité, un numérique simple pour tous**
- [ ] : L'impact des technologies (1/2) : impact écologique
- [ ] : L'impact des technologies (2/2) : impact social et économique
- [ ] : Faire soi-même, avec les autres
- [ ] : La durabilité technologique
- [ ] : L'utilité technologique 
- [ ] : Journée de clôture
:::

###### 

# Introduction

:::warning
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/f3e92cbd-1ce6-4a29-a02d-a2448f1c3f92.png =20x)
Durée de la séquence : 20mn
:::

Les nouvelles technologies sont de formidables outils qui rendent de nombreux services au quotidien et qui aident à concevoir des solutions à de nombreux problèmes. A condition evidemment d'être en capacité de les utiliser et qu'elles ne créent pas plus de problèmes que de solutions !

A mesure, que les technologies évoluent, elles s'enrichissent mais aussi se compléxifient. Elles se succèdent et chaque nouvelle "couche" nécessite de nouveaux apprentissages, la compréhension de nouvelles manières de faire, un nouveau vocabulaire...

> Pour en savoir plus sur les difficultés des français face au numérique, notamment depuis la crise du COVID-19, parcourez [cette étude réalisée en 2022](https://www.ifop.com/wp-content/uploads/2022/04/PPT_Simplon_2022.03.07.pdf) par l'Ifop pour Simplon.
> [](https://www.ifop.com/wp-content/uploads/2022/04/PPT_Simplon_2022.03.07.pdf#qrcode)

Si l'on y fait pas attention, rater une simple marche peut vite mener à rester "en bas de l'escalier"...
Pourtant si l'on souhaite que le numérique soit un facteur de transformation (sociale, écologique, économique ...), il faut bien faire attention à embarquer tout le monde. Sans quoi ce que peut nous apporter le numérique et la technologie d'une manière générale ne pourrait être qu'une promesse.

## De l'exclusion à l'inclusion numérique

L'exclusion numérique est une thématique préoccupante. Et elle concerne potentiellement tout le monde car la fracture avec le monde technologique peut survenir pour tout un tas de raisons et à différents moments de la vie : par la survenue d'une situation de handicap, l'âge, un "décrochage" personnel, ou tout simplement une inégalité d'accès liée à une situation géographique.

Pensez un "numérique pour tous" est évidemment une tâche d'une grande ampleur, mais il resulte surtout d'une vigilance et d'une prise de considération de la diversité des usages et des usagers dans la conception des technologies et des dispositifs qui les mettent en oeuvre.

Parfois il s'agit simplement de l'utilisation d'un meilleur design, de l'"allégement" des fonctionnalités superflues, le poids d'un fichier, la traduction, la conformité aux consignes d'accessibilité, etc.. Ces critères évidents suffisent parfois à déplacer la frontière entre une technologie qui exclue et d'une technologie qui rapproche, thème central des low-tech.

###### 

# Low-tech et mumérique éthique : deux approches complémentaires 

L'accessibilité et l'inclusion sont des préoccupations centrales tant dans la low-tech que dans le numérique éthique, bien qu'elles soient abordées de manières légèrement différentes dans chaque domaine.

### Accessibilité dans la low-tech  
- **Simplicité d'Usage** : Les technologies low-tech sont conçues pour être simples à comprendre et à utiliser, ce qui les rend accessibles à un plus large éventail de personnes, y compris celles qui n'ont pas de compétences techniques avancées.
> Nous reviendrons sur les aspects spécifiques lié aux coûts et la réparabilité, mais ils sont des facteurs clés de l'accessibilité en dehors des questions liées au handicap qui sont généralement celles mises en avant.
- **Coût Abordable** : En étant économiquement accessibles, ces technologies permettent à des personnes de milieux économiquement moins privilégiés d'accéder à des solutions technologiques.
  
- **Réparabilité et Maintenance** : La facilité de réparation et de maintenance des technologies low-tech favorise l'autonomie des utilisateurs, réduisant la dépendance à des experts ou à des pièces de rechange coûteuses.

### Accessibilité dans le numérique éthique  
- **Conception Universelle** : Le numérique éthique s'efforce d'intégrer des principes de conception universelle, rendant les produits et services numériques accessibles aux personnes ayant différents types de capacités, y compris les personnes en situation de handicap.  

- **Respect de la Vie Privée et Sécurité** : En se concentrant sur la protection des données personnelles et la sécurité, le numérique éthique veille à ce que les utilisateurs puissent contrôler leurs informations personnelles, un aspect crucial pour l'inclusion numérique.  

- **Transparence et Éducation** : Il promeut également la transparence dans les algorithmes et les processus, ainsi que l'éducation numérique, permettant aux utilisateurs de comprendre et de naviguer dans le paysage numérique de manière plus éclairée.

###### 

### Du point de vue de l'inclusion  
- **Low-tech** : L'inclusion dans le domaine de la low-tech signifie que les technologies sont conçues et distribuées de manière à être utilisables et accessibles dans divers contextes culturels, économiques et géographiques, notamment dans les régions plus isolées.  

- **Numérique Éthique** : L'inclusion ici fait référence à la création d'espaces numériques où divers groupes, y compris ceux souvent marginalisés ou sous-représentés, peuvent participer pleinement et en toute sécurité.

:::success
**En résumé, la low-tech et le numérique éthique partagent un engagement envers l'accessibilité et l'inclusion, mais l'appliquent de manières complémentaires. La low-tech se concentre sur la simplicité, un co^t abordable et l'autonomie, tandis que le numérique éthique met l'accent sur la conception universelle, la sécurité des données et la transparence. Ces approches visent ensemble à créer une technologie plus accessible, inclusive et équitable pour tous.**
:::

###### 

## Accessibilité : bonnes pratiques et outils
> Les [WCAG](https://www.monparcourshandicap.gouv.fr/glossaire/wcag) rassemblent les directives pour l'accessibilité du web. Elles sont éditées par le W3C. En France, on peut se référé au Référentiel Général d’Amélioration de l’Accessibilité (RGAA) 
[](https://www.monparcourshandicap.gouv.fr/glossaire/wcag#qrcode)

Pour améliorer l'accessibilité dans les domaines de la low-tech et du numérique, il est important d'adopter des bonnes pratiques, de mettre en œuvre des solutions efficaces et d'utiliser des outils appropriés. Voici quelques points de repères pour chacun de ces domaines :

### Design et conception web accessible
Concevoir des produits qui peuvent être utilisés facilement par le plus grand nombre, y compris les personnes âgées ou handicapées. Concernant le numérique, il existe des guides et référentiels qui permettent de répondre à cet enjeu, dont les WCAG (Web Content Accessibility Guidelines). Créer des systèmes qui peuvent être facilement modifiés ou mis à niveau en fonction des besoins changeants participe aussi à améliorer l'accès.

### Formation et éducation
Organiser des ateliers et des formations pour enseigner aux communautés locales comment utiliser, réparer, et entretenir les technologies low-tech, intégrer des technologies d'assistance, telles que les lecteurs d'écran, les agrandisseurs d'écran, ou les systèmes de commande vocale. Il s'agit aussi de sensibiliser et former les développeurs d'application en matière de principes d'accessibilité numérique.

### Collaboration avec les communautés Locales
Travailler en étroite collaboration avec les communautés locales pour comprendre leurs besoins spécifiques et adapter les solutions en conséquence, effectuer des tests réguliers avec des utilisateurs ayant diverses capacités pour identifier et corriger les problèmes d'accessibilité.

### Documentation et guides faciles à comprendre
Fournir des manuels d'utilisation et des guides clairs, idéalement avec des illustrations ou des instructions étape par étape (voir le focus FALC plus bas). Il convient aussi d'assurer une maintenance régulière des systèmes numériques pour garantir leur accessibilité continue.

###### 

# En pratique

:::warning
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/f3e92cbd-1ce6-4a29-a02d-a2448f1c3f92.png =20x)
Durée de la séquence : 50mn
:::

Voici quelques exercices et exemples inspirants qui vont vous permettre de familiariser votre regard aux problématiques d'accessibilité. Les consignes proposées sont indicatives. Elles vous invitent à prendre en note vos impressions. La session de visioconférence sera l'occasion d'échanger et de débatrre autour de ces questions.

## Accessibilité web
> Vivre l'expérience et se mettre à la place de l'usager en situation d'exclusion est bien souvent un exercice formateur

L'accès au services publics est un enjeu majeur de démocratie. Depuis plusieurs années, nombreux services sont passés ou sont en cours de passage au numérique. Cela simplifie la vie des usagers, accèlère les temps de traitement... Du moins si on est en capacité d'y accéder.

### Exercice 
Prenez un service public numérique, et mettez-vous en situation d'usage en vous mettant successivement dans différentes situations :

- En situation d'handicap
- Dans une langue étrangère (utiliser la traduction automatique du navigateur par exemple)
- Une autre situation que vous aimeriez tester

Notez vos commentaires au fur et à mesure de votre visite et posez vous les questions suivantes pour vous mettre dans une démarche low-tech :  

- [ ] : Comment faire  : est-ce tout simplement possible, quelles sont les difficultés ?  
- [ ] : Comment faire sans ou autrement : quelles pourraient être les alternatives plus simple ou plus accessibles ?  
- [ ] : Qu'avez-vous ressenti face aux difficultés rencontrées ?  

###### 

:::success
#### Inspiration   
> Visitez le site [Refugeye](https://refugeye.com/) [Refugeye](https://refugeye.com/#qrcode)

L'application [Refugeye](https://refugeye.com/) permet d'échanger avec les autorités, services publics, ONG, ... en utilisant un système de dessin et de pictogramme pour se faire comprendre auprès de son interlocuteur.
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/5a703aeb-b300-4378-bf4b-701e48cb999c.jpg)

_Ce projet a été réalisé par Geoffrey Dorne. Il est le fondateur d'un atelier de design indépendant dans lequel il réalise des projets graphiques, numériques, interactifs. Son but est l’engagement social, citoyen, environnemental avec pour optique de favoriser la diversité du vivant : humain et non-humain._

:::

## Le numérique en réponse à une situation de handicap 
> Visitez le site Be my eyes [Be my eyes](https://www.bemyeyes.com/language/french#qrcode) 

[Be my eyes](https://www.bemyeyes.com/language/french) est une application mobile qui permet aux déficients visuels de faire appel à une communauté de bénévole pour résoudre des difficultés du quotidien. En installant l'application, on est alors susceptible d'être contacter en appel vidéo sur son smartphone pour simplement aider un bénéficiaire à choisir un vêtement, vérifier une date de péremption sur un produit...

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/ff3fb5fb-c8a3-4b3f-8d04-cddfadf475ca.png)


### Exercice
> Visionnez la vidéo de présentation [vidéo de présentation](https://vimeo.com/113872517#qrcode) 

Regardez [la vidéo de présentation](https://vimeo.com/113872517) de l'application. Le système d'aide proposé par l'application est la mise en relation d'un grand groupe de personne afin de résoudre une difficulté dans des délais courts. C'est une forme de **crowdsourcing**.

A partir d'une situation vécue difficile dans laquelle vous vous êtes trouvé en position de blocage :
- [ ] : Décrivez brièvement la situation
- [ ] : Imaginez une solution mettant en oeuvre une forme crowdsourcing numérique

## Apprendre le numérique sans numérique
> Visionnez le site de la société COLORI [COLORI](https://colori.fr/#qrcode) 

La société [COLORI](https://colori.fr/) a créé un programme d'appropriation du numérique à destination des 3-8 ans ... sans utiliser le numérique. Les compétences nécessaires pour bien évoluer dans un monde "numérique" sont aussi des compétences que l'on peut développer dans le monde "réel".

Parcourez le site internet de l'entreprise et :

- [ ] Repérer à travers une activité comment la compétence numérique est acquise sans le numérique (quel moyen pédagogique, quel type de matériel)
- [ ] Imaginez comment adapter ces activités à un public adulte
- [ ] et/ou imaginez une activité formatrice à un usage numérique... sans numérique

:::success
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/0e8f4c29-5cfc-4348-a0cb-6533588eb63c.jpg)

**350 millions de daltoniens dans le monde**

> Visionnez le site de ColorADD [ColorADD](https://www.coloradd.net/fr#qrcode)  

Le daltonisme affecte un grand nombre de personne. Les difficultés rencontrées sont extremement variables, mais lorsque l'usage d'un service repose sur un code couleur, il devient tout simplement inacessible. Pour cela, [ColorADD](https://www.coloradd.net/fr) propose un code visuel qui permet d'identifier une couleur. Ce code commence à se répandre, vous pourrez notamment le retrouver sur de plus en plus de jeu de société.
:::

## Focus : La pratique FALC

> En savoir plus sur la pratique FALC [QrCode](https://www.culture.gouv.fr/Thematiques/Developpement-culturel/Culture-et-handicap/Facile-a-lire-et-a-comprendre-FALC-une-methode-utile#qrcode)  

La [pratique FALC](https://www.culture.gouv.fr/Thematiques/Developpement-culturel/Culture-et-handicap/Facile-a-lire-et-a-comprendre-FALC-une-methode-utile), signifiant "Facile à Lire et à Comprendre", est une méthode de communication conçue pour rendre l'information accessible à tous, en particulier aux personnes ayant des difficultés de compréhension, comme celles avec des handicaps intellectuels, des troubles d'apprentissage, ou celles qui ne sont pas familières avec la langue utilisée. Voici les points clés de cette approche :

1. **Langage Clair et Simple** : Utiliser un langage facile à comprendre, en évitant le jargon, les termes techniques et les phrases complexes.

2. **Phrases Courtes et Directes** : Écrire des phrases courtes, allant droit au but, pour faciliter la compréhension.

3. **Structure Logique** : Organiser le contenu de manière logique et prévisible, en utilisant des titres et des sous-titres clairs pour structurer le texte.

4. **Support Visuel** : Accompagner les textes d'images, de pictogrammes ou de vidéos pour aider à illustrer les concepts et les instructions.

5. **Adaptabilité** : Adapter le contenu en fonction du public cible, en tenant compte de ses besoins spécifiques et de son niveau de compréhension.

6. **Validation et Tests** : Tester le matériel avec le public cible pour s'assurer qu'il est effectivement facile à lire et à comprendre, et apporter des ajustements si nécessaire.

La pratique FALC est particulièrement importante dans les domaines de l'éducation, de la santé, de l'administration publique et des services sociaux, où l'accès à l'information est crucial. En rendant l'information plus accessible, cette méthode contribue à l'inclusion et à l'autonomisation des personnes ayant des besoins spécifiques en matière de compréhension.

:::success

**Produire des document FALC avec l'intelligence artificielle : low-tech ou high-tech ?**

> Visitez le site de la plateforme LIREC[QrCode](http://sioux.univ-paris8.fr/lirec/#qrcode)

Il existe de nombreux services en ligne qui accompagnent à l'écriture claire. Vous pouvez par exemple tester la plateforme [LIREC](http://sioux.univ-paris8.fr/lirec/) qui vous aide à produire des textes FALC.
Parmi ces services, nombres d'entre-eux reposent sur l'utilisation d'une intelligence artificielle, domaine au coeur de la technologie de pointe. 

De la high-tech pour soutenir les pratiques low-tech :  quel est votre sentiment ?
:::

###### 

# Travaux

:::warning
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/f3e92cbd-1ce6-4a29-a02d-a2448f1c3f92.png =25x)
Durée de la séquence : 50mn
:::

## Glossaire collaboratif

> Ce glossaire sera complété tout au long de la formation [glossaire](https://annuel2.framapad.org/p/cs-formation-low-tech-glossaire-a7po?lang=fr#qrcode)

Ajoutez ces trois entrées au [glossaire collaboratif](https://annuel2.framapad.org/p/cs-formation-low-tech-glossaire-a7po?lang=fr) de la session :
- Exclusion numérique
- Inclusion numérique
- Crowdsourcing

Vous êtes le premier ? Ajouter le terme à la suite du glossaire et tenter une définition en une ou deux phrases maximum.

Le terme est déjà présent dans le glossaire ? Proposer des commentaires et des modifications sur la définition proposée. Nous discuterons en session de visioconférence d'une version qui conviendrait à tous.

En complément de ces 2 termes, ajoutez-en d'autres qui vous paraitrait important. 


## Rédiger et partager votre note d'étonnement

Une note d'étonnement est un note dans laquelle un nouvel entrant (il s'agit souvent d'un nouvel entrant dans une entreprise car cette méthodologie est issu du monde RH) décrit quelque chose qui l'a supris lors de ses premiers jours et apporte une vision et une critique constructive de la situation.

Ici vous devez choisir un point qui dans ce module vous a surpris (quelque chose que vous ne connaissiez pas, auquel vous n'aviez jamais pensé...) et de le partager en 1500 caractères maximum avec le reste du groupe. 

Le format de la note est libre. Si toutefois vous souahitez un guide pour la rédiger, vous pouvez vous appuyer sur la trame suivante :  
- Ce qui était conforme à ce à quoi vous vous attendiez
- Ce qui a dépassé vos attentes
- Ce qui vous a étonné (auquel vous ne vous attendiez pas)
- Ce que cela vous inspire, donne envie de faire

***Bonus facultatif*** : Essayer de rédiger une version de votre note en respectant les consignes FALC !  


## Ressources

A la lecture de ce module, lors de vos travaux ou recherches personnelles, vous avez identifié une ressource (livres, site, vidéo...) qui vous semble intéressante ? Enrichissez le recueil de ressources du Labo Low Tech des Centres sociaux. L'édition collaborative de ce document bénéficie ainsi à toute la communauté low-tech !

Pour y contribuer, c'est ici : https://pad.lescommuns.org/Labo-Low-Tech
[Labo low-tech](https://pad.lescommuns.org/Labo-Low-Tech#qrcode)