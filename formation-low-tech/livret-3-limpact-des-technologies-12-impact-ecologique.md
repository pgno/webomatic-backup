---
title: L'impact des technologies (1/2), impact écologique
---

# L'impact des technologies (1/2) : impact écologique

Pour continuer à tisser le lien entre numérique éthique et low-tech, nous allons explorer dans ce module en 2 parties, la question de l'impact du numérique et des technologies en général.

Sur ces sujets, le premier pas est évidemment la prise de conscience de ces impacts. Peut-être êtes-vous déjà familier de certains des éléments et des chiffres qui vont vous être présenter par la suite. Quoiqu'il en soit, nous vous recommandons de les (re)lire de manière attentive et de parcourir les documents annexes qui vous sont proposés. Ils vous permettront de dresser ou compléter le panorama des incidences de notre usage du numérique à une échelle globale. Les liens avec la low-tech se tisseronts naturellement autour de la thématique écologique, mais vous pourrez commencer à éprouver l'interdépendance de ces sujets et des solutions que propose la démarche low-tech.

Les travaux qui vous seront proposés vous aideront par la suite à rattacher ces préoccupations à votre pratique quotidienne.

:::success
Rappel : notre plan de formation
- [x] : Journée de lancement, introduction
- [x] : L'accessibilité, un numérique simple pour tous
- [x] : **L'impact des technologies (1/2) : impact écologique**
- [ ] : L'impact des technologies (2/2) : impact social et économique
- [ ] : Faire soi-même, avec les autres
- [ ] : La durabilité technologique
- [ ] : L'utilité technologique 
- [ ] : Journée de clôture
:::

###### 

# Introduction

:::warning
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/f3e92cbd-1ce6-4a29-a02d-a2448f1c3f92.png =20x)
Durée de la séquence : 30mn
:::

Dans le contexte actuel, où la technologie et le numérique sont omniprésents, il est crucial de comprendre leur impact environnemental. Ces technologies, qui façonnent notre quotidien, notre économie et notre société, sont à la fois une source d'innovation et de progrès, mais aussi de préoccupations écologiques croissantes.

## Le paradoxe technologique

La révolution numérique, tout en offrant des solutions pour optimiser l'utilisation des ressources et réduire certains impacts environnementaux, génère elle-même une empreinte écologique non négligeable. Cela crée un paradoxe où les technologies qui pourraient aider à résoudre des problèmes environnementaux sont également des contributeurs significatifs à ces problèmes.

### Croissance exponentielle et conséquences environnementales

La croissance exponentielle des technologies et du numérique se traduit par une augmentation massive de la production d'appareil électronique et de la demande en énergie, principalement pour alimenter les infrastructures telles que les centres de données, les réseaux de télécommunication et les appareils individuels. Cette demande accrue se heurte à l'impératif de transition vers des sources d'énergie renouvelables et moins polluantes.

Lister l'ensemble des impacts du numérique sur l'environnement serait une tâche longue et complexe ! Voici deux exemples marquant dont l'impact semble parfois caché aux utilisateurs finaux, soit parce qu'il relève des infrastructures du numérique, ou bien que leur utilisation se soit tellement banalisé que l'on en vient à oublier le poids qu'il représente dans la balance énergétique des usages numériques : 

- **Data Centers**: Les data centers consomment une grande quantité d'électricité pour alimenter et refroidir leurs serveurs. En 2020, ils ont consommé environ 200 térawattheures (TWh), soit environ 1% de la consommation mondiale d'électricité (Source: International Energy Agency). Cette consommation est accentuée par la demande croissante de services cloud et de data mining.

- **Streaming Vidéo**: Le streaming vidéo, particulièrement populaire, est également un gros consommateur d'énergie. Des études montrent que le streaming vidéo génère environ 300 millions de tonnes de CO2 par an, ce qui représente environ 1% des émissions mondiales (Source: Shift Project, 2019). Ce chiffre est dû à la quantité d'énergie nécessaire pour stocker, transmettre et lire des vidéos sur des appareils électroniques.

> Découvrez les secrets de fabrication du site solaire de Low tech Magazine [](https://solar.lowtechmagazine.com/fr/about/the-solar-website/#qrcode)
 

:::success
**Inspirant : Un site web alimenté à l'energie solaire**
Le site https://solar.lowtechmagazine.com est auto-hébergé par son créateur et alimenté à l'énergie solaire. Regardez bien la couleur de fond du site internet, il évolue en fonction du niveau de charge de la batterie qui alimente le serveur !
Dans cet [article](https://solar.lowtechmagazine.com/fr/about/the-solar-website/), l'auteur explique les moyens et contraintes autour de ce choix.

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/8311e3ce-56a9-472f-afc4-fab93f26edc8.png)

**Vous souhaitez vous-même évaluer la performance de votre site internet ?** L'association [Green IT](https://www.greenit.fr/) à développer un indicateur d'impact environnemental des sites internet. Il est disponible sur le site https://www.ecoindex.fr/. Alors quel est votre score ?
:::

######
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/33f46363-f0e9-40f1-8964-7fa8d90bf28a.png#fullpage)

######


### Défis de la gestion des déchets

Un autre aspect critique est la gestion des déchets électroniques. L'obsolescence rapide des appareils, alimentée par le cycle constant d'innovation et de mise à jour des produits, conduit à une accumulation rapide de déchets, posant des défis significatifs en termes de recyclage et d'impact sur l'environnement.

> Le numérique est à l'origine de nombreux déchets à toutes les étapes de son cycles de vie : fabrication, usage et fin de vie.

Dès leur fabrication, les appareils électroniques (smartphones, ordinateurs, tablettes, etc.) constituent une part importante de la consommation énergétique. La production d'un seul smartphone, par exemple, génère environ 55 kg de CO2 (Source: McMaster University).

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/144f58b0-8835-4d93-8436-38c6a1e63882.png#center)


Par ailleurs, la croissance rapide de la technologie entraîne également une augmentation du volume des déchets électroniques. En 2019, le monde a produit un record de 53,6 millions de tonnes de déchets électroniques, avec des taux de croissance annuels de 3-4% *[Global E-waste Monitor 2020]*. Les déchets électroniques se composent d'un large éventail de matériaux, certains précieux comme l'or, l'argent et le cuivre, et d'autres toxiques comme le plomb, le mercure et les retardateurs de flamme bromés. Cette diversité rend le recyclage plus complexe et coûteux. 

En effet, le recyclage des déchets électroniques reste un défi majeur. Seuls 17,4% des déchets électroniques ont été correctement collectés et recyclés en 2019, ce qui signifie que la majorité des matériaux précieux et toxiques qu'ils contiennent sont soit perdus, soit mal gérés *[Global E-waste Monitor 2020]*. Les e-déchets mal gérés peuvent libérer des substances toxiques dans l'environnement, affectant la qualité de l'eau, de l'air et du sol, et ayant un impact direct sur la santé humaine, notamment dans les régions où le recyclage est effectué sans mesures de protection adéquates.

De nombreux pays manquent d'infrastructures adéquates pour gérer et recycler les déchets électroniques de manière sûre et efficace. On assiste en parallèle à une exportation illégale des déchets électroniques vers des pays moins développés pose de graves problèmes environnementaux et sanitaires, en raison du manque de réglementations et de moyens pour un traitement adéquat.

:::success
**De très nombreuses ressources à disposition !**
En regard de l'importance de cette thématique, de nombreux acteurs ont produit des rapports extrêmement détaillés sur l'impact environnemental des technologies. En voici, quelques uns dont une partie des informations des ce module ont été extraites : 
- [Evaluation de l'impact environnemental du numérique en France et analyse prospective](https://librairie.ademe.fr/consommer-autrement/5226-evaluation-de-l-impact-environnemental-du-numerique-en-france-et-analyse-prospective.html) _ADEME, 2022_
- [En route vers la sobriété numérique](https://librairie.ademe.fr/consommer-autrement/5086-en-route-vers-la-sobriete-numerique-9791029718755.html#/44-type_de_produit-format_electronique) _ADEME, 2022_
- [Déployer la sobriété numérique](https://theshiftproject.org/article/deployer-la-sobriete-numerique-rapport-shift/). _The Shift Project, 2020_
- [The Global E-waste Monitor 2020](https://ewastemonitor.info/gem-2020/). _Unitar, 2020_
:::


## High Tech, Low Tech : une complémentarité ?
> "La low tech ne cherche pas nécessairement à remplacer ou exclure le numérique ou la high tech, mais à créer un équilibre soutenable"

Les principes même de la low-tech vise à réduire l'impact écologique de nos technologies. Pour autant, la low tech ne cherche pas nécessairement à remplacer ou exclure le numérique ou la high tech, mais à créer un équilibre soutenable. En intégrant des solutions low tech dans les systèmes numériques, on peut réduire la consommation énergétique et l'empreinte carbone tout en conservant les bénéfices du progrès technologique. Des solutions hybrides peuvent être développées. Par exemple, des centres de données utilisant des méthodes de refroidissement naturelles ou des serveurs moins énergivores.

### Réduction de l'Impact Écologique

Les technologies low tech se concentrent sur la réduction de la consommation énergétique. Par exemple, l'utilisation de dispositifs et de réseaux moins gourmands en énergie ou l'optimisation des systèmes existants pour améliorer leur efficacité énergétique, des appareils numériques avec des fonctionnalités réduites et une conception simplifiée pour minimiser la consommation d'énergie et les coûts de production.

Ces technologies nécessitent souvent moins de ressources rares ou non renouvelables, ou réutilise du matériel existant, ce qui réduit l'impact environnemental lié à l'extraction et au traitement de ces matériaux.

Elle favorise par ailleurs la durabilité des appareils. En concevant des produits plus robustes et facilement réparables, la durée de vie des équipements électroniques peut être prolongée, réduisant ainsi la production de déchets électroniques.

> Les réseaux low-tech cubains : [](https://lowtechlab.org/fr/actualites-blog/nomade-des-mers-la-havane#qrcode)
 

:::success
**Des réseaux locaux low-tech**  

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/44ef662e-8713-45ed-9427-6f4f30b75d6a.png)_L'un des nœuds centraux du réseau SNET © Nesto Siré_
Cette image nous montre des éléments d'un réseau privé appelé SNET, pour Street Network, qui fut longtemps en usage à la Havane, bâti sur du matériel récupéré et bricolé. La visité guidée par le Low-tech Lab, c'est ici : 
https://lowtechlab.org/fr/actualites-blog/nomade-des-mers-la-havane
:::

### Éducation et sensibilisation

> Le site Notre environnement du gouvernement propose un ensemble riche de ressources : [](https://www.notre-environnement.gouv.fr/#qrcode)

La low tech apporte un ensemble de solutions techniques pour limiter les impacts écologiques des technologies. Cependant, la démarche low tech ne doit pas seulement être vu comme une "alternative", notamment au numérique. C'est aussi un processus positif qui permet d'accompagner un changement dans la mentalité des consommateurs et des entreprises, valorisant la durabilité, la réparabilité et l'efficacité plutôt que la consommation et l'obsolescence.


:::success
**Dark side of the web : passez au mode sombre !**

Le mode sombre (dark mode en anglais) est un choix de couleur (ou thème) appliqué aux icônes, textes et interfaces homme-machine affichées sur un fond sombre d'un site internet ou d'un système d'exploitation. Ceette option est maintenant largement répandue sur les navigateurs et les téléphones mobiles. Elle permettrait de réduire la fatigue visuelle, mais surtout de réduire de manière importante, en fonction de cas, la consommation énergétique de nos appareils.

Selon une [étude](https://www.purdue.edu/newsroom/releases/2021/Q3/dark-mode-may-not-save-your-phones-battery-life-as-much-as-you-think,-but-there-are-a-few-silver-linings.html) réalisée en 2021 par l'université de Purdue, si vous utilisez une luminosité maximale la plupart du temps, vous pourriez bénéficier d'un gain de batterie allant jusqu'à 40 % en utilisant le mode sombre (jusqu'à 9% en luminosité automatique).

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/2c1f7290-7d3c-4e14-9ebe-77c1aa036918.png)
_Sur le site https://www.notre-environnement.gouv.fr/, une autre riche source d'information, on vous propose le mode sombre dès l'accueil_
:::


### Un terme clé : la sobriété

High-tech ou low-tech, les technologies n'apporteront de réelles solutions à la problématique environnementale que si elles sont reliées à des mesures importantes de **sobriété**. En dehors de la sphère technologique, il existe de nombreux mouvements et méthodologies qui permettent d'initier des démarches où l'on peut imaginer des équilibres positifs entre le développement des sociétés et la préservation de nos environnements.

:::success
L'approche low tech offre une perspective prometteuse pour réduire l'impact écologique du numérique. En favorisant des technologies plus simples, plus durables et plus efficaces, il est possible de concilier progrès technologique et responsabilité environnementale. La clé réside dans l'équilibre entre innovation high tech et principes low tech, visant une intégration harmonieuse qui respecte à la fois les besoins humains et les limites écologiques de notre planète.
:::

# En pratique

:::warning
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/f3e92cbd-1ce6-4a29-a02d-a2448f1c3f92.png =20x)
Durée de la séquence : 30mn
:::

## Sensibiliser

Pour ces premiers travaux, nous vous proposons quelques outils qui permettent de faire prendre conscience de l'impact écologique des outils techniques, notammament à travers la consommation énergétique. 

:::warning
Choisissez un des 3 outils ci-dessuus et testez-le en solo, avec vos collègues ou lors d'une animation sur votre lieu de travail... A la suite de votre test, posez-vous les questions suivantes : 
- [ ] Avez-vous vous-même appris quelque chose ?
- [ ] Trouvez-vous cet outil adéquat pour sensibiliser à l'impact écologique de nos pratiques numérique ?
- [ ] Que pourriez-vous y ajouter ? Comment le modifiriez-vous pour le pratiquer auprès de vos publics ?
:::

### Revolt, le jeu

> Le jeu Revolt à télécharger et imprimer : [](https://la-revolt.org/#qrcode)

*Si nous devions vraiment pédaler pour produire notre énergie, combien de temps faudrait-il y passer ?*, c'est la question que pose ce petit jeu de carte à réaliser soi-même. 47 cartes proposent 47 objets du quotidien, dont des objets numériques. Tour à tour, les joueurs doivent placer une carte sur la table, et estimer si la consommation de l'objet représenté est supérieur ou inférieure aux objets déjà présents. La réponse, au dos de la carte, permet de vérifier le classement en donnant la valeur équivalente en heure de vélo !

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/96f66b71-300d-4a48-8e34-eef93967b7ff.png#center =300x)


Vous pouvez télécharger le jeu à imprimer, ainsi que ses règles à l'adresse suivante : https://la-revolt.org/

### Carbonalyser 

> L'extension Carbonalyser est disponible sur le site du Shift Project : [](https://theshiftproject.org/carbonalyser-extension-navigateur/#qrcode)

L’extension de navigateur (ou add-on) « Carbonalyser » mise à disposition par The Shift Project vous permet de visualiser la consommation électrique et les émissions de gaz à effet de serre (GES) associées à votre navigation internet.

Pour chiffrer ces impacts, le programme :

- Comptabilise la quantité de données transitant via le navigateur,
- Traduit ce trafic en consommation électrique (via le modèle « 1byte » développé par The Shift Project),
- Traduit cette consommation électrique en émissions de CO2e selon la zone géographique choisie.

Vous pouvez obtenir plus d'information et télécharger l'extension à l'adresser suivante : https://theshiftproject.org/carbonalyser-extension-navigateur/

###### 

### Le bureau des bonnes habitudes

> Une journée pour tout changer ? C'est par ici : [](https://multimedia.ademe.fr/infographies/infographie-tout-changer/#qrcode)

Embouteillages, pause déjeuner, réunions, emails, café, photocopieuse… si vous faites partie de la moitié de la population active en France qui occupe un emploi de bureau, vous connaissez sûrement cette routine par cœur. Mais avez-vous déjà pensé aux effets que vos habitudes ont sur notre planète ? Sans que l'on s'en doute, elles ont de grandes conséquences sur notre environnement.

Cette infographie proposée par l'ADEME fait le point sur les habitudes qui impactent l'environnement au bureau et donne des solutions pour les réduire.

https://www.qqf.fr/infographie/le-bureau-des-bonnes-habitudes-une-journee-pour-tout-changer/

Sur cette même thématique, vous pouvez consulter l'infographie "Une journée pour tout changer" : 
https://multimedia.ademe.fr/infographies/infographie-tout-changer/



## Un projet de service numérique

> L'Ademe édite un guide de questionnement pour le développement des services numérique disponible en ligne [](https://librairie.ademe.fr/dechets-economie-circulaire/4739-guide-de-questionnement-pour-le-developpement-de-services-numeriques.html#qrcode)

Nous vous proposons de consulter le guide suivant : [Guide de questionnement pour le développement de services numériques](https://librairie.ademe.fr/dechets-economie-circulaire/4739-guide-de-questionnement-pour-le-developpement-de-services-numeriques.html).

:::warning
Remémorez-vous un projet auquel vous ayez participer impliquant le développement d'un service numérique. Si vous aviez eu connaissance de ce guide à cette occasion, diriez-vous que :  
- [ ] Cela n'aurait rien changé au projet
- [ ] On aurait modifier suelement certains aspects du projet
- [ ] Cela aurait radicalement changer la manière de l'aborder et de le conduire
- [ ] Nous aurions choisi une solution alternative à ce projet
:::

###### 

### Focus : La fresque du numérique

> Découvrez la fresque du numérique [](https://www.fresquedunumerique.org#qrcode)

La Fresque du Numérique est un atelier ludique et collaboratif d'une demi-journée avec une pédagogie similaire à celle de La Fresque du Climat. Le but de ce "serious game" est de sensibiliser et former les participant·es aux enjeux environnementaux du numérique.

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/7fec0dad-eb72-4f87-9b84-6973ee01d6f9.png)

Des ateliers sont régulièrement animés en ligne ou en présentiel. Il suffit pour cela de se rendre sur le site https://www.fresquedunumerique.org. Ceux qui le souhaite peuvent aussi suivre une formation leur permettant ensuite de devenir animateur de cet outil.


# Travaux

:::warning
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/f3e92cbd-1ce6-4a29-a02d-a2448f1c3f92.png =20x)
Durée de la séquence : 60mn
:::

## Glossaire collaboratif

> Ce glossaire sera complété tout au long de la formation [glossaire](https://annuel2.framapad.org/p/cs-formation-low-tech-glossaire-a7po?lang=fr#qrcode)

Ajoutez ces deux entrées au glossaire collaboratif de la session :
- Sobriété numérique
- Obsolescence 

- Vous êtes le premier ? Ajouter le terme à la suite du glossaire et tenter une définition en une ou deux phrases maximum.
- Le terme est déjà présent dans le glossaire ? Proposer des commentaires et des modifications sur la définition proposée. Nous discuterons en session de visioconférence d'une version qui conviendrait à tous.

En complément de ces 2 termes, ajoutez-en d'autres qui vous paraitrait important. 

## Un projet de sensibilisation

Nous vous proposons de vous projeter dans la création d'un projet qui mettrait en oeuvre une solution low-tech en réponse à certaines problématiques environnemental évoquées plus tôt.

Au choix, imaginez un projet low-tech en réponse à : 
- La gestion des déchêts electroniques 
- La consommation énergivore des solutions de streaming/VOD

Il ne s'agit pas d'écrire un projet de toute pièce ! Nous vous proposons de suivre ces quelques consignes : 
- Isolez un angle particulier de la problématique (par exemple pour la gestion des déchêts : comment les éviter, comment les réduire, comment les retraiter... )
- Choisissez un public cible et tentez de repérer ce qui les attachent au sujet (à travers une situation de la vie quotidienne par exemple)
- Identifiez le ou les angles d'approche de la low-tech que vous souhaitez mettre en oeuvre
- Décrivez en quelques lignes chaque étapes de votre projet, incluant un temps de sensibilisation, un temps d'identification/production de la solution, un temps de bilan/restitution

Ce projet n'a pas vocation à être réalisé, profitez-en, soyez créatif ! Vous déposerez la note d'intention de votre projet sur le canal de la formation.

## Ressources

Ce module est riche en ressources (rapport, outils, liens, etc...) et il en existe bien d'autres. Continuez à enrichir le recueil du Labo Low-tech soit en ajoutant une ressource, soit en améliorant sa documentation (ajouter une photo, une capture d'écran, un petit résumé, etc..)

Pour y contribuer, c'est ici : https://pad.lescommuns.org/Labo-Low-Tech