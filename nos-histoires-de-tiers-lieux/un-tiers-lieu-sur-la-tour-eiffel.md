# Un tiers-lieu sur la Tour Eiffel

*Librement inspiré de l’atelier : Patrimoine architectural ou industriel : les enjeux vus par les professionnel.le.s*

### Prologue

> C’est un bien étrange appel à projet que vous venez de recevoir : faire tiers-lieu sur la Tour Eiffel ! Si vous le remportez, il vous faudra tirer parti de ce monument historique qui sera mis à votre disposition : une grande hauteur sous plafond :-), un restaurant tout équipé, une antenne radio et une vue imprenable.
> 
> Vous êtes convaincu d’avoir toutes les cartes en main pour remporter ce projet. Il est temps de démarrer votre réponse et de mettre en avant les idées  qui feront mouche. Voici comment l'on va s'y prendre !

### Chapitre un

La tour eiffel, c'est du solide ! Mais 300m d'acier dressé vers le ciel, ça manque un peu de verdure. Premier coup de fil aux amis arboristes, paysagistes, botanistes... On démarre par des plantations massives aux quatre pieds de la grande dame de fer. Les pilliers sont ensuite recouverts d'un mélange nutritifs favorables au développement des mousses et autres champignons, de ceux qui trouvent toujours un moyen de pousser là où on ne les attends pas. Très vite, le bâtiment est peuplé de milliers d'individus de toute espèces, du micro-organisme aux oiseaux de passage. Le deuxième étage est dédié aux plantes nourricières et autres légumes qui seront distribués aux occupants et serviront au restaurant.

Ok, maintenant que l'on a ramené un peu de vie sur site, la prochaine étape va être... 

### Chapitre deux 

...de faire une énorme fête pour lancer officiellement les hostilités. Quand les locaux et les vagabonds auront vécu une expérience inoubiable au milieu, sur, et sous l'acier au coeur des jardins suspendus, la suite se mettera en place de manière organique. Pour une bonne grosse teuf : des musiciens qui ont pas trop préparé, des théâtreux en mode carte blanche, des dj, des distributeurs de plantes médicinales et galvanisantes, une alliance avec les 12 microbrasseries du coin, des aventuriers photographes et vidéastes prêts à prendre des risques. Le jour J, les chômeurs, les inactifs compulsifs, les retraités et les chanceux de la semaine de 4 jours se rassemblent à 7h et ont pour mission de customiser le lieu avec les déchets qu'ielles trouveront dans le quartier. A 16h les portes s'ouvrent aux familles et les festivités commencent avec les enfants aux manettes. A 22h une chasse à l'enfant est organisé, ils sont évacués et les activités à fortes sensations peuvent se lancer.
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/dd68619a-91d1-4c84-98d3-7f32497c5c20.png)
La fête était un succès, tout le monde repart avec l'envie de changer de vie. Oui il y a eu quelques blessés mais les medics ont gérés, quelques crises existentielles mais les medics ont gérés, donc tout va bien en fait. Désormais... 

### Chapitre trois

...le projet est lancé, les médias ont largement relayé le bordel et le projet fait des émules. Des architectes ecrivent pour proposer des projets d'adaptation de l'existant, des collectifs ecrivent pour proposer de prendre en charge tout ou partie de ce qui va se passer sur place, on est un peu dépassés, il faut s'organiser.

Première étape, recruter une equipe de gens motivés pour structurer tout ça...

### Chapitre quatre

Alors on fait quoi ? Un AMI? un AAP? Un marché à bon de commandes? Rien de tout ça ! On invite large !
C'est bien beau d'avoir prévu des plantes et des teufs mais il nous faut constituer la communauté. On invite large donc, en s'appuyant sur les conseils de quartiers, les centres sociaux, les réseaux d'assos, l'office de tourisme ( hé! on est à Paris ou quoi ?! ): on veut des habitants du Grand Paris, des partenaires techniques et financiers, des touristes, des pauvres et des riches pour constituer la future programmation du lieu. L'animation sera bien sûr ouverte et coopérative, nous sommes encore dans le ciel des idées.

Au terme de cette journée nous avons donc une proto communauté et plein d'idées pour mettre en route la Tiers-Eiffel. Comment on engage la suite ? 