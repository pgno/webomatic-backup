# Pour finir 

Si cette parenthèse festive vous a donné goût à la documentation, ne restez pas seul car désormais vous avez participé à la création d'un commun. Discodoc est une ressource partagée autour du projet Movilab, comme de nombreuses ressources où se rejoignent les contributeurs du faire tiers-lieu. 

La création de ce livret a été soutenu par la Compagnie des tiers-lieux qui contribue à diffuser la culture et les pratiques des communs. Au delà des mots et des intentions, accélérons le passage à l'action. 

Pour cela, l'association propose aux membres un accompagnement vers les ressources libres et ouvertes qui facilitent le *faire tiers-lieux*. Notre objectif est de prendre du temps pour étudier vos principaux projets, défis et difficultés et vous orienter vers les communs qui peuvent apporter des réponses. Nous nous engageons ensuite à vous suivre dans l'accès aux ressources, à vous filer un coup de main et à vous inviter à contribuer aux communs et à devenir membre de ces communautés.

La Compagnie des tiers-lieux souhaite soutenir les démarches collectives dans un premier temps. **Si vous êtes membre du réseau et que vous parvenez à fédérer sur un sujet partagé, interpellez la Compagnie pour déclencher cet accompagnement aux communs.**
