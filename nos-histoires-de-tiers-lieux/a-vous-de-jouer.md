# A vous de jouer

Envie de continuer dans vos propres lieux. Voici deux autres pistes pour libérer la parole et les imaginations dans votre communauté !


## Les vacances en tiers-lieux
*Très librement inspiré (avec un mauvais jeu de mots :-)) de l'atelier : Tiers-lieux en « vacance »*

Vous pensez tiers-lieu, vous mangez tiers-lieu, vous rêvez tiers-lieu ! C'est votre leitmotiv, votre crédo, votre passion... mais que votre famille ne semble pas partager avec la même intensité ! Du coup c'est décidé, cette année, **vous amenez tout le monde en vacances... dans un tiers-lieu** !

Les bagages sont prêts, tout le monde (à part vous) fait la tête. Vous profitez de la route pour tenter de motiver les troupes...


## Ma maison est un tiers-lieu
*Inspiré de  l'atelier : Un tiers-lieu dans ma maison, quel projet (de vie) ?*

Vous pensez tiers-lieu, vous mangez tiers-lieu, vous rêvez tiers-lieu ! C'est votre leitmotiv, votre crédo, votre passion... mais que votre famille ne semble pas partager avec la même intensité ! Du coup c'est décidé, cette année, **vous transformez votre maison en un tiers-lieu** !

C'est le moment de planifier : le garage : un fablab ? le grenier : un espace de coworking ? Le jardin : une AMAP ?  Les idées fusent... les regards en coin de votre famille, vos colocs,... aussi ! Il va falloir les convaincre. Voici comment vous allez vous y prendre....

