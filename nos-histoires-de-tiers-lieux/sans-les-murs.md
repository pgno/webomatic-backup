# Sans les murs

*Librement inspiré de l’atelier : Comment faire tiers-lieu sans les murs ?*
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/949410cb-edc0-4af4-b45d-bff83d880e20.png)


### Prologue

> 5 ans maintenant que votre communauté fait vivre son tiers-lieu dans ce grand bâtiment. Celui la même qui vous a servit à démarrer votre projet, à mettre un toit au dessus de la tête de vos membres, celui qui a abrité une multitude de projets et de rencontres…
> Seulement vous avez pris aujourd’hui une grande décision : celle de faire tomber les murs. Plus de toit, de cloisons, d’entrée ou de sortie, un tiers-lieu à ciel ouvert, sans limite ni frontière. L’heure est venue de faire vivre votre tiers-lieu dans son plus simple appareil !

######

### Chapitre un

C'était donc le chêne de la cour qui, en lieu et place du toit, abritait désormais notre communauté. On pouvait accueilir vachement plus de monde qu'avant et stocker vachement plus de trucs aussi. Tous les projets étaient possibles ou presque du moment que le chêne tenait bon et que le thermomètre ne plongeait pas trop. Ca nous laissait un peu de marge pour faire volte-face et s'adapter ensemble, finalement, c'était ça faire tiers-lieux ! Et puis nous n'avions plus de loyer à payer, d'assurance, de facture d'eau et d'electricité, ça nous laissait les mains libres pour les factures d'élagage et le renouvelement du stock de plaids. Le ciel était devenu notre demeure et un vent (dont le fond était indéniabement frais) flottait sur notre communauté, agitant les esprits et ventilant les immaginations...

### Chapitre deux

Flottait, flottait... justement, on avait tous très peur, en vérité, qu'il se mette à nous flotter sur la gueule. Si le ciel ne risquait pas de nous tomber sur la tête, la pluie si.
Comme toujours, c'est F., notre leader charismatique inavoué qui pris la parole pour guider la petite foule que nous étions vers des préoccupations moins terre-à-terre. 
" Tiers-Lieux sans lieu, mais alors quoi ? à défaut de pouvoir nous libérer totalement du langage, ne devrions-nous pas d'abord réinventer un mot pour nous sortir de cette panade absurde de ne pas être ce que - étymologiquement même - l'on attendrait de nous ? Pour rédéfinir nos territoires d'intervention bien au-delà des vulgaires limitations topographiques et matéralistes..."
Comme toujours avec lui, la réponse était dans la question. 
Alors même si on se pelait méchamment les miches, nous nous sommes mis à brainstormer en plein air pour trouver comment on pourrait appeler le truc qu'on était en train d'essayer de faire.
On était tous d'avis qu'il fallait garder "tiers", ça faisait déjà la moitié du boulot en moins.
Par quoi remplacer le mot "lieu" ? Par collectif ? groupe ? communauté ? initiative ? 
Non, ça sonnait pas, c'était pas folichon. 
On avait beau se creuser les méninges, on tournait en rond. Comme quoi, y a pas que les bâtiments qui ont des murs. 
Soudain, S. - dont l'intense concentration était visible à ses sourcils vilainement froncés depuis le début de la conversation - éleva la voix pour dire dans un souffle : "Utopie ! U-topie : l'absence de lieu, étymologiquement. C'est ça : on est une tiers-utopie en fait !" 
M., omnibulée depuis des mois par la bascule de toute notre communication vers l'écriture inclusive, s'empressa d'applaudir : "Génial, en plus c'est au féminin !"
Mouais, n'empêche qu'un rapide tour des visages nous permettait de réaliser qu'on était pas tous.tes complètement convaincu.es. 
Il fallait trouver mieux. Il faut toujours trouver mieux. 
Et d'ailleurs, avec ou sans murs, c'est assez fatigant.

Cependant on n'était pas du genre à renoncer en chemin. On se remobilisa donc collectivement sur cette épineuse question : comment appeler un tiers-lieux sans lieu ?

### Chapitre trois

On peut l'appeler place, espace.... par exemple en alimentation on utilise le vocable "places à vivreS" (cf. Les Anges Gardins, Ménadel et St Hubert à Loos en Gohelle). Sous ce nom peuvent être rassemblés tous les lieux qui contribuent à créer des espaces communs autour de fonctions alimentaires, que ce soit un jardin en pied d'immeuble, une épicerie, un bar, une zone maraichère, un resto, une cuisine partagée, un espace test agricole.

### Chapitre quatre

Bref, un endroit où l'alimentation a pour fonction de rassembler autour du vivre ensemble et autour des vivreS (nourriture, boisson). Et tout ça rassemblait, ça faisait tiers-lieu! Ce tiers-lieu à ciel ouvert offrait toutes les possibilités de monter des structures temporaires et de les démonter très facilement en fonction des besoins : un temps serre à la fin de l'été, le tiers-lieu se meut en chapiteau pour l'hiver pour se dévétir totalement aux prémices de la belle saison et s'orner d'éclairage solaire.
La communauté fabriquait du mobilier avec des matériaux récupérés.

### Chapitre cinq

Après avoir fabriquer une chaise, la pluie se mis à tomber. L'ensemble des membres rentrèrent chez eux se mettre  à l'abri sauf un qui resta assis là pensif. Il se dit alors : "Et donc tout ça pour ça !"

Et la vie reprit son cours apaisé.






