# Exquis !

## Cadavre exquis

Nous avons proposé aux plus intrépides de participer à l'écriture de cadavres exquis sur des scénarios inspirés de la programmation de l'évènement.

La consigne est simple : chacun prend la suite de son prédécesseur pour enrichir le récit. Pas d'autres limites que le fruit de leur imagination. 

> Inspirez-vous de cet atelier. Documenter peut être intimidant. Mais qui à dit que cela devait toujours être sérieux. Ici pas de mode d'emploi interminable ou de notice IKEA, mettre la main à un récit collectif, c'est mettre le doigt dans la documentation !
> Vous pouvez repartir des thèmes de ce livret (deux autres vous sont proposé plus loin) ou inventer ceux qui font sens dans votre communauté.

