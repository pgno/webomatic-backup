
# Brèves de tiers-lieux

## Carlotta,	Compagnie des Tiers-Lieux
### Son anecdote
"Je veux de l'amour, du plaisir, de la confiance dans cet espace" Ces mots, ce sont ceux de Guy, 75 ans, habitant d'une résidence autonomie à Barlin (Pas-de-Calais) qui projette l'installation d'un tiers-lieu dans ses murs pour s'ouvrir vers le quartier, offrir un espace intergénérationnel à toute la communauté. J'ai eu l'immense honneur d'accompagner au cours de 2 ateliers l'émergence des valeurs que souhaite incarner ce collectif et ce qu'iels souhaitent vivre dans un espace tiers-lieu. Je suis toujours émue de partager ces moments de vie, de découverte et de partage avec toutes les personnes, quelque soit leur âge, leur genre, leur apparence... C'est ça pour moi les tiers-lieux : la diversité, se rencontrer et s'apporter mutuellement dans la joie et la bonne humeur !	
### Son slogan pour les communs
> Commun Ouragan ! (l'effet des tiers-lieux sur nos vies, nos coeurs <3)


## Mihaela, Chez violette
### Son anecdote
La discussion permanente de qui fait le menage, qui fait la vaisselle, qui achète le PQ	
### Son slogan pour les communs
> Les Communs c'est cool, mais personne ne le sait

## Antoine, RFFLabs
### Son anecdote
Je suis un maker, quelqu'un qui fait par lui-même et avec les autres pour presque tout fabriquer. Depuis 2016 que j'ai découvert les Labs, j'ai toujours voyagé pour visiter des lieux (plus de 400 Espaces du Faire visités maintenant) afin de faire une passerelle entre les lieux, nous inspirer et faire ensemble. Je me souviens d'une visite à Paris de 8 lieux sur une journée, une aventure en soit ! Nous étions partis avec Andy du Petit FabLab de Paris de Volumes, quintessence des labs archi et bobo jusqu'à l'ElectroLab, le lieu symbole des geeks barbus. Nous avions passé les mondes en moins d'une journée et une panne de voiture sans jamais nous sentir exclu, toujours pour faire ensemble. C'est ce jour là où je me suis dis que ce que nous faisions dans toute notre plus belle diversité nous contribuons à changer le monde. Tout cela avec un ami proche, ce jour-là j'avais fais par moi-même et surtout avec les autres. 
### Son slogan pour les communs
> Maker Ma Gueule !


## Antoine, RoseLab	
### Son anecdote
Savoir rater dans un lab est une joie de tout instant. Comment en galérant ensemble nous arrivons à passer de si beaux moments ? C'est la force de la méthodologie maker. Un des plus grands projets de notre vie a commencé par une erreur, par un défi collectif à réussir une découpe impossible... une galère sans nom pour comprendre que nous devions plus partager. Depuis de cette galère sans nom est née notre soirée mensuelle la 404 la soirée not found pour faire ensemble du RoseLab où nous nous retrouvons pour boire des coups, mettre en valeur des fails, rire et toujours aller plus loin ensemble. 

######

### Son slogan pour les communs
> Faire Ensemble pour contribuer à changer le monde.

## Cécile, L'Octopus 	
### Son anecdote
Premier tiers-lieu que j'ai visité dans le village de mes grands-parents à Cunlhat dans le Puy-de-Dôme. Première rencontre avec un bar associatif, une friperie participative, une communauté porteuse d'une autre proposition de vie sociale et engagée dans une transition sociale très à la marge sur ce territoire fortement paysan. 	
### Son slogan pour les communs
> Jouer le jeu des communs, c'est jouer collectif, c'est participer à un monde plus humain ! Si tu la joues commun c'est que tu as compris le monde de demain !

## Benoît, La Fabrique à communs des tiers lieux
### Son anecdote
L'espace de création et de coopération que nous avons bâti est tellement tiers-lieux que certaines personnes qui nous rejoignent passent plus de temps à se demander où elles sont, pour qui elles bossent et qu'est-ce qu'elles ont le droit de faire plutôt qu'à bosser avec nous. Est-ce que c'est une bon signe, peut-être... Est-ce que c'est pratique, pas du tout.	
### Son slogan pour les communs
> Si toi aussi tu trouves que le capitalisme c'est trop facile essaie les communs !

######

## Benjamin, Le Lieu Commun	
### Son anecdote
Nous avions créé un lieu en 2014, un coworking, pour nous, travailler en ayant des "collègues", mais aussi pour accueilli des personnes de passage et des étudiants locaux. Finalement, c'est quelques années après la création que nous avons été sollicité pour la première fois par des étudiants en informatique de l'ULCO. Ils avaient besoin de bureaux ouverts en soirée pour se réunir et participer à un concours international de programmation organisé par Google. Sachant que nous évoluions dans le domaine du numérique, il était évident pour nous de leur proposer notre espace de travail. Les étudiants était ravis de pouvoir participer à ce concours ensemble, même si ils n'ont pas atteint les objectifs. De notre côté, nous avons été surpris de recevoir, et conserver pendant longtemps dans notre lieu, des goodies Google : ballons, fanions et lunettes de soleil !