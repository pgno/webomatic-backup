# Documenter en commun


Ce livret a été produit lors de l'évènement **En compagnie des tiers-lieux**, le 22 novembre 2023, sur le thème *"Foncier, les tiers-lieux investissent le territoire !"*

Ce document est une expérimentation d'écriture collective : produire du commun, c'est produire un patrimoine collectif ! Ecrire, raconter, mettre en récit et en page les histoires de chacun, c'est une forme de documentation à laquelle un groupe de contributeurs de la Fabrique à Communs s'attelent pour rendre accessible au plus grand nombre ce qui nous permet de faire tiers-lieu.

Lors de l'évènement, nous avons recueilli les anecdotes et slogans des participants et les plus intrépides ont pu participer à l'écriture de cadavre exquis. De taille modeste, son contenu est ouvert pour être repris, enrichi, modifié, diffusé à l'envie.

Le présent document est compilé et mis en page à la volée grâce à la plateforme Webomatic et au projet open-source paged.js. Il a été réalisé avec joie et enthousiasme par Perrine, Thomas, et Pierre.

Les contenus sont sous licence Creative Commons CC-BY-SA.