
# Brèves de tiers-lieux (la suite)


## Florine, FABNUM du FCP
### Son anecdote
Un nom d'atelier à la FABNUM ? Harry Plotter et la découpe de feu ? 
### Son slogan pour les communs
Commun comme cochons ! 

## Marie, Plateau fertile 	
### Son anecdote
Mon anecdote brève : Ça fait longtemps que j'évolue dans des fablabs, mon anecdote c'est que c'est un petit monde ! A Paris ou à Lille je croise toujours quelqu'un qui a bossé avec une connaissance ou que j'ai croisé quelque part. Bref, vraiment un petit monde !! 	
### Son slogan pour les communs
Comme un con ! Ne me laisse pas comme un.e con.e sans documentation ! 


## Magali, La Soufflerie
### Son anecdote
Lorsque j'ai franchi la porte de la Soufflerie la première fois, Vyndhia m'a présenté la genèse et la philosophie du tiers-lieu. Comme cela paraissait plein de bons sentiments, plein de belles valeurs, plein d'utopies, j'ai voulu voir concrètement comment cela prenait formes et je suis allée au 1er challenge Mon bonnet rose. J'y ai vécu une expérience fantastique et enthousiasmante. Le tiers-lieu grouillait de personnes qui ne se connaissaient pas et qui parfois avaient parcouru plus de 50km pour être présents quelques heures à couper du tissu, l'assembler, le repasser... ou masser, nourrir, divertir... Venue pour passer une heure à observer, je me suis laisser prendre au jeu. Cette soirée m'a permise de croire à nouveau en l'engagement citoyen et solidaire. Depuis, je crois fermement dans les tiers-lieux et dans l'énergie créée et déployée. Ils contribuent activement au réenchantement !
### Son slogan pour les communs
> Tous ensemble, dans les tiers- lieux, en mode Commun'do *(Crédits : Sébastien Soriano, directeur général d'IGN France)*


## Pauline, Cuisine commune 
### Son anecdote
Chaud bouillon Fives cail,	j'ai participé dans le cadre pro aux ateliers Cuisiniers du monde de la cuisine commune de chaud bouillon ! Des moments géniaux, enrichissants et même, hors du temps! Des rencontres, du partage et de la bienveillance...
### Son slogan pour les communs
> Commun ouragan, rassemble tellement!



## Magali, Le Tok'Ici
### Son anecdote
J'étais attablée suite à un déjeuner avec mon collègue podcasteur Etienne, quand soudain apparu Thomas. On devait se croiser vite fait autour d'un café pour échanger sur notre intérêt commun pour la radio. Nous nous sommes mis à discuter de la radio qu'il a monté dans son quartier (Radio Bois Blancs), puis à imaginer des nouvelles choses, comme une webradio des Communs, dans laquelle on pourrait faire ceci cela, comme ci, comme ça... Quand je regarde ma montre, l'après-midi entière avait filé, j'étais à la bourre sur tous mes projets de la journée. De radio des communs, pour le moment : point. Mais enfin, une connexion est née, une graine d'idée a été semée.... qui sait ce qui pourra un jour arriver ?
### Son slogan pour les communs
> Les communs : c'est vous qui les vivez, c'est nous qui en vivons. ;-) (je rigole, c'est du plagiat)


## Julie, Pas de tiers lieu
### Son anecdote
Je pense ne jamais être allée dans un tiers lieu et je suis ici aujourd’hui, entre autre, pour mieux appréhender cette notion qui je vous l’avoue, me semble très abstraite :)	
### Son slogan pour les communs
> Tiers-lieu, le commun de l’humain et des moyens


## Stéphane, Bazar St So
### Son anecdote
Le Bazar c'est le lieu de la rencontre impromptue, celle qui n'est pas prévue, peut-être un peu espérée mais certainement pas préparée. Et ici aujourd'hui, j'ai fait la connaissance de Nora que j'avais entendu dans une intervention un peu plus tôt aujourd'hui. Et nous avons réfléchis sur le mot "Tiers-Lieu" qui devrait peut être mué en un autre nom pour une plus grande acceptation par Tous.	
### Son slogan pour les communs
> Le Tiers-Lieu le Lieu de la Rencontre !

######

## Nora, La Voisinerie
### Son anecdote
Lors de mon entretien avec le gérant de La Voisinerie de Wazemmes il m'a proposé un café. Sur sa tasse de café il y avait son prénom: Didier et moi je lui dit que bientôt j'aurais ma propre tase avec mon prénom. J'avais un sourire mais en même temps de gêne. Un échange bienveillant. Bien sûr j'ai eu le poste mais pas ma tasse :)	
### Son slogan pour les communs
> Les Hommes font des murs mais les murs ne font pas les hommes


## Rémi, Quai des transition	
### Son anecdote
La vie est une fête
### Son slogan pour les communs
> Un pour tous, tous pour un commun


## Emmanuel, POP Café
### Son anecdote
Acte 1 : en 2020 POP Café a animé un atelier "Donnes-moi tes données" sur les données personnelles de santé avec les habitants de Loos-en-Gohelle, au sein du tiers-lieu Menadel. On s'est dit que c'était une ressource intéressante à partage, et on en a fait un petit commun méthodologique numérique. On l'a mis sur notre wiki et on est passés à autre chose. Acte 2 : en 2023, dans une rencontre de directeurs de CPAM à laquelle on assiste, le représentant du Finistère explique qu'il a formé 200 de ses agents avec un super jeu sur les données de santé : c'était notre commun qui avait vécu sa vie : )	
### Son slogan pour les communs
> Produire des communs c'est super, les diffuser c'est génial, réussir à suivre leur usage et à impliquer les usagers contributeurs finaux, c'est encore mieux mais c'est vraiment pas le plus facile... Il y a commun truc qui manque à ce niveau là.