---
title : Faire tiers-lieu avec le vivant
cover : https://hot-objects.liiib.re/pad-relief-aura-fr/uploads/0aca8511-9e24-43c1-91ff-3932a03edba1.jpg
---
# Mi-lieux: Faire Tiers-lieu avec le vivant/non-humain

- [Accueil](https://pad.relief-aura.fr/ftlalvnv-petit-histoire)
    - [Le démarrage](https://pad.relief-aura.fr/ftlalvnv-demarrage)
    - [Le groupe](https://pad.relief-aura.fr/ftlalvnv-groupe)
    - [Agenda des rencontres](https://pad.relief-aura.fr/ftlalvnv-agenda)
- [Langages/pensées bricolables](https://pad.relief-aura.fr/ftlalvnv-languages)
    - [Mycéliomix](https://pad.relief-aura.fr/ftlalvnv-myceliomix)
    - [Pincette conceptuelle](https://pad.relief-aura.fr/ftlalvnv-pincette-conceptuelle)
    - [Tableau périodique et cosmogénie](https://pad.relief-aura.fr/ftlalvnv-table-periodique)
    - [Licence de co-viabilité](https://pad.relief-aura.fr/ftlalvnv-licence-coviabilite)
    - [Quasi-objet](https://pad.relief-aura.fr/ftlalvnv-quasi-objet)
    - [Naturogramme](https://pad.relief-aura.fr/ftlalvnv-neoprotolangage)
    - [L'espace qui nous lie/sépare](https://pad.relief-aura.fr/ftlalvnv-espace)
    - [Pacte de négociation avec le non-humain/non-vivant](https://pad.lescommuns.org/SPtuKjP4SDaDZMGiOKaM3g#)
    - [Statut associatif associant le non-humain](https://pad.lescommuns.org/Ky9jBW-9QjCXGvIVLepi4Q#)
- [Sujets abordés lors des rencontres](https://pad.relief-aura.fr/ftlalvnv-sujets)
    - [Introduction](https://pad.relief-aura.fr/ftlalvnv-intro)
    - [La charte de l'arbre ](https://pad.relief-aura.fr/ftlalvnv-charte-arbre)
    - [Ethologie](https://pad.relief-aura.fr/ftlalvnv-ethologie)
    - [Zone humide](https://pad.relief-aura.fr/ftlalvnv-zone-humide)
    - [La notion de nature](https://pad.relief-aura.fr/ftlalvnv-notion-nature)
    - [La coviabilité](https://pad.relief-aura.fr/ftlalvnv-coviabilite)
- [Veille et documentation](https://pad.relief-aura.fr/ftlalvnv-veille-documentation)
- [Dévidoir auto-réflexif à visée collaborative](https://pad.relief-aura.fr/ftlalvnv-devidoir)
- [Dans la tête de Maïlis](https://pad.relief-aura.fr/ftlalvnv-dansmatete)