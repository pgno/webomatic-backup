# Zones humides

Rencontre du 28 avril 2023 9h-12h
Présents: Isabelle, Claire Marie, Maïlis, Morgane, Pierre

**Maïlis nous parle de zones humides et coopération internationale**
Interlude sur la question de la responsabilité
Pierre: Valeur, Droit -éthique et morale : la question du language et les pensées bricolable

protéger les oiseaux = protéger les habitats = les milieux humides
=> 1971 signature d’un traité international, 170 parties

valeur économique des zones humides =services rendus mais aussi une valeur culturelle: la rencontre entre eaux douces et eaux salées qui permettent la pèche et plein de choses… mais pourtant on les a toujours détestés négligée et on a cherché a avoir du pouvoir dessus et les faire disparaître. => 87% ont disparu

Maïlis a passé 10 ans à travailler pour faire connaitre et protéger les zones humides notamment par la mise en place de la convention de Ramsar sur le bassin Méditerranéen.
Plus d'informations ici: https://fr.wikipedia.org/wiki/Convention_de_Ramsar

Maïlis a travaillé pour une association MedWet, issu loi 1901, qui a mis en place une collaboration assez innovante:
27 pays membres + des ong membres + des centres de recherche membres

RAMSAR = un label, comme le label des parc nationaux => les gouvernement membres mettent en place les moyens de la protection de la ZH en labellisant leur site et en lui assurant une protection internationale quand souvent la protection nationale ou locale fait défaut.


structure:

    un réseautage entre pays (rencontre tous les 1 ans et demi) = dialogue politique
    alliance méditerranéennes pour les zones humides : ONG pour se battre et se défendre par exemple alerter les médias
    du lobbying et des rapports, inventaires et webinaires

focus sur un projet, soutenu par un bailleur de fond, qui implique des ong, des juristes et des conventions méditérannéennes => se mettre tous ensemble pour produire des documents qui reconnaissent les apports des ZH pour les défendre : https://www.wetlandbasedsolutions.org/


La collaboration s’est faite d’abord par l’interconnaissance pour créer de la collaboration et identifier des valeurs communes et par la recherche et la documentation
le site web et la charte graphique a créé un sentiment de communauté, de collectif.

Plein de documentation ici: https://medwet.org/fr/
le projet a fait des petits: communauté sur l’eau douce

une université des anciens acteurs de la biodiversisté, pour enseigner aux jeunes, biodiv academy