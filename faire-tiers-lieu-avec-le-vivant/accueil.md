# Accueil

**Les Mi-Lieux sont des espaces dehors, où on on aurait enlevé les toîts pour faire **noŪs****


Inspirés du mouvement des Tiers-Lieux les Mi-Lieux cherchent à faire tomber les murs entre les écosystèmes.
Quelques humains vennant de milieux très différents ont déjà pu se rencontrer et faire tiers-lieux, inventer ensemble des outils, dans les Mi-Lieux, on considère les autres viviants (ou pas) comme actants on crée ensemble des outils pour habiter.

![](https://hot-objects.liiib.re/pad-relief-aura-fr/uploads/e22f85ff-da5b-4956-9278-7d1b3b2c253d.JPG)

![](https://hot-objects.liiib.re/pad-relief-aura-fr/uploads/442dd9a4-29b5-48fa-9116-88dc34e83914.jpg)

)