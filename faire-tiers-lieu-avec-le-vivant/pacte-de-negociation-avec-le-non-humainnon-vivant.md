# Négocier avec le non humain

Explorer le champ symbolique et dialectique de la négociation appliquée aux non-humains

## Ecouter ce qui ne se dit pas

Engager un dialogue en tête à tête, sans mot, avec un non-humain. Ne pas tenter d'entendre ce qui se dit, mais fouiller dans le silence. Les idées qui nous viennent en tête à ce moment viennent-elles d'une refléxion intérieure, d'un monologue cérébral méditatif ou sont-elles issues et/ou influencées par un dialogue non verbal avec le non-humain ? Aurais-je eu les mêmes pensées, idées, réflexions sans sa présence et ce temps d'échange ? Peut-être que non, peut-être que c'est déjà une conversation puisqu'il s'est nourri d'une interaction, certes silencieuse, avec le non-humain... Converser, c'est négocier.

## Appliquer une contrainte

Appliquer une contrainte, douce, dans tous les cas non mutilante et destructive envers le non-humain. Attendre. Observer. Prendre note. "Ecouter" et retranscrire la réponse d'une manière intelligible par vous et vos pairs. Augmenter, adapter ou changer la contrainte. Attendre. Observer. Prendre note. Recommencer jusqu'à commencer à décrire un premier vocabulaire de contrainte-réponse, de message emetteur-récepteur. Inverser : utiliser la réponse comme signal. Attendre. Observer. Prendre note. A partir d'un vocabulaire suffisant, démarrer la négociation. Note pour le novice : démarrer avec un cours d'eau sera surement plus simple qu'avec un rocher...

## Pratiquer l'échange

Prendre quelque chose au non-humain (qui ne le détruise pas évidemment). Posez la chose devant soi. Qu'est-ce que je pourrais lui donner en échange qui rende équitable cette "négociation forcée" ? Inverser : donner quelque chose de soi au non-humain. Qu'est-ce que je pourrais attendre de lui que je trouve juste et équitable ? Prenez des notes, ceci est une introduction à la symbiose. Si ça se trouve, tout le monde peut y gagner.

## Préambule à la négociation

Quel rite/tradition/pratique instaurer comme préambule à la négociation, qui marque le début d'un temps d'échange ? Tout négociateur ou diplomate sait qu'il doit d'abord établir une confiance réciproque avec l'autre, trouver un terrain trans-culturel commun ? Un échange de cadeau ? Prendre des nouvelles de la famille ? Quel est la "bienséance culturelle" attendue par le non-humain avant d'entamer la négociation ? Et, vous, qu'attendez-vous de votre interlocuteur avant une négociation ? Quel geste la rendrait caduque d'avance ou au contraire faciliterait votre accord ?

## Epilogue de la négociation : conclure l'accord 

Faites une négociation avec un non-humain. Trouver un compromis acceptable. Proposer le lui. Il est d'accord ? Bien. Maintenant quelle preuve, garantie, gage de confiance, attendez-vous de lui pour sceller l'accord ? Qu'est-ce que vous pouvez lui proposer qui le rassurerait sur votre capacité à tenir parole ? Quelles sanctions pour celui qui ne le respectera pas ?

## Embaucher un médiateur

C'est que l'on fait quand on ne se comprend pas ou plus. Choisissez un tiers neutre, aguerri aux techniques de médiation. Exposer lui votre problème avec le non-humain. Le médiateur interroge les parties prenantes. Ecoute, synthétise, relance... garantit un temps d'expression équitable, pousse dans les retranchements, revient en arrière, rassure, apaise. Propose des exercices, des points de vue. Soyez indulgent avec le médiateur, ce n'est pas simple... Pour les plus avancés : tentez la médiation par un médiateur non-humain.
