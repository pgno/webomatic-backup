# 1/ Intro : 

## Rencontre 1 - 05/12/23
Isabelle fait l'introduction pour resituer cette proposition d'atelier:
historique de nos discussions lors du comm'un lundi 7 novembre sur les budgets contributifs, et de la dérive entre contributeurs et prestataires, de la notion de subordination, et surtout de l'établissement d'un **commun,** et de contributeurs à ce commun (qui en bénéficieront/notion de besoin de contribution)

> On prends soin des communs parce qu'on en dépends"

![](https://hot-objects.liiib.re/pad-relief-aura-fr/uploads/59885106-7aad-4465-a5a1-ae61903cdbf9.png)

![](https://assemblee.lescommuns.org/wp-content/uploads/sites/6/2016/03/cdc.jpg)

Considérant nos expériences passées et intérêts collectif pour la nature, le vivant, nos combats, la discussion à ensuite évoluée vers la notion de commun du vivant, de son statut juridique, de la notion de faire tiers lieux avec le vivant / non Humain...

# En guise d'intro je vous partage mon parcours:

mon travail d'artiste depuis 2016:
Comment "Faire Avec le vivant"
> Mon travail d’artiste consiste à tenter de faire exister - par l’imaginaire et la poésie - une autre vision de  l’utilisation humaine des “ressources naturelles” pour tenter un glissement, une écoute et accueillir les Non-Humains dans les Communs et reconnaître une vraie égalité de droits et de devoirs pour l'ensemble des écosystèmes. 
> Comment vivre, produire, exister ensemble : arbres, nuages, entreprises, oiseaux et humains ? 
> Comment entrer dans une économie qui respecte ses acteurs: les agriculteurs, marchands, mais aussi les sols, les nappes phréatiques, les rivières comme les animaux?
> 
D'abord l'idée de faire pousser du mobilier - en lui "murmurant à l'oreille" : 2015 Tree Coach - La Myne
 ![](https://hot-objects.liiib.re/pad-relief-aura-fr/uploads/8a01ae37-e1d6-48e8-9e15-a32c85cd2734.png)

Puis la réalisation que l'important c'est "la réciprocité et l'observation, afin d'éviter de croire que la nature est là pour l'usage personnel de l'homme et de développer un "processus de biofabrication symbiotique" et se poser la question " que peut-on faire POUR les arbres? "
 ![](https://hot-objects.liiib.re/pad-relief-aura-fr/uploads/7fa00427-f17e-4182-97b8-7b05fb6a910a.png)
fabriquer AVEC les arbres des "espaces mentaux"

 2016: Ave les Plantoids, l'idée qu'une "ressource" peut s'appartenir à elle même - l'oeuvre d'art peut "embaucher" des artistes - de là l'idée d'une forêt qui s'appartient à elle-même et peut "embaucher" des avocats pour se défendre 
 ![](https://hot-objects.liiib.re/pad-relief-aura-fr/uploads/025ce3c4-7f7a-4926-b0a1-ffc6c26ef689.png)

[Forest as a DAO](https://hackmd.io/@OJ1AuLNBT_uFVmM2nstSYA/H169HKKg-?type=view) est un geste poétique contributif (#cc by sa) qui participe à un mouvement plus large visant à établir une relation réciproque avec ce que nous appelons les "ressources naturelles" et une expérience liant les technologies blockchain et la gestion des ressources partagées ou commons.
plus d'info: http://lesusineslouise.com/ProjetsLUL.pdf


## la notion de "personnalité juridique pour les milieux de vie" une notion qui se développe 
exemple: l'Altarato en Colombie
![](https://hot-objects.liiib.re/pad-relief-aura-fr/uploads/67530b5a-1376-40ef-922a-95f75e308223.png)
![](https://hot-objects.liiib.re/pad-relief-aura-fr/uploads/0f7c57e1-75fd-4f1b-a786-45afd67a3673.png)

![](https://hot-objects.liiib.re/pad-relief-aura-fr/uploads/0585285d-b60e-4b85-80a4-11c7bd817c53.png)

![](https://hot-objects.liiib.re/pad-relief-aura-fr/uploads/b7ee83b1-0537-4489-8c57-94ed1b4a0d16.png)

![](https://hot-objects.liiib.re/pad-relief-aura-fr/uploads/7365fc16-68f4-4af4-a538-3e985d983565.png)

**des expériences locales**
L'appel du Rhône - La Têt - La Loire - La seine ....
Il en ressort surtour des questions de gouvernance - on crée des communautés de "gardiens" 
![](https://hot-objects.liiib.re/pad-relief-aura-fr/uploads/a59f1ca2-4b6d-42e0-8531-72f80d76b46e.jpg)

## Ou alors un renversement conceptuel du droit de propriété? 


![](https://scontent-cdt1-1.xx.fbcdn.net/v/t39.30808-6/278573518_3161570260721967_6090373890880851303_n.jpg?stp=dst-jpg_s960x960&_nc_cat=105&ccb=1-7&_nc_sid=e3f864&_nc_ohc=mvH-p5wEKMsAX_kry3i&_nc_ht=scontent-cdt1-1.xx&oh=00_AfBbhEjKpO_PZCLeliqCL5F8W7bngTfChSPJ3pgpHLaATA&oe=63916BD7)
Bermuda. les artistes propriétaires du Tiers-lieux 
Un toît pour abriter -un sol pour exister - une nouvelle temporalité et une autonomie qui permet de faire pousser un projet politique - devenir une espèce endémique : 
![](https://hot-objects.liiib.re/pad-relief-aura-fr/uploads/4bbaa8d9-8f62-4439-a3c2-8b057348389b.png)


Le Vorgey du LAC: un millieu de vie qui s'appartien à lui même => comment on co-décide de ce qui s'y fait? 
https://lac.lesusineslouise.com/
![](https://hot-objects.liiib.re/pad-relief-aura-fr/uploads/4652c639-dadd-44f2-ba67-ae8ec3ebf236.png)

![](https://hot-objects.liiib.re/pad-relief-aura-fr/uploads/184b45e9-6657-4a54-9106-ac969e3b81fd.png)



> *Accueillir explicitement les non-humains dans la gestion des communs en suivant le « principe de symétrie» issu de la théorie de l’acteur-réseau, résumé ainsi par Philippe Descola :[…] l’idée de symétrie, c’est-à-dire […] l’exigence qui consiste à **introduire les non-humains sur la scène de la vie sociale autrement que comme des ressources ou un entourage extérieur.** Faire de l’anthropologie symétrique, de ce point de vue, cela ne signifie pas expliquer la vie des humains par l’influence des non-humains, mais rendre compte de la composition d’un monde où les uns comme les autres prennent part en tant qu’acteurs – actants dirait Latour – avec leurs propriétés et leurs modes d’action, et constituent donc des objets d’intérêt égal pour les sciences sociales[46]. Lionel Maurel, [Accueillir les Non-Humains dans les Communs (Introduction)](https://scinfolex.com/2019/01/04/accueillir-les-non-humains-dans-les-communs-introduction/#_ftn46)*
> 
## « Nous ne défendons pas la Nature ; nous sommes la Nature qui se défend ».

![](https://hot-objects.liiib.re/pad-relief-aura-fr/uploads/a52934da-7f80-4e63-89cd-2cb5b50dd894.png)


le célèbre mot d’ordre des Zadistes de Notre-Dame-des-Landes -aujourd'hui est-ce qu'on peut encore se mettre "en dehors" et considérer la nature comme une ressource dont on prends soin? Ou est-ce que **"[la nature n'existe pas"]( https://www.youtube.com/watch?v=fV2VqJhxuEE&feature=youtu.be)** 
**Et donc, il nous appartiendrai maintennant de faciliter comme nous le faisons déjà dans nos lieux,  la mise en lien humain-autres que humain dans un rapport de respect mutuel - un droit à être là et à proposer ?** 
Les communs à développer seraient donc par exemple des méthodes d'animation, https://le-lichen.org/

![](https://hot-objects.liiib.re/pad-relief-aura-fr/uploads/442dd9a4-29b5-48fa-9116-88dc34e83914.jpg)


**Faire “Tiers-Lieux” ( [Le manifeste des Tiers Lieux. Movilab](http://movilab.org/index.php?title=Le_manifeste_des_Tiers_Lieux)) pour une relation de pair à pair avec le vivant**
“Sur un territoire identifié, le Tiers-Lieu est une **interface ouverte et indépendante permettant l’interconnexion** ainsi que le partage de biens et de savoirs. le Tiers-Lieu est une manière d’articuler les différentes ressources d’un territoire afin de générer de nouvelles valeurs.”
“Le Tiers-Lieu est un bien commun révélé, délimité, entretenu par et avec un collectif: Le Tiers-Lieu constitue un cadre d’action pour reconfigurer un système de valeurs. Des individus se réunissent autour d’une problématique. Ils se l’approprient, recherchent et produisent des solutions. “
“La notion de Tiers réside dans le processus qui va permettre aux individus de s’approprier cette interface. De la mettre en mouvement. Un échange continu est à provoquer car les rencontres improbables ne peuvent se décréter. La programmation n’est pas suffisante. Il est nécessaire de générer différents flux d'interactions entre les individus, entre les espaces, entre l’intérieur et l’extérieur.” 
