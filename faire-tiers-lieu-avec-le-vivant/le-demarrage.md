#  Mi-Lieux ou comment "Faire tiers lieu" avec le vivant / Non-humain

## Contexte
La question du vivant/non-humain et des tiers-lieux nous tiens à coeur. Lors de Comm'un lundi le 5/12/22 on a vu qu'on a des artistes, vignerons, activistes, arboristes... intéressés par le sujet. Ce sont des enjeux juridiques et politiques qui sont trop vaste pour les comm'un lundi on a donc décidé de se rencontrer en ligne et en vrai pour défriccher le sujet   

![](https://hot-objects.liiib.re/pad-relief-aura-fr/uploads/0aca8511-9e24-43c1-91ff-3932a03edba1.jpg)

**Discussions:**
Rappel du contexte historique, des communs dans les TL, des outils d'animation (Cahier des TL), de documentation (Movilab), le budget contributif (budget d'un collectif sans lien hiérarchique).

Faire TL avec le vivant peut il être un commun?
Le premier commun: méthode d'animation pour inclure le vivant dans les décisions, dans l'animation?

Olivier: Notion de collectif est essentielle (Descola) pour intégrer le non humain et soit sujet de droit, il faut concevoir le collectif humain et non humain dans le groupe: par ex les arbres, les animaux, les plantes. La notion de les inclure.
Les élus coupent les arbres comme un objet qui dérange. Sans se rendre compte qu'ils tuent un être vivant, pour une raison souvent discutable.
Faire conscience qu'il y a un collectif, qu'il faut réfléchir avant, respecter le vivant par définition.
L'humain ne supporte plus le végétal, dès que ca pousse on rase au sabot 5 mm. C'est effrayant cette absence d'éveil. Dans le monde tout le monde n'a pas cette réaction là, cet éveil.

Yoann:Le TL comme espace délimité peut permettre de donner cet espace de faire autrement, d'expérimenter autrement.
L'intérêt du TL est de créer des petits laboratoires ou l'on peut réfléchir à plusieurs et faire collectif.
Vision plus artistique est importante aussi dans les TL, en plus du numérique et autres sujets.
LEs vrais sujets de société: TL et le vivant pourrait être un vrai sujet de société qui sort des comm'uns Lundi.
Question : peut on comparer le droit qu'on donne aux arbres, aux lacs, aux rivières avec le droit donné aux femmes, aux insectes? #polémiqueàlaYoann
est ce compliqué? Peut on utiliser les mêmes méthodes que celles utilisées pour les animaux?

Olivier: un arbre doit avoir des représentants, des gardiens, représentation avec l'intérêt d'un abre, d'une rivière. Considerer que ce sont des personnes vivantes qui méritent notre attention,et qui apparaissent comme être vivant dans les plans d'urbanisme, avoir le droit d'exister.
On peine a donner des droits car pas de considération des droits physiques de l'arbre, rivière.
Olivier: préjudices: rivière polluée. Procureur qui estime atteinte à la population.
Préjudice écologique pur. Sans atteinte à l'humain le droit quand il es appliqué considère l'environnement sans considérer le lien d'atteinte à l'humain.
Notion de la continuité écologique.

Les rivières flottantes :un nuage ne peut être possédé, le nuage peut être un partenaire.
Creuser le sujet et l'imagination pour amener ce sujet.
Ontologie: manière d'être et d'exister.
Notion de culture érodée et hybridée, on parle (P descola) du rapport avec le vivant. **En occident on est en rupture, une non relation, en séparant culture et nature, entre l'homme et le vivant**.
Dans le monde, la représentation du vivant est trés différente. **Rapport à la terre sur la propriété en Europe, différent avec l'Afrique. L'appropriation est occidental. Alors qu'on fait partie d'un commun. Il faut changer de vision.**
On expérimente sur des pactes territoriaux socio écologique. En Indonésie, ils comprennent trés bien en Amérique Latine, en Asie ... En Europe on est complètement faconné en terme d'appropriation: d'avoir.
En Afrique les jardins sont partagés à l'origine, on s'approprie un temps de travail, mais pas un espace, on fait une rotation. En Europe, on privatise, et on condamne les sociétés.
C'est le COLLECTIF qui va sauver le monde.
L'individu et l'appropriation qui va faire chuter les sociétés.
Rencontre entre le monde scientifique, et sensible est trés intéressant.

Maïlis : **réflection intéressante entre l'avoir (possession mode Occidental) et l'être dans notre rapport à la nature (autre que utilitariste)**