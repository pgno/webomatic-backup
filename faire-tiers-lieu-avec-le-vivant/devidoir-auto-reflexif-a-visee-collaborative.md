#  Dévidoir auto-reflexif à visée collaborative



## Une idée de nom : ~~Groupe de recherche sur~~ Tiers-lieu Cultures trans-naturelles

- on cherche à renouer le dialogue avec la "nature", l'entendre, lui parler, intéragir, communiquer pour mieux se comprendre et se respecter. Travailler sur les formes d'encodages (émission-réception) de ces messages pour lui "donner la parole". C'est la constitution d'une culture voire de cultures : histoires et symboles partagés, mythes et rites communs à inventer...
- se limite-t-on au vivant ? la nature regroupe du vivant et du non-vivant (déjà la question  de la définition du vivant est bien difficile... ). Qui plus est, ce qui est mort aussi nous nourrit (physiquement et symboliquement). Les déjà-morts et les pas-encore-nés sont notre histoire, le vivant est un état transitoire qui modifie et assure la continuité de cette histoire. Donc plutôt nature (ensemble des êtres et des choses) que vivants ou non-humains... 
- Trans : qui traverse, qui va d'un point à un autre, qui va au-delà, ... 
- Dona Haraway parle de natureculture (en un seul mot) pour poser sa réflexion dans un cadre (scientifique, philosophique et poétique) qui dépasse se clivage. Cultures trans-naturelles me semble un héritage/prolongement intéressant à la juxtaposition des 2 mots... (à moins que l'inverse soit tout aussi pertinent : Natures trans-culturelles !)

Et on peut appeler ça un tiers-lieu :
- c'est un espace de sociabilisation
- c'est un espace de recherche, d'innovation, et de mutualisation 
- il crée un terrain propre au développement de projet collectif et de communs
- c'est un outil inclusif de développement du territoire "la planète"

Ca peut donc être marrant de le déposer comme projet dans les dispositifs de financement de tiers-lieu comme un acte poélitique émancipateur !
Cela peut surtout être une "bannière commune" pour y développer/regrouper des projets différents

## Du Tiers-lieu au Mi-lieu :-)
Pour y intégrer la nature, il faut peut être dépasser le stade du tiers-lieu et lui donner une nouvelle dimension : le mi-lieu. Déjà un mi-lieu, c'est 17% de lieu en plus qu'un tiers ! Et puis le milieu, c'est l'environnement dans lequel on vit bien que l'on soit à mille-lieux de l'avoir compris et exploré. Faire la moitié du chemin, c'est déjà bien :-) Allez, faisons mi-lieu avec la nature !

## Les virus

Je trouve les virus hyper-intéressants à plein de points de vue : 
- chaque minutes 400 000 virus pénétrent dans nos poumons. Ils sont parfois pathogène (pas besoin de le rappeler en 2022....), on oublie qu'ils sont aussi bactériophages et nous sauve de tout un tas de pathologie bactérienne. 
- il pose la question de la définition du vivant. Les virus sont considérés non-vivants par la majorité des virologues car ils ne remplissent pas tous les critères du vivant. En effet, ils sont acellulaires, ne peuvent ni se reproduire, ni métaboliser sans infecter une cellule hôte.
- Ils convoient du matériel génétique, transferent des gène à l'hôte... plein de truc purement dingue (cf [endosymbiose](https://fr.wikipedia.org/wiki/Endosymbiose)) qui questionne la notion même d'invidu
- 8% des nos gènes seraient d'origine virale, dont les gènes s'exprimant dans le développement placentaire. Les mammifères sont des OGM !
- imaginer comme cela la co-évolution du vivant/non-vivant est un super premier pas pour envisager un dépassement culturel et scientifique du tropisme libéral dans lequel se retrouve ingérée la pensée évolutionniste et darwiniste réduite à la compétition

Dans une perspective culturelle trans-naturelle :-), je trouve ce texte trop bien : https://www.cairn.info/revue-chimeres-2014-1-page-137.htm 

et pour mettre en perspective la place progressive de l'humain et du vivant dans notre écosystème global, ça c'est bien aussi : 
![](https://static.fnac-static.com/multimedia/Images/FR/NR/9a/9c/dd/14523546/1540-1/tsp20230126080117/La-terre-le-vivant-les-humains-Petites-et-grandes-decouvertes-de-l-histoire-naturelle.jpg)

## Les chants d'oiseaux

Cette émission de france culture sur le chant des oiseaux est dingue : https://www.radiofrance.fr/franceculture/podcasts/la-science-cqfd/chant-des-oiseaux-redecouvrez-la-flute-a-bec-7165830

Ce qui m'a marqué : 
- les oiseaux ont des accents régionaux :-) !! Outre le fait que j'ai tout de suite essayé d'imaginer un pigeon avec l'accent du sud ou l'accent belge.... c'est surtout le fait que cela m'ait étonné qui m'a étonné. En fait, il n'y a rien d'étonnant. C'est bien une forme de culture au sens où on l'entend habituellement
- Ca montre bien le reflexe que j'ai encore d'une conception "mécaniste" de la nature.
- les oiseaux incorporent des phrases mélodiques de leur environnement dans leur chant. Il y a un brassage culturel (il faut voir l'oiseau lyre : https://www.youtube.com/watch?v=VjE0Kdfos4Y !!!!). 

Quand la nature émet dans le sonore comme nous avec la parole, on peut plus facilement imaginer des formes de dialogue et donc potentiellement d'échanges avec la nature. Mais c'est assez réducteur...

(Il y a quelques choses autour de ça que je trouve très chouette dans "Les furtifs" d'Alain Damasio autour de la vibration.)

## Protéodie : la musique des protéines

Attention, on nage dans le flou science/business. Mais une fois dit, c'est marrant. Joël Sternheimer, scientifique avéré sous l'égide de De broglie, c'est pas rien... est un peu iconoclaste. Après s'être pris 2/3 claques par le monde académique, il devient chanteur Yéyé sous le nom d'Evariste pour financer ses recherches ! Son travail (en gros) est d'avoir associé une onde à un acide animé et une mélodie à une protéine. On comprend et on entend mieux ici en 5mn :  
https://www.radiofrance.fr/francemusique/podcasts/musique-connectee/connaissez-vous-la-proteodie-5691293
Et de là, on glisse : si les protéines font de la musique quand elle se crée, on doit pouvoir influencer la création des protéines en leur jouant la bonne musique... Bon, faible enthousiasme de la communauté scientifique. Et hop, un brevet exploité par la société Génodics. J'ai déjà vu leur installation, il joue les sons dans les vignes (ou autres plantations) pour prévenir les maladies, stimuler la croissance, etc... 

C'est pas beau ça la musique des protéines pour soigner les plantes :-)

## L'écologie chimique : le langage de la nature

Dans une autre registre, c'est pas les sons, c'est les molécules. L'écologie chimique ouvre plein de chemins passionnants de communication avec la nature. 
On sait déjà qu'on y communique des messages du type: es-tu un allié, un agresseur, les rapports hiérachiques, ...
Ce bouquin, il est trop bien, simple et agréable à lire : 

![](https://www.inee.cnrs.fr/sites/institut_inee/files/styles/book/public/image/ecologie_chimique.jpg?itok=Qrjq0528)

## La taille de la vigne

Quand on travaille la vigne, on essaie de trouver une sorte de deal avec la plante pour qu'elle produise ce qu'il faut de raisin. La taille me questionne beaucoup. J'essaie de dire à la vigne : 
- détend toi sur ta fonction de liane... passons un deal : je m'assure que tu recoive assez de soleil pour te nourrir et en échange tu me donnes du raisin
- sauf que ma manière de lui dire, c'est de la tailler et de mutiler les rameaux qu'elle tente de faire pour atteindre la "canopée" pour qu'elle concentre son énergie sur la fructification
- en même temps, j'essaye de ne pas la forcer à produire trop pour ne pas s'épuiser et que l'on puisse collaborer longtemps ensemble

Nos moyens de communication sont assez limités et un peu brutaux ! Comment lui dire ce que je souhaite, qu'elle me dise ce qu'elle souhaite de son côté et que l'on trouve un consensus non mutilant et productif pour que chacun s'y retrouve ? Même si on travaille beaucoup sur les tailles dite douces (un peu paradoxal....) j'aimerai tellement me passer du sécateur !

En tout cas les lianes sont passionnantes :
![](https://static.fnac-static.com/multimedia/Images/FR/NR/f6/c6/d5/14010102/1540-1/tsp20220713071852/Eloge-des-lianes.jpg)

## La place de l'observateur

Quand on a parlé du rôle de l'observateur dans les échanges avec la nature, j'ai tout de suite pensé à la physique quantique ! Si il y a bien un domaine où la question ne peut être éviter, c'est bien celui là !
Sans rentrer dans les détails, en physique quantique on a fini par admettre que, à l'inverse de la physique classique, on ne peut pas connaître l'état d'une particule avec certitude car son état est une probabilité d'être dans un certain état et même une superposition de ces états ! On va pas détailler, ça serait un peu long et je n'en suis pas capable :-) ! 
Il n'empêche que c'est quand on observe une particule que l'on fige son état. Et ce n'est pas qu'on le révèle en l'observant, non ! son état est une probabilité et quand on l'observe, on le fige de manière aléatoire (cf. effondrement de la fonction d'onde pour ceux qui veulent creuser et surtout les très intriguants états intriqués qui ont valu le dernier prix nobel de physique à Alain Aspect, un prof de mon ancienne fac :-) ).
Ici alors, il n'est physiquement pas possible d'avoir un observateur neutre puisque c'est son observation qui détermine l'état du système ! L'observateur fait partie du système. En extrapolant, nous existons de mille manière simultanément, et c'est le regard de l'autre qui détermine celui que l'on va être au moment où il nous regarde...
Je trouve que l'on a besoin de ce regard sur la nature. Arreter de penser que l'on peut en être un observateur extérieur (même si j'ai plein d'amis naturalistes et que c'est super d'observer la nature :-)). Mais, le simple regard que l'on pose sur elle détermine ce qu'elle ! Et c'est vrai aussi pour les mots (cf [sémantique générale](https://fr.wikipedia.org/wiki/S%C3%A9mantique_g%C3%A9n%C3%A9rale) de Korzybski, la barre du A de Mutualab :-)) que l'on utilise ! Et les concepts et preuve de concept que l'on élabore pour "intrepréter la nature" ( cf. Penser comme un rat de Vinciane Despret )
![](https://static.fnac-static.com/multimedia/Images/FR/NR/3d/b0/29/2732093/1540-1/tsp20220228073858/Penser-comme-un-rat.jpg)
Alors quel regard pose la nature sur nous, en quoi elle nous détermine autant que nous la déterminons par simple observation/co-existence ! Parce qu'observer, c'est déjà reconnaitre l'existence de l'autre et y poser une attention... Et ce n'est définitivement pas un acte neutre, voire même un acte créateur en lui même : du moi, de l'autre et du nous qui en émerge !
