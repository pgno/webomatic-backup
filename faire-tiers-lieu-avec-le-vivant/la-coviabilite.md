# Coviabilité

Morgane, bien avancé dans l’été:
Olivier, en train de préparer des vidéo de 15 minutes et des affiches - le bouquin est en cours - doit faire appel à des perssonnes qui vont écrire unpetit bout, dont Nicolas Roesh

Morgane et Margaux vont faire un podcast qui pourra être diffusé par le biais du salon Innopolis (innovation et ville durable) avce des fictions aussi - souhaitent le diffuser dasn des expos et des festivals - vont commencer en octobre

créer des ateliers Coviablité en tiers lieux et revenir en groupe pour en discuter et évaluer ce qui a été fait en atelier pour arriver à une sorte de “cahier d’activité des tiers-lieux connectés aux vivant”
Jean Pierre Aubin, mathématicien, - fondateur du mot “coviabilité” - qui doit remplacer “développement durable”

coviable, c’est un état - alors que DD c’est un processus

En indonésie, la coviablité fait partie de leur philosophie - enfin pour beaucoup

Isabelle, a rapporté d’une rencontre à Arles, la notion de Peuple domestique versus peuple tellurique - où les peuples domestiques cherchent à transformer les choses tout en retsant dans le carcan de leurs habitudes, (Développement durable)

Olivier, cherche quelqu’un pour écrire un passage (1-2 pages) sur le déni (dans l’esprit de don’t look up) - peut être tradauit et ré-écrit par
Isabelle va demander à Moira Milan - https://fr.wikipedia.org/wiki/Moira_Millán

Morgane part de cohabitation inter-espèce
le registre de la viabilité donne un registre plus scientifique - parce que ça a comme origine une formule mathématique et un groupe au CNRS qui va participer aussi au bouquin - donc à minima le terme parles aux scientifique et permet peit être plus d’aller vers une approche transdisciplinaire

Homocoviabilis - une nouvelle ère

avant on trouvait du fer et de l’or par terre, maintenant on mine des tonnes de minerai pour quelques grammes d’or, tout ça passer de chercheurs d’or à une autre forme de recherche, lorsque les milliardaires n’auront plus d’esclaves

designer le vivant + Design des relations interspécifiques par nicolas roesh

Arturo escobar - ontologie
Eleonor SAS - entrer dans le vivant par le jeu
Eric Chenin - laboratoire mixte internnational franco Brésilien - sur IA et coviabilité
Eric Bapes (tous entrelacés), CNRS
viabilité e écologie
Isabelle compromis et contrainte - trajectoire optimale pour atteindre l’état de coviabilité
Développement durable - mieux en savoir pour mieux en sortir
Romain Pinel
Dominique Bourg, urgence écologique
Christine Ambar

plan en construction

    Une urgence écologique

         1.1. Des prévisions climatiques alarmantes

         1.2. Un déséquilibre entre espèces domestiques et espèces sauvages

         1.3.  L’impact significatif de l’action humaine

    Entrer dans le vivant, c’est à dire ?

         2.1. Sortir de la séparation avec le vivant : passer du statut d’objet à sujet

         2.2. Réintégrer les sociétés humaines dans le vivant : entrer en symbiose

         2.3. Recréer des liens au vivant : faire collectif

    Entrer dans le vivant, comment ?

         3.1. Différentes façons de vivre, d’exister

3.2. Différentes ontologies hybridées

    Parvenir à la coviabilité socio-écologique par le lien de viabilité

         4.1. L’ontologie coviabiliste, une supra-ontologie

4.2. Le lien de viabilité, lien d’interdépendance

    Atteindre la coviabilité socio-écologique par le dépassement du « développement durable »

         5.1 Passer du « développement » des sociétés humaines à la viabilité des systèmes sociaux et écologiques

         5.2. Le socio-écosystème : un espace de coviabilité dans un seuil de viabilité

    Ce qui fait coviabilité : des indicateurs territoriaux de la coviabilité socio-écologique aux trajectoires compatibles avec les limites planétaires

    Ce qui fait coviabilité : la santé des territoires, un destin commun de santé humaine et des écosystèmes

    Du concept à l’action

         8.1. Passer d’un concept-paradigme de la coviabilité aux réalités locales du territoire

         8.2. Des étapes progressives des transformations des modes de vie et du modèle économique capitaliste pour une redirection écologique

    Construire la coviabilité socio-écologique à l’échelle locale

9.1. Déterminer le seuil de viabilité par un diagnostic territorial

        9.2. Co-construire une régulation locale maintenant la viabilité des groupes sociaux et des écosystèmes : le pacte territorial de coviabilité socio-écologique

    Intégrer le paradigme de coviabilité socio-écologique dans le droit positif (national et international) et dans les politiques publiques : une règle impérative, un impératif catégorique

Proposer quelque chose pour agir pour le vivant?
autour de l’action, un espace tiers-lieu ?
Pour entrer en action
cartographie et discussions - ?
les gardiens du Rhône? Ce sont une mise en action de la coviabilité, car on est à l’opposédu rapport de prédation - les conséquences sont politiques

des lois négociées entre les acteurs
construire une légitimité - la co-construction du pacte, créé la légitimité