# Naturogramme

Une langue basé sur très peu de signes, "issus de la nature", et qui se combine.
A la manière des kanjis, idéogrammes, les signes et associations sont polysémiques et dépendent du contexte.
Avoir peu de "mots" nous oblige à élargir notre pensée. La spécialisation des mots enferment le dialogue, clive artificiellement les idées. Avec ce
protolangage, exprimer des choses compliquées "coûtent" à celui qui l'exprime. A l'inverse des termes sur-spécialisés qui "coûtent" à celui qui les
reçoit.
S'exprimer clairement réclame alors du temps, un effort pour être compris de celui qui écoute. Ecouter demande de l'attention et une ouverture à
la compréhension de celui qui s'exprime.
Toute la complexité de notre monde déconnecté de la nature retrouve un sens en l'exprimant avec ses entités fondamentales.
Traduire un texte en naturogramme demande un temps long et une compréhension profonde des termes du texte et de la subtilité de leurs sens
sous-jacent et de la manière dont ce sens est partagé culturellement avec son interlocuteur. Il oblige à un lâcher-prise sur la maîtrise de ses
propos et à accepter l'interprétation de l'autre.
Utiliser au sein de la langue usuelle, il permet la substitution de mots galvaudés, clivant ou confus par une notion plus fondamentale et
fédératrice.
La prononciation est basée sur la respiration (inspiration, expiration des voyelles). Elle fait respirer le corps et apaise. Les mouvements de l'air
circule entre le corps de celui qui s'exprime et son environnement. Il peut être, en présence de l'interlocuteur, accompagné de gestes, non
codifiés, pour investir physiquement l'espace de la conversation.

![](https://hot-objects.liiib.re/pad-relief-aura-fr/uploads/4a143963-f21d-4f3d-abb9-55c15e3817cf.png)


![](https://hot-objects.liiib.re/pad-relief-aura-fr/uploads/c9f3ab05-3fd3-4441-8fa1-29d1e28343a7.png)