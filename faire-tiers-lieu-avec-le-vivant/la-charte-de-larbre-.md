# La Charte de l'arbre avec Yoann

## Présentation de yoann

Constat : les arbres sont "maltraités", la formation est bonne mais les pratiques non (ex : gestion des arbres pour faire "joli" dans une ville mais qui abime les arbre)

Point de départ : 
- Tout le monde aime les arbres (pleins de livres, de podcast...)
- Dans la réalité les arbres sont maltraités (partie aérienne et racinaire)
- Les gens commencent à s'y intéresser et prendre conscience
- Des maladies se développent : craintes "épidémiques" et abattage obligatoire (ex le chancre bleu) > 2/3 des arbres risquent d'avoir disparu en France en 2050 à cause de ravageurs monospécifiques

Yoann organise des balades + apéro pour engager la discussion, faire de la pédagogie dans des communes où les élus sont sensibles au sujet

Documentation autour des chartes de l'arbre ( principalement charte de l'arbre **urbain**) sur : https://www.lezarbres.wordpress.org/

Lien avec les tiers-lieu pour faire "tiers-lieu" autour de la problématique et de la charte pour créer un cadre d'échange et de réciprocité > Charte de l'arbre en tiers-lieu

Idée principale : donner des droits à l'arbre comme d'autres "groupes" les ont obtenus. Il existe des jurisprudences

> Olivier : les sociétés modernes sont "naturalistes", on sépare l'humain du reste du vivant . L'arbre est un objet et pas un sujet, donc pas de droit associé aux objets. La notion de nature est inventée, "occidentale", cela rend les choses difficiles... 
> A lire : https://www.babelio.com/livres/Pignocchi-Ethnographies-des-mondes-a-venir/1411355
> a voir : https://youtu.be/fV2VqJhxuEE

Héritage Darwien pose probléme : le végétal n'a pas d'intelligence. Maintenant on commence à comprendre que c'est faux

Héritage Napoléonien : les arbres sont des objets, un ornement 

Métier d'arboriste-grimpeur : héritage d'une culture/formation faite pour le rendement, puis l'ornement

Principe de la charte de l'arbre : créer des cadres d'échanges et réciprocités pour prendre en compte les écosystèmes et les contractualisations qui vont avec

Etapes : 
- 1. Donner des droits à l'arbre
- 2. Contractualiser avec lui
- 3. Le doter de moyens financiers

C'est important de donner des moyens financiers car les arboristes-grmpeurs n'ont pas le temps (donc de l'argent) pour faire "bien" le travail + organiser de la concertation

Mode d'action : créer une plateforme (sur Communecter) pour créer de nouvelles sources de financement par le don (via un fond de dotation) et permettre la défiscalisation

Charte de l'arbre : juste une note d'intention (de 2 à 80 pages). 

Il faut développer de l'ingénierie de formation. C'est la où il y a des sous.
2022 : Présentation de l'idée à France Tiers-lieu et les AFPA. France Tiers-lieu ne répond pas à la sollicitation. Les AFPAs ok pour creuser la question. Proposition d'une maquette pédagogique adossée à la charte de l'arbre pour un MOOC qui se connecte aux formations existantes.

Depuis sécheresse, feu de forêt, on est dans l'urgence écologique.

Le sujet d'une formation avec l'AFPA avance en partenariat avec La Myne dont la Charte de l'arbre en Tiers-lieu serait le fil rouge.

http://negociation-ecologique.fr/index.php/la-charte/



## Discussion



Yoann: envie d'ancrer localement et de diffuser mais pas hors sol
Pascaline: Pas sûre d'avoir compris comment le lien se fait entre la charte de l'arbre et la formation ? 
former sans spécialiser ?
Est-ce que c'est une base pour une gestion plus sensible? 
Comment ça fabrique du paysage? 
Crée des pépiniaires pour que des liens concrets et symboliques se créent entre les habitants et les arbres, lorsque les humains plantent des arbres dans la ville 

On se positionne du point de vue des arbres à planter - réinviter le vivant dans la ville - il y en a presque pas dans le nord pas de calais 
Très intéressée par la formation -

Yoann: A st etienne pour appliquer la charte il faut qu'ils aient les tunes pour se défendre - mais en réalité ils n'ont pas adopté le projet à St Etienne
Se tourne vers Lyon et peut-être Genève où il y a une réelle volonté politique 

Former pour que ça devienne une forme de pensée - pour diffuser et la formation en tiers leiu pour casser les silos et faire entrer de la norme

Mailis: 2 grands sujets:
- un outil de négociation et de dialogue + les moyens financier
- la formation 

conseil: simplifier la présentation pour une meilleure comprésenssion des deux sujets et de leur relation - cartographier les acteurs que tu souhaites impliquer : mieux les définir 
qui sont les opérateurs? --> Yoann: ceux qui font: les élaguers les bucherons 
influencer des politiques publiques -pour qu'elles financent ensuite 
contractualiser avec la nature, est-ce que ce n'est pas des plans de gestion? 
Ajouter la notion de liens sensibles: l'amour de "la nature" nous lie chasseur et naturaliste...

Olivier: 
la notion de charte pose question -- [la déclaration des drois de l'arbre en 2019](https://www.arbres.org/docs/actualites/colloques_conferences/190321DECLA_DROITS-1.pdf) "Ce texte a pour vocation de changer le regard et le comportement des hommes, de
leur faire prendre conscience du rôle déterminant des arbres au quotidien et pour le
futur, en ouvrant la voie à une modification rapide de la législation au niveau national." 
cette notion est intéressante mais réductrice par rapport à ce que Yoann propose 

le dernier point est du à Francis Hallé - pour défendre la santé des collectif des arbres 

Yoann: l'intention était de la faire entrer dans la constitution - mais ça n'avance pas et elle n'est pas appliquée 

persuadé qu'il ne faut pas appeler ça une charte -

Olivier: c'est plutôt une déclaration, et même si elle est appliquée au niveau constitutionel, ça n'aura peut être pas d'effet très important 
l'ensemble des acteurs du territoire doivent co-participer à faire un PACTE - qui implique toutes les parties prenantes 

la déclaration doit pouvoir donner des billes pour l'appliquer dans des pactes de territoires

Sève: si dans le dialogue, chacun garde sa casquette d'élu ou autre on n'arrive pas à instaurer un réel lien, 
le commanditaire c'est la nature elle-même - ce qui est important aujourd'hui c'est de FAIRE différement et déposer sa casquette  qui fait qu'on défends nos droits individuels 

dans la formation: faire des jeux de rôles et inclure les arbres et les oiseaux aussi!