# Le groupe

- Isabelle Radkte 
- [Maïlis Renaudin](#Ma%C3%AFlis-Renaudin)
- Claire-Marie MERIAUX 
- pascaline	
- Frédérique	
- Yoann Duriaux 
- Lucie - Tiers lieu du Treuil 
- Pierre 
- Laurent 
- Olivier Barrière 
- Séverine Carrez 
- Morgane Pecot 

intéressés
- Mélanie Clidière 
- 
**Biographies:**


## Isabelle Radtke 
Comment "Faire oeuvre avec le vivant ?" Mon travail d’artiste depuis 2016 en plus d'avoir co-créé un tiers lieu, consiste à tenter de faire exister - par l’imaginaire et la poésie - une autre vision de  l’utilisation humaine des “ressources naturelles” pour tenter un glissement, une écoute et accueillir les Non-Humains dans les Communs et reconnaître une vraie égalité de droits et de devoirs pour l'ensemble des écosystèmes. 
Comment vivre, produire, exister ensemble : arbres, nuages, entreprises, oiseaux et humains ? 
Comment entrer dans une économie qui respecte ses acteurs: les agriculteurs, marchands, mais aussi les sols, les nappes phréatiques, les rivières comme les animaux?
 
## Maïlis Renaudin #
Indépendante engagée dans la transition environnementale, l'économie sociale et solidaire.
J'ai travaillé 10 ans dans l'écologie à l'international pour la protection des zones humides, notamment pour la Convention de Ramsar.(2009-2019)
Puis j'ai participé à la création d'un tiers lieu en centre ville d'Arles que j'ai dirigé pendant un an (2019-2022)
Depuis je suis indépendante, et facilitatrice du Groupe de Travail Animation du réseau Sud Tiers Lieux PACA. Je suis également facilitatrice du collectif de lieux indépendants et tiers-lieux à Arles. Je travaille également à l'animation de la documentation entre réseaux régionaux de Tiers Lieux. 
**Maïlis** retour sur expérience 
A travaillé 10 ans dans l'écologie à l'international, spécifiquement sur les zones humides - pour une convention internationale dites "Convention de Ramsar". J'ai notamment travaillé en Tunisie avec l'évaluation des besoins d'un lac en eau (besoins pour le fonctionnement naturel du lac). En conclusion, il a été possible d'obtenir un lac de barrage spécifique pour la zone humide = considérer ce lac pour lui même et respecter ses besoins naturels.
Un traité aussi qui protège les zones humides au niveau mondial - Convention de Ramsar
A collaboré avec un bailleur de fond qui a financé une campagne de lobyying, ce qui a permis de créer une grosse communauté d'acteurs ONG et acteurs inter étatiques (Union pour la Med par ex, IUCN), qui a permis d'embaucher des juristes et rassembler des ressources = un site web + des campagnes de protections de rivière et de littoraux avec des partenaires très visibles, comme patagonia.
-site web com/plaidoyer zones humides côtières en méditerranée
https://www.wetlandbasedsolutions.org/
-exemple en espagne lagune statut juridique
https://fr.wikipedia.org/wiki/Mar_Menor

## **Claire Marie** 
pour l'exemple des Saprophytes Lille et le jardin ressources: La ville de Lille avait donnée un droit d'occupation temporaire à une asso pour un terrain - qui y a fait un jardin - à la fin de la convention la ville demande de déménager le jardin -  ils ont du partir alors que les travaux prévus pour une cours d'école végétalisée: la ville était toute puissante sur la décision - conclusion de l'élu: on ne donnera plus jamais accès à des espace à des collectifs qui provoquent de l'attachement  
    

**Yoan:** partage de son approche - arboriste = notion de non-humain, une urgence dans notre métier de donner des droits aux arbres comme on l'a fait sur les droits des animaux  
Voilà pourquoi je fais tiers lieux aujourd'hui - Assureur, travaillait avec les ONG a croisé des tas d'experts , des ONG  entre experts on est dans les cloisonnements - s'est d'abord intéressé aux communs numérique - heureux d'avoir vu des artistes arriver (Isabelle Radtke, David bartholoméo)  pour remettre du sensible - de la poésie !  
Envie de travailler en ce sens c'est le but des Tiers-lieux..
 
 
**Liens du témoignage (pas fait en entier faute de temps)** :

* Qui suis je : https://movilab.org/wiki/Utilisateur:YoannDuriaux
* Mon parcours : https://lezarbres.wordpress.com/2018/11/01/transition-et-reconversion/
* Mon indignation : https://inventaire.io/users/lezarbres/inventory
* Ma part de Colibri : https://lezarbres.wordpress.com/2021/12/04/chartes-de-larbre-concept-realite-mise-en-pratique-et-developpement/
* Faire Commun : https://movilab.org/wiki/ArboVegetaLab
* Mon intention : https://www.communecter.org/#@europeanTiliosTreeCharter.view.detail
* Ma proposition : https://soundcloud.com/lezarbres/podcast-afpa-tiers-lieux-et-apprentissage
* Un imprévu : https://lezarbres.wordpress.com/2021/11/10/en-elagage-un-accident-est-une-succession-de-negligences/
* Mon adaptation : https://poc.dokos.cloud/
* Exemple de terrain de jeu : https://www.hermitagelelab.com/


## **Pierre:** 
Bts viticulture - un projet dans le beaujolais, pour défendre les terres agricoles - aujourd'hui c'est pas très dur de trouver des vignes, mais très difficile de trouver des bâtiments  
Un projet à un moment d'achat de grange mais ça a capoté

acheter à plusieurs? Comment on préserve un paysage agricole, c'est des terres et des bâtiments - on habite plus à côté de là où on travaille - car les gens ne veulent plus vendre aux agriculteus

## Olivier Barrière 
juriste Environnement IRD, travaille sur le rapport au vivant, relation société-vivant (non anthropocentrée)
a développé le concept de coviablité socio écologique: aucun système n’est autonome, tout système dépend de l’autre : “co viabilité”
réserves de Biopshère pour vivre en harmonie avec la nature. A travaillé sur les communs en Afrique (anthropologue du droit) exporter le caractère individuel du rapport au monde.
Travaille sur le foncier environnement aussi.

Divers travaux d’Olivier :

Coviability: ouvrage en 2019 avec Springer : https://drive.google.com/drive/folders/1ep9TZcKA275AdfuvHggdeXthb4RtUZvY?usp=sharing (PDF demander l’accès)

Le pacte pastoral : maintenons les activités et l’identité pastorales du territoire ! : https://caussesaigoualcevennes.fr/competences/pacte-pastoral/

Lancement du projet CovPATH :
Une nouvelle voie pour relier l’Homme à la biosphère : https://www.mab-france.org/fr/actualite-et-publication/lancement-du-projet-covpath-une-nouvelle-voie-pour-relier-l-homme-a-la-biosphere/
résumé du projet en français:
https://www.ird.fr/une-nouvelle-voie-pour-relier-lhomme-la-biosphere-la-coviabilite-socio-ecologique
les résultats attendus s’articulent autour de la consolidation d’un consortium partant d’une cinquantaine de chercheurs, gestionnaires et acteurs locaux, associé à l’animation du réseau.
Cette première phase doit nous conduire à une seconde phase plus opérationnelle encore, permettant la mise en place d’un protocole de recherche-action autour de la co-construction de pactes territoriaux, comme intendance territoriale. L’idée sera enfin d’aboutir à une définition endogène, co-construite de la coviabilité socio-écologique et d’envisager des stratégies et scénarios dans le but de préparer un guide sur les interactions humains/non-humains.

https://www.belmontforum.org/archives/projects/coviability-path-a-new-framework-to-sustainably-link-mankind-and-biosphere
The main objectives of the project are the constitution of a consortium and the identification of a sustainability trajectory.
the CovPath project, focuses on two objectives:

The consolidation of a consortium gathering local actors, a network of researchers, managers, and institutions involved in the management of the project sites.
The identification of a three-dimensional sustainability trajectory.