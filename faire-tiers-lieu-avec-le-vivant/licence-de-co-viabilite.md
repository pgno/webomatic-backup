# Licences de co-viabilité

S’inspirer des créative commons (communication) et de leur lois modulaire avec l’idée des cubes/jeux d’enfants (proposés par Yoann) pour faire des LCV : licences de co-viabilité.

Possibilité de les utiliser pour s’exprimer en réunion ou pour indiquer une intention dans un lieu, ce qui nous réunit - des outils de dialogues au sein d’un collectif. (Ce qui met à l’aventure)

![](https://hot-objects.liiib.re/pad-relief-aura-fr/uploads/8781e674-473d-4948-92be-be2c9f1c6034.png)
