#Canvas de statut

## Article xx - Conseil d'administration

L'association est dirigée par un conseil de X membres, élus pour X années par l'assemblée générale. Les membres sont rééligibles. 

[...]

### Article xx.xx - Représentation associée

Tout membre du conseil d'administration peut choisir d'être représenté par une entité humaine, non-humaine, vivante ou non-vivante. Le membre humain et son représentant sont considéré comme un seul membre et dispose donc d'une seule voix quand bien même ils viendraient à sieger conjointement au conseil.

Par cette association, toute décision prise en présence de l'un ou de l'autre les engage de manière conjointe et réciproque vis à vis du conseil et de l'ensemble des membres de l'association.

### Article xx.xx - Approbation de la demande

Pour ce faire, le membre humain doit :

- signaler lors d'une réunion du conseil ou par voie d'assemblée, générale ou extraordinaire, son intention d'être représenté au conseil par association avec un non-humain-vivant-non-vivant. 

- cette réprésentation ne sera effective qu'après délibération et approbation à (la majorité - l'unamité) des membres (du conseil - de l'association)

- le membre du conseil à l'initiative de cette représentation doit fournir lors de sa demande :
	- l'identification de son représentant 
	- le pacte d'association qui le lie au représentant

### Article xx.xx - Identification du représenant

L'identification du représentant est une description suffisament précise du représentant qui permet aux membres du conseil de reconnaître de manière non équivoque le représentant.

### Article xx.xx - Changement du représenant
 
Tout changement du représentant doit faire l'objet d'un signalement au conseil au préalable à toute nouvelle représentation dans un délai de XXX. Le changement de représentant est soumis à  la délibération et l'approbation à (la majorité - l'unamité) des membres (du conseil - de l'association).

### Article xx.xx - Pacte d'association

La forme prise par le pacte d'association du membre et de son représentant est libre et laissé à l'appréciation du membre.

Il doit toutefois permettre aux autres membres du conseil  d'avoir une compréhension suffisante des modalités de négociation entre le membre et son représentant. Le pacte ne peut en aucun cas porter préjudice ni au membre ni à son représentant. L'accord de représentation ne peut se faire sans l'approbation du pacte. Le pacte sera inséré en annexe du reglement intérieur, à la disposition de l'ensemble des membres de l'association.

### Article xx.xx - Modification du pacte d'association

Toute modification du pacte doit faire l'objet d'un signalement au conseil au préalable à toute nouvelle représentation dans un délai de XXX. Le changement de représentant est soumis à  la délibération et l'approbation à (la majorité - l'unamité) des membres (du conseil - de l'association).

### Article xx.xx - Révocation de la représentation associée

La représentation associée peut être révoqué sur demande de tout membre du conseil d'administration à l'issue de la délibération et l'approbation à (la majorité - l'unamité) des membres (du conseil - de l'association) de la décision..

### Article xx.xx - Délégation de pouvoir

Le conseil d’administration peut déléguer tel ou tel de ses pouvoirs, pour une durée déterminée, à un ou plusieurs de ses membres, y compris  au membre associé humain-non-humain-vivant-non-vivant.

[...]




