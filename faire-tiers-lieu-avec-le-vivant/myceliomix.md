# Mycéliomix

**Inspiration:** https://museomix.org/editions/2019/forez-un-village-et-des-collections-textiles
Inspiration:
https://phauneradio.com/en/ecoutes/what-the-phaune-2/

Aller dehors ensemble et apprendre à se parler, à s'couter et à créer ensemble une réalité

![](https://hot-objects.liiib.re/pad-relief-aura-fr/uploads/d87d3e49-07ce-454f-a296-2b4ba089c941.jpg)
