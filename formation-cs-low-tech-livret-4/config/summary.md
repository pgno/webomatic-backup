---
title: Livret 4 - Faire soi-même avec les autres
licence : CC-BY-SA
author : Centres Sociaux Connectés
cover: https://img.freepik.com/premium-vector/chemistry-subject-concept-scientific-experiment-lab_277904-2166.jpg
collection : Projet de pierre
---

- [Livret 4 - Faire soi-même avec les autres](https://pad.lescommuns.org/formation-cs-low-tech-diywo)
