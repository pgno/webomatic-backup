---
title: Faire soi-même avec les autres
---

# Faire soi-même, avec les autres

Ce module aborde la notion de DIY, de l'anglais "Do It Yourself", faire soi-même. Nous sommes au coeur de la démarche low-tech. Fabriquer, réparer, documenter... sont autant de pratiques qui assurent la compatibilité d'une réalisation technologie aux principes de durabilité et d'utilité sociale.

Ces pratiques s'incarnent à la fois dans de nombreux mouvements, technologiques, politiques ou idéologiques et dans l'espace physique à travers des tiers-lieux, des fablabs, des jardins partagés ou tout autre lieux qui allient production responsable et sociabilité. 

Il n'est donc pas étonnant de voir se croiser autour de ces thématiques des acteurs issus de secteurs comme l'éducation populaire, l'entrepreneuriat social, l'écologie, les sciences, l'agriculture... Cet aspect multidisciplinaire et transversal confère à la low-tech un grand potentiel transformateur. Il peut aussi être à l'origine d'une critique ou d'un manque de lisibilité en tant que terme trop "généraliste" dans ses ambitions et réduit dans ses applications à grande échelle.

:::success
Rappel : notre plan de formation
- [x] : Journée de lancement, introduction
- [x] : L'accessibilité, un numérique simple pour tous
- [x] : L'impact des technologies (1/2) : impact écologique
- [x] : L'impact des technologies (2/2) : impact social et économique
- [x] : **Faire soi-même, avec les autres**
- [ ] : La durabilité technologique
- [ ] : L'utilité technologique 
- [ ] : Journée de clôture
:::

###### 

## Aux origines du DIY

Faire soi-même n'a évidemment rien d'innovant, remontant à l'époque où les individus devaient fabriquer eux-mêmes leurs outils, vêtements et abris. Le mouvement "Do It Yourself" (DIY), traduit par "Faites-le vous-même" en français, trouve ses racines dans diverses pratiques et philosophies culturelles à travers l'histoire. En tant que mouvement culturel distinct, il a commencé à se former au milieu du 20ème siècle.

Après la Seconde Guerre Mondiale, le mouvement a pris de l'ampleur en partie en réponse à la consommation de masse. Les gens ont commencé à réaliser des projets de bricolage, de réparation et de rénovation pour personnaliser leurs espaces de vie et exprimer leur individualité.

Dans les années 60 et 70, le DIY a été adopté par divers mouvements de contreculture qui rejetaient les normes commerciales et promouvaient l'autonomie, notamment dans la musique, l'art et le mode de vie. Cela a contribué à une culture de l'indépendance et de l'autoproduction touchant de nombreux secteurs, de la culture à l'agriculture.

Avec l'avènement d'Internet et des avancées technologiques, le mouvement DIY a connu une nouvelle vague de popularité. Les tutoriels en ligne, les forums et les plateformes de partage de vidéos ont rendu l'apprentissage et le partage de projets DIY plus accessibles que jamais.

### Les raisons d'un retour au "faire soi-même"

Plus récemment, le mouvement DIY a été lié à des préoccupations environnementales et à la durabilité. Les gens adoptent le DIY non seulement pour des raisons de créativité et d'économie, mais aussi pour réduire leur empreinte écologique en recyclant et en réutilisant des matériaux et répondre aux  préoccupations d'autonomie et de résilience, notamment en réaction aux risques liées au déréglement climatique. 

La low-tech peut-être vu comme un prolongement du mouvement DIY, ou comme une "généralisation" de concepts et de pratiques à l'oeuvre à la fois dans les environnements culturels, techniques et s'appuyant sur les différentes sciences de l'ingénieur (mécanique, informatique, agronomie...) et la réappropriation de savoir-faire plus anciens. Les deux mouvements encouragent une manière de penser et d'agir qui valorise les solutions personnalisées, écologiques, et accessibles, tout en favorisant le partage de connaissances et l'innovation ouverte.

######

### Une pratique qui touche tous les secteurs

DIY et Low-tech touchent l'ensemble des secteurs de l'activité humaine : alimentation, habitat, énergie, transport, mobilités, éducation, santé, habillement.

> Dès les années 70, une réflexion sur la sobriété technologique est engagée
> [Wikipedia](https://fr.wikipedia.org/wiki/Technologie_interm%C3%A9diaire#qrcode)

:::success
**Les technologies appropriées, l'ancêtre des low-tech ?**  

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/7e08cce9-170c-49d0-b6b8-c693b09526bb.jpg)

La [technologie appropriée ou intermédiaire](https://fr.wikipedia.org/wiki/Technologie_interm%C3%A9diaire) est un mouvement idéologique initié par l'économiste Ernst Friedrich Schumacher dans son ouvrage _Small is beautiful [1973]_. [...] Cela englobe les choix technologiques et applications à petite échelle, décentralisés, à forte utilisation de main-d'œuvre, économes en énergie, respectueux de l'environnement et localement contrôlés. En 1976, suite au premier choc pétrolier, le congrès américain créé le _National Center for Appropriate Technology_, qui sera fermé en 1981 dès l'arrivé de Ronald Reagan au pouvoir.
:::

## Numérique et DIY

### Les mouvements du libre, de l'open source et de l'open hardware

L'histoire du logiciel libre et de l'open source a débuté dans les années 1980, initiée par Richard Stallman et son projet GNU, visant à créer un système d'exploitation entièrement libre. La création de la Free Software Foundation et la licence publique générale GNU ont posé les bases juridiques et éthiques pour le partage et la modification des logiciels. Dans les années 1990, le mouvement open source a émergé en se concentrant davantage sur les avantages pratiques de l'open source pour le développement de logiciels, notamment la qualité, la flexibilité et l'innovation. Ces mouvements ont profondément influencé le développement de logiciels, en mettant l'accent sur la collaboration, la transparence et l'accès libre au code source.

> L'open source se décline aussi autour du matériel et du tangible [OSHWA](https://www.oshwa.org/definition/french/#qrcode)

:::success
**La déclaration de l'open Hardware**
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/c3d92ced-3d95-4f98-80af-ab0ed5e85107.png)

[Open source hardware](https://www.oshwa.org/definition/french/) regroupe les conceptions “Hardware” réalisées publiquement et disponibles de manière à ce que n’importe qui puisse étudier, modifier, distribuer, créer et vendre un “design” ou un produit basé sur ce design. La source du produit hardware, le design duquel le produit est issu, est disponible sous un format choisi pour permettre de faire des modifications. Idéalement, open source hardware utilisera des composants et matériaux facilement approvisionnables, des procédés de fabrication standard, des infrastructures libres, des contenus libres de droit et des outils de design “Open-source” pour maximiser la possibilité donnée à d’autres de concevoir ou utiliser un produit hardware. Open source hardware permet à quiconque d’avoir le contrôle sur leur technologie du moment qu’elles partagent leur savoir et encourage le commerce au travers de l’échange de design libre.
:::

Au début des années 2000, s'inspirant des principes de l'open source logiciel apparait la notion d'open-hardware. Elle vise à promouvoir la conception et la production de matériel dont les plans, les schémas, et les spécifications sont accessibles publiquement et peuvent être utilisés, modifiés et partagés librement. Cette approche a été popularisée par des projets tels que l'Arduino et le Raspberry Pi, qui ont révolutionné le domaine de l'électronique DIY et de l'éducation. L'open hardware a également eu un impact significatif dans d'autres domaines, y compris la robotique, l'automobile et même la biotechnologie, en encourageant l'innovation collaborative, en réduisant les coûts de développement et en démocratisant l'accès à la technologie.

######

> Les licences Creatives Commons favorisent la diffusion d’œuvres et de contenus numériques ou papier [CC](https://creativecommons.org/#qrcode)

:::success
**Plus de 20 ans de Creative Commons**

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/413369f7-8cd6-44ce-9d35-778e9c979f99.png)

Les licences [Creative Commons](https://creativecommons.org/) ont soufflé leur 20e bougie en décembre 2022. Créées en 2002 par la société à but non lucratif Creative Commons,elles favorisent la diffusion d’œuvres et de contenus numériques ou papier en fixant les conditions de diffusion et de réutilisation par toute personne. En 2017, plus de 1,4 milliards de contenus ont été diffusés avec une licence Creative Commons. 

:::

### Fablab et makerspace

Les fablabs (laboratoires de fabrication) et les makerspaces sont nés au début du 21ème siècle, avec le premier fablab établi au MIT par le professeur Neil Gershenfeld en 2001. Ces espaces sont conçus pour démocratiser l'accès aux outils de fabrication numérique comme les imprimantes 3D, les découpeuses laser et les CNC (machines à commande numérique). Inspirés par la culture DIY et le mouvement open source, les fablabs et makerspaces se sont rapidement répandus à travers le monde, offrant des espaces de collaboration où individus et communautés peuvent partager des connaissances, apprendre de nouvelles compétences et concrétiser leurs idées en projets tangibles. Ils jouent un rôle crucial dans l'éducation, l'innovation et l'entrepreneuriat, en facilitant l'accès à des ressources et des technologies autrefois réservées aux industries et aux institutions de recherche.

>  Une cartographie des Fablabs français est diposnible à cette adresse : [Carto](https://carto.rfflabs.fr/#qrcode)

:::success
**Une cartographie des Fablabs français**

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/bf1c8bdb-c5e0-48b2-ac34-0a1484f7f331.png)

Le Réseau Français des FabLabs, Espace et Communautés du Faire, [RFFLabs](https://rfflabs.fr/) est une association nationale qui vise à favoriser le faire ensemble (DIWO – « do it with others ») et la constitution de communs en lien avec la fabrication. Une cartographie des Fablabs français est diposnible à cette adresse : [https://carto.rfflabs.fr/](https://carto.rfflabs.fr/)
:::

## Low-tech et DIY

### Simple et économe

Dans les 3 premiers modules nous avons parcouru les grandes thématiques transverses du numérique éthique et de la low-tech. Mais cette dernière est clairement reconnue à travers quelques projets emblématiques d'auto-fabrication comme : 

- **Le four solaire**, sous forme de caisse ou de parabole, il permet de cuire des aliments en utilisant l'énergie solaire.  

:::success
**Le wiki du Low-tech Lab**

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/dca6bba4-9d13-47c5-a72d-3b60dcdbd4c3.png)

Ce [wiki](http://wiki.lowtechlab.org/wiki/Accueil) recense de nombreux projets et tutoriel DIY comme celui du [four solaire](http://wiki.lowtechlab.org/wiki/Four_solaire_%28cuiseur_type_bo%C3%AEte%29)
:::

- **la marmite "norvégienne"**, qui est un procédé de cuisson des aliments consistant à les placer dans un récipient lui-même contenu dans un réceptacle isolant : après avoir été chauffés de façon traditionnelle — mais moins longtemps que le temps de cuisson habituel —, les aliments peuvent y finir de cuire de façon autonome, sans nouvelle dépense d'énergie. 

:::success
**Mü, passer à l'ation pour la sobriété**

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/ae57c859-6f2a-48eb-b67c-1eeb77870a14.png)

L'association [Mü](https://www.mouvementdusages.org), Mouvement d'usages, est à l'origine de la [Frichtie](https://www.mouvementdusages.org/Frichtie), une déclinaison de la marmite norvégienne qui donne lieu à de nombreux partenariats locaux.
:::

- **les toilettes sèches**, sont des toilettes qui n'utilisent pas d'eau. L’intérêt d'assainissement écologique est de récupérer les excréments pour une valorisation séparée ou non, pour en faire du fertilisant (compost) ou de l'énergie (biométhanisation, électricité, chaleur).

_Ces réalisations sont très souvent à l'origine de démarches autour du réemploi et de la réparabilité que nous explorerons dans le module suivant._

Il existe de très nombreux exemples, qui à travers des projets simples et peu couteux, donnent à voir les différents pilliers de la low-tech. Ils sont d'excellents vecteurs de diffusion des technologies durables. Mais comme pour l'ensemble des projets DIY, ils tendent parfois à masquer la portée plus globale des démarches dont ces projets sont issus. 

Dans le cadre d'une diffusion de la low-tech et du numérique éthique, il est souvent nécessaire de permettre au public d'associer ces projets de "bricolage" à leur nature participative au coeur duquel le numérique est souvent central. Car si les high-tech renvoient par nature à une image de progrès, l'usage de techniques de fabrication manuelle peut parfois être symbole de "retour en arrière". 

A titre d'exemple, la popularité de l'imprimante 3D, y compris auprès d'un large public non-technicien, est le reflet d'une technologie qui est à la fois compatible avec la démarche low-tech et symboliquement associée au progrès et à la modernité du numérique.

## Faire soi-même... avec les autres

Il est très courant de voir l'expression DIY suivie de "WO", pour "With Others", c'est à dire "faire soi-même avec les autres". Si les porteurs de projets souhaitent insister sur cette dimension, c'est qu'elle est totalement différenciante d'autres pratiques plus individualistes. On peut effectivement faire soi-même, mais sans partage des savoir-faire ou des connaissances. Sans la notion de "faire ensemble", le DIY n'est plus directement compatible avec une démarche low-tech.

### Documenter, partager

Dans le contexte du DIY, la documentation permet aux individus de partager leurs créations, leurs méthodes et leurs astuces avec une communauté plus large, transcendant les barrières géographiques et culturelles. Cette transmission de connaissances facilite l'apprentissage collaboratif et encourage plus de personnes à s'engager dans des projets de bricolage, en leur fournissant des guides étape par étape, des plans détaillés, et des conseils pratiques. Pour les projets low-tech, qui visent souvent à proposer des solutions durables et accessibles, la documentation est essentielle pour assurer que ces solutions peuvent être reproduites et adaptées localement. Elle permet une diffusion plus large des innovations, en particulier dans les régions du monde où l'accès aux technologies avancées est limité ou coûteux. 

DIY et low-tech favorise une culture de l'innovation ouverte et de la collaboration. Il crée un environnement où les idées et les compétences sont échangées librement, ce qui stimule la créativité et l'innovation. Cette dynamique est particulièrement importante dans le développement de solutions low-tech, où l'adaptation et l'amélioration continues des designs sont nécessaires pour répondre aux défis spécifiques des différents contextes environnementaux et sociaux. 

> Movilab est une initiative qui vise à créer un patrimoine informationnel autour des tiers-lieux et de leurs pratiques [Movilab](https://movilab.org/wiki/Accueil#qrcode)

:::success
**Movilab : Documenter en tiers-lieux**

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/0ed955fc-04c3-4382-8abd-7fb17c678bb4.png)

En croisant l'approche du libre et du durable, Movilab s'appuie sur les pratiques de communauté issues du "Web 2.0" pour co-produire des modes de vie durables et les partager au plus grand nombre dans une démarche d’éducation populaire. Sa [présence en ligne](https://movilab.org/wiki/Accueil) sous la forme d'un wiki est un espace riche d'une très large documentation.

:::

### Mettre en "communs"

Au delà de la documentation, travailler autour d'une ressource partagée, cela peut-être également le premier pied posé dans l'univers des biens communs, ou simplement "communs".

La théorie des biens communs se concentre sur la manière dont les groupes de personnes gèrent des ressources partagées, telles que les pâturages, les pêcheries, l'eau, ou l'atmosphère, qui sont accessibles à tous mais limitées en quantité. 

> Pour en savoir un peu plus sur les communs, le site [lescommuns.org](https://lescommuns.org) fourni de nombreuses informations et sources. [lescommuns.org](https://lescommuns.org#qrcode)

Cette théorie, popularisée par l'économiste Elinor Ostrom, défie l'idée traditionnelle que de telles ressources sont inévitablement condamnées à la surutilisation et à la dégradation (connue sous le nom de "tragédie des communs" de Garrett Hardin). Ostrom a démontré que, contrairement à cette croyance, les communautés peuvent gérer efficacement les biens communs de manière durable à travers des systèmes de gouvernance locaux, adaptés, et basés sur des règles claires, la coopération, le contrôle mutuel, et des sanctions pour les abus. 

:::success
**Le Label Numérique en commun[s]**

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/904c4c39-80fe-4ac7-be04-018a05ababbb.png)

Le [label Numérique en Commun[s]](https://societenumerique.gouv.fr/fr/dispositif/label-nec/) décerné par l'Agence Nationale de la Cohésion des Territoires vise à identifier des dispositifs concourant à l'inclusion numérique sur l'ensemble du territoire. On y retrouve de nombreux critères compatible avec une démarche low-tech, notamment la documentation.
:::

######

# En pratique

:::warning
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/f3e92cbd-1ce6-4a29-a02d-a2448f1c3f92.png =20x)
Durée de la séquence : 30mn
:::

### Partager vos savoir-faire

Pour cet exercice, nous vous proposons de réfléchir à un savoir-faire que vous maitrisez que cela soit en fabrication manuelle ou numérique. Ici pas besoin de chercher quelque chose de techniquement compliqué, cela peut même être de savoir réaliser un avion en papier !

Une fois identifié ce savoir faire : 
- [ ] Faites une recherche en ligne pour savoir si quelqu'un a déjà partager un tutoriel ou une ressource sur ce savoir-faire
- [ ] Si ce n'est pas le cas, imaginez (il ne s'agit pas ici de réaliser un tutoriel) comment vous vous y prendriez pour partager votre savoir-faire ? Un tutoriel ? Une vidéo ? Sur quelle plateforme ?
- [ ] Si la ressource existe, comment est publié ce contenu ? est-il libre d'accès ? Pensez-vous pouvoir l'améliorer ? 
- [ ] Si vous pensez à des améliorations, est-il possible de contacter l'auteur pour lui proposer ? Pouvez-vous initier une collaboration ?

### Une activité low-tech avec mes publics

Nous vous proposons de passer quelques minutes à parcourir le site https://wiki.lowtechlab.org/wiki/Accueil. 

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/756ed93a-2bc2-4e8c-8727-29161bbdb2f9.png)

Pendant que vous le parcourez, essayer de repérer un projet que vous pourriez conduire dans votre structure. Cela peut-être en interne, ou avec le public de la structure.

- [ ] Pourquoi avez-vous choisi ce projet ? Que peut-il vous aider à mettre en avant par rapport aux enjeux du numérique responsable ou de la low-tech ?
- [ ] Est-il utilisable tel quel ? Peut-être devez-vous l'adapter à des enjeux ou ressources locales disponibles. Quelles seraient ces adaptations ?

### Quels licences pour mes publications, celles, de ma structure ?

Vous et vos partenaires produisez de l'information régulièrement, en interne, à destination des publics, etc...
Vous êtes-vous déjà posé la question de la licence de droit d'utilisation que vous souhaiteriez apposer à vos documents ? 

Prenez d'abord quelques instants pour visiter le site [Creative Commons](https://creativecommons.org/) si vous n'êtes pas familier avec ces licences. Ensuite rendez-vous sur l'outil de choix https://chooser-beta.creativecommons.org/ puis choisissez un document  produit et publié en ligne récemment par vous ou votre structure. 

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/093ef2df-7264-4504-8786-c8ac1ce170eb.png)


- [ ] Si vous deviez utiliser ce type de licence, sauriez-vous laquel choisir ?
- [ ] Quels bénéfices ou inconvénients verriez-vous à utiliser cette licence ?

## Travaux

:::warning
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/f3e92cbd-1ce6-4a29-a02d-a2448f1c3f92.png =20x)
Durée de la séquence : 50mn
:::


## Glossaire, interview, portfolio, cartographie, ressources

Vous devez dorénavant consacrer une partie de votre temps de travail dispnible sur ce module pour avancer sur vos différents travaux. Voici une petite liste de suivi pour vous permettre de faire le point.

### Glossaire
- [ ] Y-a-t'il de nouveaux termes à intégrer?
- [ ] Puis-je améliorer une définition, lui apporter un exemple

### Interview
- [ ] J'ai identifié mon interlocuteur
- [ ] J'ai pris rendez-vous avec mon interlocuteur
- [ ] J'ai constituté ma trame d'interview, les questions que je souhaite poser
- [ ] Je sais quel type de restitution je veux réaliser
- [ ] J'ai conduit l'interview
- [ ] Jai finalisé la restitution

### Portfolio
- [ ] J'ai choisi mon thème
- [ ] J'ai choisi l'angle, la problématique que je traite au sein de ce thème
- [ ] Je sais quel type de restitution je veux réaliser
- [ ] J'ai commencé à produire les éléments de mon portfolio (textes, sons, images...)
- [ ] J'ai finalisé mon portfolio

### Ressources
- [ ] Y-a-t'il des ressources (livres, site web, podcast, film, ...) que je puisse partager sur https://pad.lescommuns.org/Labo-Low-Tech ?




