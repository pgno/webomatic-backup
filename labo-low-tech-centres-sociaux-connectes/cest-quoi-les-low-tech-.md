# C'est quoi les Low Tech ?


**La définition du Low Tech Lab** : 

> Au Low-tech Lab, nous employons le terme low-tech pour qualifier des objets, des systèmes, des techniques, des services, des savoir-faire, des pratiques, des modes de vie et même des courants de pensée, qui intègrent la technologie selon trois grands principes : 

> **Utile**
> 
> Une low-tech répond à des besoins essentiels à l’individu ou au collectif. Elle contribue à rendre possible des modes de vie, de production et de consommation sains et pertinents pour tous dans des domaines aussi variés que l’énergie, l’alimentation, l’eau, la gestion des déchets, les matériaux, l’habitat, les transports, l’hygiène ou encore la santé. En incitant à revenir à l’essentiel, elle redonne du sens à l’action.
> 
> **Accessible**
> 
> La low-tech doit être appropriable par le plus grand nombre. Elle doit donc pouvoir être fabriquée et/ou réparée localement, ses principes de fonctionnement doivent pouvoir être appréhendés simplement et son coût adapté à une large part de la population. Elle favorise ainsi une plus grande autonomie des populations à tous les niveaux, ainsi qu’une meilleure répartition de la valeur ou du travail.
> 
> **Durable**
> 
> Éco-conçue, résiliente, robuste, réparable, recyclable, agile, fonctionnelle : la low-tech invite à réfléchir et optimiser les impacts tant écologiques que sociaux ou sociétaux liés au recours à la technique et ce, à toutes les étapes de son cycle de vie (de la conception, production, usage, fin de vie), même si cela implique parfois, de recourir à moins de technique, et plus de partage ou de collaboration !

Source : https://lowtechlab.org/fr/la-low-tech