# Ressources (acteurs, lieux, projets) dans les Hauts-de-France

## Acteurs
### Les Saprophytes
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/807d5f2d-2206-474f-aeec-63e6f960f3b1.png =200x)

Architectes, paysagistes, plasticiens, constructeurs, graphistes, les Saprophytes développent depuis 2007 des projets artistiques et politiques autour de préoccupations sociales, économiques et écologiques.
https://www.les-saprophytes.org/

### Collectif Zerm
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/fec93f07-5c37-4590-b909-92a2e4d11784.png =200x)

Zerm est un collectif d’architecture qui a un rapport à l’espace structuré autour de trois grands axes : Réemploi, Réhabilitation, Low-tech
https://zerm.org/

### L'atelier paysan
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/e320ae28-d8ba-47ee-ba6e-0e86308d5066.png =200x)

L’Atelier Paysan est une coopérative (SCIC SA). Nous accompagnons les agriculteurs et agricultrices dans la conception et la fabrication de machines et de bâtiments adaptés à une agroécologie paysanne. En remobilisant les producteurs et productrices sur les choix techniques autour de l’outil de travail des fermes, nous retrouvons collectivement une souveraineté technique, une autonomie par la réappropriation des savoirs et des savoir-faire.

https://www.latelierpaysan.org/

### La frugalité heureuse et créative
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/7c506564-5637-46d1-b962-c1bd8e6071b6.png =200x)

Le Mouvement pour une Frugalité heureuse et créative s’est construit autour du Manifeste du même nom, lancé le 18 janvier 2018 par Dominique Gauzin-Müller (architecte-chercheure), Alain Bornarel (ingénieur) et Philippe Madec (architecte et urbaniste). Reconnaissant « la lourde part des bâtisseurs » dans les désastres en cours, ce manifeste appelle à développer des établissements humains frugaux en énergie, en matière et en technicité, créatifs et heureux pour la terre et l’ensemble de ses habitants, humains et non humains, respectés. 
https://frugalite.org/
La Cartographie nationale des ressources locales (CNRL)
https://frugalite.org/ressources-2/cartographie/
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/80e208f8-019d-4b78-913b-d90494689eb8.jpg)

### De Rives en Rêves - Rumilly (62)
Ecolieu nourricier et pédagogique en création Jardin-forêt | Fournil | Atelier low-tech...
https://linktr.ee/de_rives_en_reves?fbclid=IwAR13o8J_NuSaOHLIDgs3a63WQMzmsKUapJSF2b1FQvYeiYaFVl1RIjZ6_Sk

### Association Low-Tech Côte d'Opale
Promouvoir la démarche low-tech et sensibiliser les citoyens sur la sobriété des consommations d'énergies et de matières, sur la récupération des matériaux afin d'améliorer l'autonomie technique et la résilience individuelle et collective du territoire dans le respect du vivant et des capacités limitées de notre planète ; participer à la diffusion de savoir-faire et techniques accessibles et utiles aux citoyens pour répondre à leurs besoins fondamentaux
Contact : thumerel.francois@orange.fr

### La gazette de l'expérimentation - territoires de Concarneau
L’expérimentation “Vers un territoire low-tech” place la démarche low-tech au cœur d’une ambition de transformation sociale, écologique et solidaire, qui s’avère être de plus en plus partagée. De cette expérimentation sort une gazette, consultable ici : https://lowtechlab.org/media/pages/actualites-blog/territoire-la-gazette-de-l-experimentation/eb0f0da29e-1695629064/low-tech_lab_gazette_vers_un_territoire_low-tech.pdf

## Lieux ressources

### Tiers-lieu

#### La Chaufferie (porté par les saprophyte)

https://www.les-saprophytes.org/chaufferie-chantier/
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/247b4e8c-e2dd-4d3c-9c6f-ff0858856fd3.png)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/11536023-bdd0-47aa-87f0-0b4285b919a9.png)



#### Le couvent des Clarisses / Roubaix
https://www.vozer.fr/2021/01/02/a-roubaix-comment-le-couvent-des-clarisses-se-transforme-en-maison-de-leconomie-circulaire/
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/ca6d7ab7-e236-424c-b8dd-48ec3ea21ab7.jpg)



#### Ecoland (Palluel-62)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/abfe9a8e-72b8-4d09-bdab-6b1c92405377.png)
Un écolieu, ouvert à tous, pour un après-midi, une ou plusieurs nuits, une ou plusieurs vies.
Une aventure entre marais et forêts. Une alternative low tech, fun et ludique. Un centre de vie unique en France (1 er Super Adobe ouvert au public), six hébergements insolites, un bar, une terrasse sur un toit, une micro ferme.
https://parc-ecoland.fr/

#### Centre Ressource du Développement Durable (Cerdd) / Loos-en-Gohelle
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/8805a08b-2cc4-4306-b89f-6d6eed593fc5.png)

La vocation du Cerdd est d'accompagner la généralisation du développement durable et de la prise en compte du changement climatique.
https://www.cerdd.org

#### Halle aux sucres / Dunkerque
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/cdde3a30-debf-4b5a-9156-1833b09b8e52.png)

Un lieu pour tous ceux qui réfléchissent à la transformation de nos vies quotidiennes.
https://www.halleauxsucres.fr/le-projet/un-lieu-vivant

#### La ferme des loups / Caudry
Explor'acteurs, aventuriers Nature, jardiniers amateurs, maraîchers en devenir ou tout simplement joyeux curieux [...], vous êtes intéressés par la Nature, la permaculture, la culture bio-intensive, l'agroécologie, ou des pratiques tout simplement naturelles? Vous êtes en quête de liberté, d'autonomie, de résilience et d'alternatives pour notre mode de vie et notre société? Simplement à la recherche de produits frais, sains et locaux pour votre alimentation quotidienne, ou d'un lieu où passer de bons moments et qui regroupe tout ça à ,la fois?? 
[https://la-ferme-des-loups.jimdofree.com/](https://)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/ce2dbdc4-8bfd-4d74-851f-a986b3d4a865.png)



### Réemploi, bricolage

#### Le parpaing / Roubaix
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/3c2ae84b-ca19-4edf-a572-c782d8d58da2.png)
Le Parpaing est un magasin de matériaux de réemploi pour la construction lancé par Zerm à Roubaix. Il vise la remise en circulation de matériaux de seconde main ...

https://leparpaing.fr/

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/2273bf9b-71de-4422-b317-9b27007b83e4.png)

#### Rewood/ Tourcoing
Rewood est un magasin de matériaux de réemploi spécialisé dans le bois
https://rewood.fr/

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/390f73c7-d921-4d32-b86f-f89b75f61728.png)

#### Opal réemploi/ Boulogne-sur-Mer
Initié en 2018 par Geoffrey MARGOT, le projet d’implantation d’une recyclerie du bâtiment a fait l’objet d’une étude de faisabilité sur le territoire de la Communauté d’Agglomération du Boulonnais. Cette étude a permis de mobiliser un collectif qui s’est constitué sous forme associative en 2019 : l’association Opal’ Réemploi a ouvert dans la foulée sur la commune de Saint Léonard.
https://www.opalreemploi.fr/

#### Tipimi / Lille Fives
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/a1840981-fca1-43ec-8689-6d4c464f6f66.png)

Objectothèque  : https://tipimi.fr/

#### La fabrique d'architecture partagé / Roubaix
Atelier de bricolage avec des matériaux de réemploi
https://www.les-saprophytes.org/project/fabrique-darchitectures-bricolee/

#### Atelier Pastoch / Hazebrouck
Atelier de bricolage, réparation de vélo
https://www.centreandremalraux.com/quartier-artistique-hazebrouck/
{%youtube M_YWpLHQFd0 %}

#### Le relais / Billy-Berclau
Fabrication d'isolant métisse à partir de textile recyclé.
[](https://)https://lerelais.org/aussi.php?page=metisse
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/85e0a9c8-ea8e-4e90-b830-3c2766a1ff6d.png)

#### Communauté précious plastique / Roubaix
La Condition Plastique est un projet dédié au recyclage des déchets plastiques établi en Halle C de La Condition Publique à Roubaix (France). C’est un espace consacré à la recherche, la création, l’expérimentation et la sensibilisation, ayant pour objectif de développer de nouvelles façons de revaloriser les déchets plastiques.
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/a34b0a8d-5337-4e16-b5ec-91a59e764026.png)

[http://laconditionplastique.com/](https://)


#### Le techshop / Lille
Le techshop, autour de la communauté des makers propose des façons alternatives de créer,travailler, collaborer, apprendre et transmettre.
[https://techshop-lille.fr/](https://)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/e2f2c089-6035-4a9b-a649-18819df282b7.png)

#### Unéole 
Technologies mixtes solaires et éoliens
[https://uneole.fr/](https://)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/d0b898c2-639e-4227-afcc-5a4f8d738adc.png)





