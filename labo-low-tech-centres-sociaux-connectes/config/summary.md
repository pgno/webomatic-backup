---
title: Labo Low Tech des Centres Sociaux Connectés du Nord Pas-de-Calais
licence : CC-BY-SA
author : Centres Sociaux Connectés
cover : https://hot-objects.liiib.re/pad-lescommuns-org/uploads/8818d6b8-b3e9-4d7e-b4ea-e7052b882e0e.png
---

- [Vue d'ensemble](https://pad.lescommuns.org/sommaire-labo-low-tech)
- [Les explorateurs du Labo](https://pad.lescommuns.org/labo-low-tech-laborantins)
- [Les rendez-vous du Labo](https://pad.lescommuns.org/labo-low-tech-rendez-vous)
- [C'est quoi les Low tech ?](https://pad.lescommuns.org/labo-low-tech-definitions)
- [Expériences Low Tech avec le centre social](https://pad.lescommuns.org/labo-low-tech-exeriences-centre-social)
- [Ressources dans les Hauts-de-France : Actions, lieux, collectifs](https://pad.lescommuns.org/labo-low-tech-ressources-hdf)
- [Ressources documentaires sur les démarches Low Tech](https://pad.lescommuns.org/labo-low-tech-ressources-documentaires)
- [Formation expérimentale au Numérique éthique : En route vers la Low Tech](https://pad.lescommuns.org/labo-low-tech-formation-expe)
- [Vivons Low Tech Leven : La Fédération NPDC partenaire associée au projet Interreg](https://pad.lescommuns.org/vivons-low-tech-leven)
- [Premier Labo Low Tech 28/11/23 : Souvenirs du futur !](https://pad.lescommuns.org/labo-low-tech-souvenirs-du-futur)