# Les rendez-vous du Labo Low Tech

## Labo #1 : Mardi 28 novembre à La Chaufferie des Saprophytes à Lille Hellemmes

Ordre du jour : 
- Présentation des participant.e.s
- Présentation des pratiques Low Tech par Mélia Delplanque
- Repas partagé (chaud et végan)
- Tables d'échanges et partages : 
    - Expériences avec les Low Tech dans les centres sociaux
    - Acteurs, lieux et expériences ressources dans les Hauts-de-France
    - Ressources documentaires
    - Projet européen Vivons Low Tech Leven
    - Expérimentation pédagogique
- Mise en commun : que veut-on faire ensemble dans le Labo ?

Mes gribouillages sur la journée (Sandrine)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/9442f5f9-c4ce-407a-ba0d-3ebc33846d24.jpg)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/61494dea-1397-4b28-a74f-9877d8669c9f.jpg)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/64774810-ff57-422e-bb5f-2ee3d39f9583.jpg)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/e8f8459e-e797-4ef8-82a5-fcc80bceff27.jpg)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/e2384596-77b1-44ab-a5c1-fa794c5ffd01.jpg)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/f7ea2485-0459-44b7-8d65-a985cbf0c065.jpg)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/affe4bdd-c5dc-4c1b-95af-7bca267e034.jpg)

## Labo #2 : Jeudi 22 Février à La Halle aux Sucres - Dunkerque

**Ordre du jour :**
* Tour d'actualité pour partager ce qu'il s'est passé pour chacun.e en matière de Low tech  depuis notre première rencontre le 28 novembre (découverte, action entreprise, envie de faire...)
* Présentation des actions de la Halle aux Sucres en matière de Low tech
* Présentation du projet atelier Low-Tech et atelier paysan association De rives en rêves à Rumilly 
* Temps de travail pour imaginer / préciser ensemble comment les Low tech peuvent s'inscrire concrètement dans l'action d'un centre social
    

**1. Les actions de la halle aux sucres.**

Présentation d'objets réalisés par Mathilde VANDERRUSTEN - cheffe de projet relations publiques pour la communauté urbaine de Dunkerque.

Ces ateliers ont été menés conjointement avec François Thumerel, de l'association [[Low-Tech Côte d'Opale](https://www.nordlittoral.fr/158591/article/2022-11-20/boulogne-sur-mer-lancement-de-l-association-low-tech-cote-d-opale)] également présent lors de cette journée pour nous partager son expérience.

François propose également des interventions ["fresque des Low-Tech"](https://fresquedeslowtechs.wordpress.com/) ainsi que des interventions adaptées aux entreprises et associations sur la mise en place d'outils Low-Tech adaptés.

Tutos d'inspiration : 


[[18h39 - Castorama fabriquer une enceinte]](https://wiki.lowtechlab.org/wiki/Lampe_solaire_%C3%A0_batteries_lithium_r%C3%A9cup%C3%A9r%C3%A9es?fbclid=IwAR2f7ZqIwASitCeSVgqWEsZKXXzb2JkhDKuxV4S8-_3Fq5O2Av57j5LdBeM)

[Low-Tech lab fabriquer une lampe solaire](https://wiki.lowtechlab.org/wiki/Lampe_solaire_%C3%A0_batteries_lithium_r%C3%A9cup%C3%A9r%C3%A9es?fbclid=IwAR2f7ZqIwASitCeSVgqWEsZKXXzb2JkhDKuxV4S8-_3Fq5O2Av57j5LdBeM)

![](https://)![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/b1244f53-7ed6-46c6-9c34-5a3cbae8fce6.png)

**2. Présentation de l'association [De Rives en Rêves](https://www.facebook.com/derivesenreves)** 

Le projet atelier Low-Tech est mené dans le cadre d'une étude FIDESS par Jérôme Devulder. Jérôme nous a également présenté la dynamique de l'[atelier paysan](https://latelierpaysan.org/).
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/dc5c7220-c569-45a7-8187-8fad4a69004c.jpg)

A ce sujet, les notes vivantes et pertinantes de Sandrine Goessens

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/91475efe-0832-4485-bc9f-00ea3f0d74eb.jpg)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/c0cc5f24-3c32-4a6a-8610-3eb3e2bc2364.jpg)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/c9646d4e-e8bb-4f94-a13a-41403ad8dc64.jpg)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/a3295eb4-12cd-47ce-b4d7-e0284c7030e7.jpg)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/bd1eb66b-a761-412f-a24b-9e8588d01bdc.jpg)




**3. Temps de travail en sous groupe les Low-Tech dans les centres sociaux, concrètement ça donnerait quoi ?**


![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/f25e877e-cebe-486f-bdd5-8521ec78e08c.jpg)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/e483f884-c6e2-488a-aa2d-4dc5810b4d23.jpg)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/983bb38b-5d55-4f09-bd9e-7e661d195cab.jpg)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/d20943cf-05f8-4c4f-b33f-1e35d6914868.jpg)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/4170ab21-81a5-4142-950e-7501b7245d31.jpg)







