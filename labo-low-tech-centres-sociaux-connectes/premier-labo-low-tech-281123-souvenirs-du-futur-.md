# Premier Labo Low Tech : Souvenirs du futur !

Au printemps 2025, nous célébrons les réussites du Labo Low Tech. Racontez-nous !

Dans le Labo Low Tech... j'ai...on a... ?
Avec le Labo Low Tech...j'ai...on a... ?
Dans mon centre social...j'ai...on a... ?
A la fédération...




> Dans mon centre social, on a créé un groupe de récup' et bricolage avec du matériel informatique qui se réunit tous les mois pour faire de la réparation, de l'apprentissage/découverte, de la création de nouveaux objets...

> Ici un autre souvenir du futur...

Chloé CTN
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/000de0d9-c85b-47bc-986c-ba730ff90246.jpg)

**Maxime - coordinateur Flandre Maritime**

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/8721eabf-af04-4cf1-be5a-c3e819d7b40f.jpg)

**Clémence - Coordinatrice Valenciennois**

"Grâce au Labo Lowtech, nous avons pu répondre aux attentes des centres sur les questions d'écologie et les accompagner dans leurs projets: 
- Création d'un cyber centre mobile qui répond aux thématiques de mobilité douce, transition numérique et accessibilité du numérique en s'appuyant sur les énergies renouvelables
- La mise en place d'une cuisine extérieure entièrement Lowtech
- Alimentation d'une borne numérique grâce à l'éolien
- Une cartographie des acteurs du domaine sur le Valenciennois"
