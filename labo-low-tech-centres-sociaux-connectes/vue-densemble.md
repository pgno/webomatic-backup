# Labo Low tech des Centres Sociaux Connectés du Nord Pas-de-Calais 2023-2024

- [Les explorateurs du Labo](https://pad.lescommuns.org/labo-low-tech-laborantins)
- [Les rendez-vous du Labo](https://pad.lescommuns.org/labo-low-tech-rendez-vous)
- [C'est quoi les Low tech ?](https://pad.lescommuns.org/labo-low-tech-definitions)
- [Expériences Low Tech avec le centre social](https://pad.lescommuns.org/labo-low-tech-exeriences-centre-social)
- [Ressources dans les Hauts-de-France : Actions, lieux, collectifs](https://pad.lescommuns.org/labo-low-tech-ressources-hdf)
- [Ressources documentaires](https://pad.lescommuns.org/labo-low-tech-ressources-documentaires)
- [Formation expérimentale au Numérique éthique : En route vers la Low Tech](https://pad.lescommuns.org/labo-low-tech-formation-expe)
- [Vivons Low Tech Leven : La Fédération NPDC partenaire associée au projet Interreg](https://pad.lescommuns.org/vivons-low-tech-leven)