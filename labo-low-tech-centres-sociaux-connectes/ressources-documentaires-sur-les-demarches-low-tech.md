# Ressources documentaires sur les Low Tech

## Documents

### "Droit au don"? pour les ressourceries
Redemander à Mélia le document type 

### Cartographie des Low-Tech 
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/1598e7b6-7ded-45d9-8fc0-353e8033ea70.png)

Réalisée par le dessinateur/illustrateur Lillois VITO 
https://www.facebook.com/photo/?fbid=921156836042702&set=pcb.921157092709343

## Site web
### Association Entropie
https://www.asso-entropie.fr/fr/
https://www.asso-entropie.fr/fr/design-libre/catalo/

### Low Tech Lab 
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/dd135460-0d7f-4d5e-8853-12b7dc2c74e8.png#left =150x)

Alors nous nous sommes donnés pour mission de partager les solutions et l’esprit low-tech avec le plus grand nombre, afin de donner à chacun l’envie et les moyens de vivre mieux avec moins.
https://lowtechlab.org/

**Les tutos**
https://wiki.lowtechlab.org/wiki/Tutorials

**Le groupe Facebook**
https://www.facebook.com/groups/1192299394158466


### Solar Brother
Entreprise spécialisée dans la vente de produits Low-Tech à énergie solaire, elle fournit les plans de ses créations en open source
https://www.solarbrother.com/creation/

## Revues, périodique

### Low Tech Journal
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/e390c52f-48be-47fd-a116-5584e4ab7021.png#left =150x)

Le mag des technologies sobres et des modes de vie résilients.
Partager l'essentiel, renoncer au superflu et lier l'utile à l'agréable.
Tous les 2 mois, 36 pages pour vous occuper l’esprit et les mains, sans prise de chou, ni greenwashing ! 

https://www.lowtechjournal.fr/

### Low Tech Magazine
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/d87535d3-e66e-4271-ba75-37efebcae74b.png#left =250x)

Low-tech Magazine underscores the potential of past and often forgotten technologies and how they can inform sustainable energy practices.

Technology has become the idol of our society, but technological progress is—more often than not—aimed at solving problems caused by earlier technical inventions. There is a lot of potential in past and often forgotten knowledge and technologies when it comes to designing a sustainable society. Interesting possibilities arise when we combine old technology with new knowledge and new materials, or when we apply old concepts and traditional knowledge to modern technology.

Low-tech Magazine was founded in November 2007. Since 2018, the magazine also runs on a solar powered server. The website is also available in Spanish, French, Dutch, German, and Polish. Since 2019, LTM also appears in print. Low-tech Magazine publishes at most 12 well-researched stories per year. Sister blog No Tech Magazine brings more regular updates.

https://solar.lowtechmagazine.com/fr/

### La pensée écologique : Low tech

https://www.cairn.info/revue-la-pensee-ecologique-2020-1.htm

### La Maison Ecologique
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/d392b1fd-74b3-4dc1-8810-53149e13f26d.png#left =150x)

– La Maison écologique c’est le premier magazine 100% écoconstruction en France, créé en 2001 sous forme associative (aujourd’hui devenu SCOP), à l’heure où ceux qui s’intéressaient au sujet étaient pris pour de doux rêveurs !

Un magazine 100% indépendant

– 100% des associés sont salariés ou anciens salariés : aucun groupe de presse aux manettes !

–  20 années de publication en faveur d’une unique cause : faire avancer l’habitat écologique et le rendre accessible à tous.

– Une ligne éditoriale unique et pratique avec de vraies informations [pas de simples communiqués de presse copiés-collés] pour que nos lecteurs puissent passer à l’action. Une large palette de reportages et d’articles : construction, rénovation, extension, autoconstruction, réalisation par des pros, habitat groupé, petits habitats, équipements…Des exigences de qualité sur les projets présentés (matériaux naturels, sains, locaux, pas ou peu émetteurs de CO2 ; faible consommation énergétique ; recherche d’autonomie en eau et en énergie ; budget raisonnable). Des enquêtes et des dossiers qui font référence, au cœur des matériaux écologiques, des équipements économes en énergie et des énergies renouvelables.

Disponible tous les deux mois en kiosque, sur abonnement, mais aussi en version numérique enrichie (photos bonus, plans, documents techniques, vidéos, liens vers les pros…).
https://lamaisonecologique.com/


## Livres

### L'âge des low-tech
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/98dfe99a-59ce-45df-ab91-5d67bea6cec7.png#left =150x)
**Philippe Bihouix - 2021**



Face aux signaux alarmants de la crise environnementale globale – changement climatique, effondrement de la biodiversité, dégradation des sols, pollution généralisée, tensions sur l’énergie et les matières premières –, nous fondons nos espoirs sur les technologies « vertes » et le numérique.

Plus consommatrices de ressources rares, plus difficiles à recycler, trop complexes, ces nouvelles technologies nous conduisent pourtant, à terme, dans l’impasse. Ce livre démonte les mirages des innovations high tech, et propose de questionner la course en avant technologique en développant les low tech, les « basses technologies », plus sobres et plus résilientes. Il ne s’agit pas de revenir à la bougie, mais d’explorer les voies possibles vers un système économique et industriel compatible avec les limites planétaires.

Philippe Bihouix est ingénieur, spécialiste des ressources minérales et des enjeux technologiques associés, il est l’auteur de Le bonheur était pour demain (Seuil, 2019) et Le Désastre de l’école numérique (avec Karine Mauvilly, Seuil, 2016).

### Low-tech: Repenser nos technologies pour un monde durable - Conseils et témoignages 
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/dd72c06f-e837-4aac-8f4d-ca8a142625d7.png#left =150x)

Meuble garde-manger, marmite norvégienne, chauffe-eau solaire, puits canadien, poêle de masse, phytoépuration, toilettes sèches…

Fiches détaillées et explicatives, carnets de bord et cheminements pour trouver les meilleures solutions, c’est un véritable guide-témoignage qui nous permet de repenser nos modes de vie sans perdre en confort et en qualité !


### Perspectives low-tech
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/3ac766a7-623f-4306-ae22-964932fc435a.png#left =150x)
**Quentin Mateus et Gauthier Roussilhe - 2023**

Plus nous avançons dans un siècle incertain, plus nous prenons la mesure de la fragilité des systèmes techniques qui structurent nos modes de vie. La low-tech, qu’on oppose généralement à la high-tech, interroge nos besoins dans un monde contraint. S’il n’est pas dépourvu d’ambiguïtés, ce mouvement dynamique pourrait bien participer à reconstituer des cultures techniques et conviviales, d'autres manières de vivre et de s’organiser. Qu’il soit rattrapé par des logiques marchandes et autoritaires, ou qu’il constitue un levier d’émancipation, la question que pose en creux le mouvement low-tech est celle des chemins techniques à prendre pour refonder nos sociétés sur des bases viables, justes et désirables.

https://www.editionsdivergences.com/livre/perspectives-low-tech


### Objets low-tech du quotidien
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/89c5be9f-559b-46c5-b2ce-3bb277729dc0.png#left =150x)
Imaginez-vous petits et gros ustensiles (blender, frigo, cuisson...) sans électricité ni pétrole ? Des objets low-tech du quotidien que l'on peut fabriquer et réparer facilement, tout en prenant soin de la planète et des humains ? Alizée Perrin et Yoann Vandendriessche proposent de fabriquer germoir, frigo du désert, garde-manger, marmite norvégienne, blender à pédale... Des objets utiles, parfois pleins de poésie qui nous permettent de réduire notre consommation énergétique domestique. Accessible à tous, même aux bricoleurs novices et aux citadins !


## Vidéo

### Low Tech, les batisseurs du monde d'après

### Web série Ecolowgie
http://evandebretagne.fr/3-minutes-pour-changer-ses-habitudes/

### Nomades des mers
Sur le catamaran Nomade des mers, l'ingénieur Corentin de Chatelperron voyage à travers le monde à la découverte des perspectives de la low-tech. Une séduisante aventure maritime pour repenser nos usages. 
https://boutique.arte.tv/detail/Nomade_des_mers