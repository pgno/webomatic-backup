# Expérimentation pédagogique de la Fédération


Articulée autour des axes principaux de la low-tech*, cette formation vise à  :
* donner les clés de lecture et de compréhension des enjeux liés la transition écologique et sociale autour des outils et dispositifs numériques 
* favoriser la prise d'initiative d'acteurs de proximité pour intégrer ces thématiques dans leurs projets et actions avec les habitants
* initier une démarche collaborative d'échange et de travail sur la constitution de nouveaux formats et d'une documentation partagée


*La ou les low-tech, littéralement basses technologies, désignent une catégorie de techniques durables, simples, appropriables et résilientes. 



**Objectifs pédagogiques** : 
Au terme de la formation, l’apprenant saura se positionner sur les objectifs suivants : 
- Je connais les grandes thématiques de la low-tech et le vocabulaire qui lui est associé
- Je sais donner au moins un exemple de pratique, outils, projets significatifs… pour chacune des grandes thématiques
- Je sais replacer les principes de la low-tech dans le cadre plus large des enjeux globaux de la transition écologique et sociale
- Je sais identifier les aspects d'améliorations numérique et éthique dans un projet en me basant sur les principes de la low-tech
- Je peux intégrer tout ou partie des principes de la low-tech dans la conception de mes projets

**Moyens pédagogiques** : 
La formation est dispensée en format hybride présentiel /distanciel et exploite majoritairement des pédagogies actives : 
- Temps de formation en autonomie, essentiellement sous forme de lecture, visionnage
- Travaux de pratique personnel à travers des exercices et mise en situation
- Mise à disposition et enrichissement collaboratif d’un « centre de ressource » autour de la thématique low-tech
- Co-développement des parcours apprenants

**Evaluation de la formation** : 

- Evaluation en pair à pair des rendus pour chaque modules (fiche projet, note d’étonnement, analyse d’outils et de pratique)
- Évaluation d’un travail réalisé pendant la formation et constitué de deux parties : une partie personnelle et une partie réalisée collectivement illustrant l’appropriation de la thématique à travers les exercices réalisés.
