# L'utilité technologique

Nous aborderons en conclusion du panorama des thématiques la notion d'utilité. 

Il est relativement facile d'en dresser un large portrait, notamment si on regarde la satisfaction de besoins essentiels et vitaux. Mais définir précisement ce qui est utile et ce qui ne l'est pas un exercice complexe et subjectif : la réponse sera évidemment différente d'un contexte à un autre, voire d'une personne à une autre. 

Ce court module vous permettra par ailleurs de faire la synthèse de vos acquis et de finaliser vos travaux personnels en vue de la journée de restitution et clotûre de la formmtion.

:::success
Rappel : notre plan de formation
- [x] : Journée de lancement, introduction
- [x] : L'accessibilité, un numérique simple pour tous
- [x] : L'impact des technologies (1/2) : impact écologique
- [x] : L'impact des technologies (2/2) : impact social et économique
- [x] : Faire soi-même, avec les autres
- [x] : La durabilité technologique
- [x] : **L'utilité technologique** 
- [ ] : Journée de clôture
:::
## Utile ?

### Satisfaire les besoins essentiels

Dans le domaine des technologies, l'utilité fait référence à la mesure dans laquelle une technologie répond efficacement aux besoins et aux attentes des utilisateurs. Cette notion englobe la facilité d'utilisation, la pertinence, l'efficacité et la satisfaction apportées par la technologie dans l'accomplissement d'une tâche ou la résolution d'un problème. Une technologie utile doit donc non seulement être fonctionnelle et performante, mais aussi accessible et intuitive, de manière à offrir une expérience utilisateur optimale. 

Toutefois, dans une démarche low-tech, la notion d'utilité est abordée sous un angle plus spécifique. Dans ce cadre, une technologie est utilie si elle contribue à rendre possible des modes de vie, de production et de consommation sains et pertinents pour tous dans des domaines aussi variés que l’énergie, l’alimentation, l’eau, la gestion des déchets, les matériaux, l’habitat, les transports, l’hygiène ou encore la santé. En incitant à revenir à l’essentiel, elle redonne du sens à l’action.

:::success
**Les toilettes sèches : 3 contextes, 3 approches :**  

![Oxfam](https://www.oxfamfrance.org/app/uploads/2021/11/oxfam_humanitaire_wash_toilettes_Somalie_@PabloTosco_Oxfam.jpg)

Dans les zones du monde où l'eau manque, la question des toilettes est un enjeu majeur de d'hygiène et de qualité de vie. L'ONG Oxfam y dédit une partie de son action pour apporter des solutions simples et facilement mobilisables dans un esprit low-tech.

Dans une autre approche, la fondation Bill & Miranda Gates financent un projet développé par l'université de Cranfield de toilettes sèches appelés [Nano Membrane Toilet](https://www.cranfield.ac.uk/case-studies/research-case-studies/nano-membrane-toilet). Répondant à un même besoin, l'approche technologique y est radicalement différente.

Enfin, même dans un contexte où l'eau ne manque pas, on voit beaucoup d'initiatives proumouvant l'usage des toilettes séches dans une logique d'économie de l'eau en tant que ressource à préserver.
:::

### Le numérique utile ?

Le numérique peut-il être considéré comme utile ? Il y a quelques décénnies  seulement, le numérique ne serait surement pas apparu comme un apport utile et essentiel au développement de nos sociétés. Aujourd'hui, il est présent dans la quasi-totalité de nos organisations  : santé, culture, lien social, ... et rend de nombreux services dont l'impact peut être positif. Il est donc aussi vain de qualifier le numérique d'inutile, même si il existe évidemment de nombreux services dont les qualités n'ont rien d'essentielles.

> Envie d'en savoir plus sur nos données, le poids écologique et leur utilité, écoutez le podcast de France Culture : ["Datacenter, l'énergie n'est pas donnée"](https://www.radiofrance.fr/franceculture/podcasts/la-science-cqfd/datacenters-l-energie-n-est-pas-donnee-1063649)

Toutefois, si l'utilité du numérique est plus rarement remis en cause, il se dessine un consensus concernant l'inflation exponentielle du volume de données générées par les usages numériques. Avec l'apparition des technologies dites "cloud", et le récent avénement de l'intelligence artificielle, le besoin en stockage des données (et des infrastructures adéquates) croit a un rythme impressionnant. Et il est acquis aujourd'hui de considérer qu'une majorité de ces données ne présentent aucune utilité.

:::success
**Le databerg, la partie immergée de nos données**

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/49926eef-7ac7-4bb1-bf38-8712cd02fdc9.jpg)

Le [Rapport Mondial Databerg de Veritas](https://www.veritas.com/fr/fr/news-releases/2016-03-15-veritas-global-databerg-report-finds-85-percent-of-stored-data) Révèle que 85 % des Données Stockées Sont Soit Obscures, Soit Redondantes, Obsolètes Ou Inutiles (ROT)

Seulement 15 % des données s'apparentent à des informations stratégiques. La surcharge des données pourrait coûter aux entreprises jusqu'à 2 900 milliards d'euros d'ici 2020.
:::


## En conclusion

Dans les sociétés globalisées s'est développé en parrallèle de l'omniprésence numérique un discours plutôt technophobe, dénonçant une aliénation aux usages technologiques à l'opposé d'une vision utile et essentiel de la technologie. Cette vision s'oppose notamment à une approche dites "technosolutionniste" qui voit en la technologie l'outil par lequel résoudre les enjeux sociaux, économiques et environnementaux auxquels nous faisons face.

A travers l'exploration des différentes facettes du numérique éthique et responsable, nous pouvons comprendre que le regard porté sur la technologie par nos sociétés n'est pas une question technique : il s'agit d'un débat politique et d'enjeux cruciaux de démocratie. Comme nous l'avons évoqué en introduction de cette formation, se loge dans le paradoxe technologique nombre des tensions sociétales à l'oeuvre au sein des sociétés développées, ainsi que leurs rapports aux pays dits en développement.

Dans ce contexte, l'approche low-tech propose une synthèse qui refuserait de renoncer au développement des techniques tout en prenant en compte l'ensemble de ces enjeux à l'échelle planétaire. 

Si il peut sembler qu'il existe une fracture importante entre les approches high-tech et low-tech, on a pu voir se dessiner au fil des modules un impératif qui les rapprochent : la sobriété. En dehors des positions extrêmes sur ces sujets et au delà de l'urgence écologique,  la sobriété technologique favorise l'équité et l'accès aux technologies pour un plus grand nombre, en réduisant les coûts et en évitant la surconsommation inutile. Enfin, elle encourage une réflexion plus profonde sur nos véritables besoins et sur l'impact de nos choix technologiques, soutenant ainsi un développement plus harmonieux et durable des sociétés à l'échelle mondiale.

Si le panorama qui vous a été proposé à travers l'ensemble de ses modules ne peut avoir été exhaustif tant le sujet est vaste, vous avez maintenant en main un certains nombres de clés pour établir votre propre opinion sur les enjeux du numérique éthique et sur la pertinence d'une approche low-tech pour y répondre.

## Travaux

:::warning
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/f3e92cbd-1ce6-4a29-a02d-a2448f1c3f92.png =20x)
Durée de la séquence : 50mn
:::


## Glossaire, interview, portfolio, ressources

Vous devez dorénavant consacrer une partie de votre temps de travail dispnible sur ce module pour avancer sur vos différents travaux. Voici une petite liste de suivi pour vous permettre de faire le point.

### Glossaire
- [ ] Y-a-t'il de nouveaux termes à intégrer?
- [ ] Puis-je améliorer une définition, lui apporter un exemple ?

### Interview
- [ ] J'ai identifié mon interlocuteur
- [ ] J'ai pris rendez-vous avec mon interlocuteur
- [ ] J'ai constituté ma trame d'interview, les questions que je souhaite poser
- [ ] Je sais quel type de restitution je veux réaliser
- [ ] J'ai conduit l'interview
- [ ] Jai finalisé la restitution

### Portfolio
- [ ] J'ai choisi mon thème
- [ ] J'ai choisi l'angle, la problématique que je traite au sein de ce thème
- [ ] Je sais quel type de restitution je veux réaliser
- [ ] J'ai commencé à produire les éléments de mon portfolio (textes, sons, images...)
- [ ] J'ai finalisé mon portfolio

### Ressources
- [ ] Y-a-t'il des ressources (livres, site web, podcast, film, ...) que je puisse partager sur https://pad.lescommuns.org/Labo-Low-Tech ?

