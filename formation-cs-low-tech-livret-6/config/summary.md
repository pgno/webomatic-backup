---
title: Livret 6 - L'utilité technologique
licence : CC-BY-SA
author : Centres Sociaux Connectés
cover: https://hot-objects.liiib.re/pad-lescommuns-org/uploads/49926eef-7ac7-4bb1-bf38-8712cd02fdc9.jpg
collection : Projet de pierre
---

- [Livret 6 - L'utilité technologique](https://pad.lescommuns.org/formation-cs-low-tech-utilite)
