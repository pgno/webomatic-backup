# Dépôt de backup 

Dans ce dépôt sont stockés les backup réalisés depuis https://webomatic.vercel.app

Dans l'écran d'édition d'un projet, le bouton "Backup project" déclence un commit sur ce dépôt pour le projet en cours. 

Chaque projet est sauvegardé dans son propre dossier selon le "slug" du projet. A l'intérieur de chaque dossier on retrouve : 
- les fichiers markdown de chaque section du projet
- dans le dossier /config, une sauvegarde de la configuration print (JSON contenant les paramètres utilisés dans l'application) et les styles personnalisés
- dans le dossier /print, un seul fichier html statique qui inclue la totalité du projet au format HTML, les styles générés et l'inclusion du script paged.js qui génére le projet au format print. En téléchargeant ou en copiant-collant le contenu de ce fichier, il suffit alors de l'ouvrir dans un navigateur pour retrouver le format print complet. En utilisant la fonction Imprimer du navigateur et en sélectionnant comme imprimante "Imprimer dans un fichier PDF", on récupère son projet prête l'impression en PDF. On peut éditer les styles et le HTML en local.
