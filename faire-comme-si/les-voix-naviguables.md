# Micro-festival “Les Voix Navigables” ou "Les Eaux Intérieures"
*Festival d’écoute et de création sonore et radiophonique dédié à l’eau dans la ville et la vie des habitant.e.s.*

Eau élément, milieu, symbole, risque. Eau réalité : Vie des bateliers et des riverains, loisirs et tourisme fluvestres, écosystèmes naturels à la lisière entre terre et eau douce, sociabilités au bord de l’eau, sociologie de l’insularité, économie de la mobilité fluviale. Eau poésie. 

Le festival explore et soutient une diversité de formes d’expression sonore et de dispositifs d’écoute ancrés dans le paysage, le territoire et les milieux de vie des urbain.e.s. Il se niche sur l’île des Bois Blancs, à Lille, mais invite des créateur.ices et auditeur.ices au-delà de ses frontières fluviales. Il se déplace même le long des voies navigables et fait halte là où on veut l’accueillir.

Le festival est un temps fort porté par Les Ondes Blanches - Espace sonore de documentation, d’expression et de création pour le quartier des Bois Blancs, dont l’objet est de créer et rendre vivantes des archives populaires du quartier, de contribuer à son histoire orale, de cartographier son paysage sonore et de faire de la diversité des sons collectés une matière première pour des créations collectives originales, avec les habitant.e.s, artistes et artisan.e.s sonores du quartier. 

