# Partage
Licence CC-BY-SA applicable sur l’ensemble de ces élucubrations, leurs combinaisons, et toutes les formes qu’elles ont pu, peuvent et pourront prendre aussi (qu’il advienne que pourra). 

# Attribution : 
Emilie dite Bougie par contraction de deux noms. Quelques jours pressés d’octobre deux mille vingt-trois. Première auto-édition performative. Achevée d’imprimer les tous premiers jours de novembre de la même année à la photocopieuse du boulot. En police Garamond 11 pour faire comme dans les livres.

En toutes lettres et en toutes langues, raconte que ces mots ont vu le jour -ou même juste la lumière- ailleurs que sur le papier : embougie [at] GrandMechIAntLoup.com

Date d’impression :
………………………………………