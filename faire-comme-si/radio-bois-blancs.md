# Radio Bois Blancs
*Webradio participative de quartier*

Sur une île dans la Ville de Lille, des habitants s’outillent, se forment et se lancent pour créer une webradio locale, participative et créative. 
Inspirée par tant d’autres avant elle, poussée par une identité de quartier toujours à questionner, Radio Bois Blancs est un petit outil médiatique dont les habitant.es prennent soin en commun. Ouverte à toutes sortes de propositions et réalisations documentaires, poétiques, ludiques et musicales.
