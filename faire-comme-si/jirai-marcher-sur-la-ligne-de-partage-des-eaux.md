# J'irai marcher sur la ligne de partage des eaux

Créer un sentier de Grande Randonnée
En longeant les points les plus hauts
En risquant de laisser couler les pensées 
d’un côté, ou de l’autre
