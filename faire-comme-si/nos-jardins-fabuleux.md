# Nos Jardins Fabuleux
*Atlas de la diversité terrestre et fluvestre à Bois Blancs*

Nous qui portons la voute céleste sur nos épaules, voyons-nous seulement ce qui vit, git et interagit à nos pieds ? Qui sont les êtres qui, accessibles à notre attention ou cachés par notre empressement, partagent le sol sous nos pas, l’air que nous respirons et l’eau qui nous entoure, sans nous interpeller ? Comment les regarde-t-on ? Comment les nomme-t-on ? Comment les comprend-on ?

Observation après observation (une image, un nom, un lieu, une date, une histoire), saison après saison, les marcheurs du quotidien ou de passage cartographient, dans toute leur diversité, des témoins de la polyphonie végétale, animale, fongique et minérale de l’île des Bois Blancs. Des arpenteurs* composent ainsi, rencontre après rencontre, le bel ouvrage qui saisit les innombrables personnages qui peuplent notre milieu. 
Bouleau et passereau, mais aussi vipérine commune, lysimaque, libellule demoiselle, accumète et peut-être noctule… Que produit notre rencontre ? Que ressentons-nous et quel sens lui donnons-nous ? Que voulons-nous en raconter ?

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/d42f6ef1-cd5e-4dc9-839d-1ae1c7911faf.jpg)
*Didier Rousseau-Navarre, Peau de crue, 
Le Havre, 30 octobre 2023*

L’atlas est un œuvre de "conscience documentaire" autant qu’une invitation quasi-mésologique 
à nous considérer un.e [agencé.e] parmi les autres, sur nos 168 hectares d’écoumène. 

*(document-alistes, -aristes, -eurs)


    			
