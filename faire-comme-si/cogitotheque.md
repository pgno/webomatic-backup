# Cogitothèque

* Sur les sentiers karstiques de l’anthropocène, 18 septembre 2018
* Fablab mobile dédié à la création sonore, 25 octobre 2018
* Cabinets de curiosité sans eau potable, 2018
* Atopolis, 19 mars 2019 
* Microfestival Les Eaux Intérieures, février 2020
* Les Ondes Blanches, février 2020
* Radio Bois Blancs, mars 2020 (pendant et bien avant le Covid)
* Fab-U-laB, mars 2020
* Open Source Radio, 202.
* Sur les berges de Bois Blancs, 2020
* Sur les pistes de Vinciane Despret, 2021
* Nos jardins fabuleux, atlas de la diversité terrestre et fluvestre à Bois Blancs, 2023
* Les Avalantes, 2023
* J’irai marcher sur la ligne de partage des eaux, 2023
* Catalogue de belles choses, octobre 2023
