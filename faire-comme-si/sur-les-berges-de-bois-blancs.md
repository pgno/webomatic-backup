# Sur les berges de Bois Blancs

Berges et marges ont quatre lettres en commun. Qu’ont à partager les ferrailleurs de la Deûle, les pêcheurs silencieux, les résident.e.s des Aviateurs, celles et ceux qui vivent sur une péniche, celui qui a planté sa tente sous le saule pleureur, et ces autres qui marchent le long du canal ?

Documentaire sonore. Chaque portrait est un épisode d’une série qui trace les berges de l’île.
