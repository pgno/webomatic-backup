# Sur les pistes de Vinciane Despret

Quelles traces laisse sur nous la lecture des ouvrages de Vinciane Despret ? Qu’a–t-elle activé ? Qu’a–t-elle provoqué ? Qu’a-t-elle rendu possible ? « Raconte-moi ce qu’il s’est passé après que tu aies lu Vinciane Despret. » 

Collecte sans fil de témoignages de lecteurs et lectrices de ses ouvrages, pour documenter la réception de son œuvre. Et suivre les pistes qui s’ouvriront.
