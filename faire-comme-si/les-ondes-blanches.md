# Les Ondes Blanches
*Espace sonore de documentation, d'expression et de création à Bois Blancs*

A la croisée des chemins entre media documentaire, espace d'expression populaire et œuvre poétique collective. Un media très local et hyper sensible qui explore autant la mémoire des époques et événements vécus par les habitants, les vies singulières qui s'y sont installées et se trament aujourd'hui, que les projections de futurs qui s'inventent quand on leur en donne le temps. Une œuvre liquide : collaborative, cumulative et inventive au long cours, faite à la main et au micro par les habitants du quartier, pour une collection d'archives populaires, une exploration cartographique du paysage sonore et une plongée dans les imaginaires de ses habitants.
