# Les Avalantes

Une péniche navigue sur les eaux intérieures, à la lenteur des voies qui se cherchent, avec, à son bord, des livres, des zines et d’autres ouvrages à lire, toucher ou écouter, des assises pour faire halte, courte ou longue. A bâbord, un atelier de micro-édition, à tribord une table de conversation radiophonique. Bief après bief, on y cherche d’autres voix navigables, on y coule d’autres langues, on y crée d’autres attaches.

Bateau d’éditions, Les Avalantes donne corps à des lettres et des formes, balises pour des pensées flottantes et des savoirs discrets. Espace de canalisation ou de débordement, la péniche est une invitation temporaire, à chaque port fluvial où elle s’amarre. Librairie très choisie et bibliothèque qui renait des dons de celles et ceux qui l’ont abordée, elle s’éloigne volontiers, dérive, pour aller à la rencontre des librairies et éditions terrestres et indépendantes. Petite imprimerie, elle jette l’encre et le papier sous les mains de ses résidentes passagères. Cabine de collecte et d’infusion sonore, elle remonte à la source de nos conditions fluvestres et célèbre ainsi l’art de mettre en lien les aménités de nos berges et celles de nos voies d’eau. 

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/67d67e82-e00a-4b90-a3b7-c65682a89ce6.jpg)
