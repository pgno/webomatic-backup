---
title: Faire Comme Si, Invitations spéculatives et projets rarement advenus
author: emilie b.
cover: https://hot-objects.liiib.re/pad-lescommuns-org/uploads/67d67e82-e00a-4b90-a3b7-c65682a89ce6.jpg


---

# Faire comme si
**Catalogue de projets non advenus**

- [Catalogue de belles choses](https://pad.lescommuns.org/emilieb-catalogue-belles-choses)
- [Fab-U-laB](https://pad.lescommuns.org/emilieb-fab-u-lab)
- [Les Avalantes](https://pad.lescommuns.org/emilieb-les-avalantes)
- [Sur les sentiers karstiques de l’anthropocène](https://pad.lescommuns.org/emilieb-sentiers-karstiques)
- [Open Source Radio](https://pad.lescommuns.org/emilieb-open-source-radio)
- [Atopolis](https://pad.lescommuns.org/emilieb-atopolis)
- [Les Voix Naviguables](https://pad.lescommuns.org/emilieb-voix-naviguables)
- [Fablab mobile dédié à la création sonore](https://pad.lescommuns.org/emilieb-fablab-son)
- [Nos Jardins Fabuleux](https://pad.lescommuns.org/emilieb-jardins-fabuleux)
- [Les Ondes Blanches](https://pad.lescommuns.org/emilieb-ondes-blanches)
- [Radio Bois Blancs](https://pad.lescommuns.org/emilieb-radio-bois-blancs)
- [Sur les berges de Bois Blancs](https://pad.lescommuns.org/emilieb-sur-les-berges)
- [Sur les pistes de Vinciane Despret](https://pad.lescommuns.org/emilieb-pistes-despret)
- [J’irai marcher sur la ligne de partage des eaux](https://pad.lescommuns.org/emilieb-partage-des-eaux)

- [Cogitothèque](https://pad.lescommuns.org/emilieb-cogitotheque)
- [Générateur](https://pad.lescommuns.org/emilieb-generateur)
- [Partage](https://pad.lescommuns.org/emilieb-partage)





