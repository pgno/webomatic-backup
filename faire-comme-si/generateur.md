# Générateur aléatoire de projets à faire rêver
*à imprimer, découper, tirer au sort, faire résonner et remettre à demain*


: geste :  recette :  piste :  correspondance : fiction : habiter :
: résonance : enquête : eau : bateau : langues : radio : île :
: voix : sentiers : livre : imprimer : fanzine : poésie : risque :
: atelier : carte : lettres : récits : milieux : fragments : archive : 
: festival : spéculations : ouvert : mémoire : media : relier :
: recherche : cartographie : narration : oralité : uchronie : 
: imaginaire : design : document : documentaire : traduction :
: documentation : faire : libre : futurs non advenus : mots :
: sensible : œuvre : création : numérique : écouter : corps :
: inventaire : journal : passé : sonore : paysage : cours d’eau : 
: utopie : abécédaire : collectif : commun : lieu : territoire :
: quartier : micro : papier : partage : ligne : résurgence : liens :
: source : sens : écoute : atlas : voisinage : transmission :
: collection : flottante : balise : canal : bois blancs : pont :
: voisin.e.s : dictionnaire : normographe : envers : regard :
: coordonnées : géodésique : perspective : traversée : 
: agencement : singulier : polyphonique : 
