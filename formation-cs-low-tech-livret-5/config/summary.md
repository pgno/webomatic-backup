---
title: Livret 5 - La durabilité technologique
licence : CC-BY-SA
author : Centres Sociaux Connectés
cover: https://hot-objects.liiib.re/pad-lescommuns-org/uploads/6027370f-eb24-4026-bf28-5d050a6b9377.png
collection : Projet de pierre
---

- [Livret 5 - La durabilité technologique](https://pad.lescommuns.org/formation-cs-low-tech-durabilite)
