# La durabilité technologique

Dans ce module, nous allons nous intéresser à la notion de durabilité.

Ce vaste sujet s'applique évidemment bien au-delà des problématiques technologiques. Il est toutefois essentiel à une démarche responsable et low-tech du numérique.

Dans notre contexte, la question de la durabilité fait principalement écho à la manière dont sont conçues les technologies, quels matériaux utilisent-elles, sont-elles recyclables, réparables... mais aussi à l'usage du numérique pour concevoir et fabriquer des objets techniques.

:::success
Rappel : notre plan de formation
- [x] : Journée de lancement, introduction
- [x] : L'accessibilité, un numérique simple pour tous
- [x] : L'impact des technologies (1/2) : impact écologique
- [x] : L'impact des technologies (2/2) : impact social et économique
- [x] : Faire soi-même, avec les autres
- [x] : **La durabilité technologique**
- [ ] : L'utilité technologique 
- [ ] : Journée de clôture
:::

## Durable

### Limiter la consommation et la production de déchêts

Un des premiers pas dans le développement d'une technologie durable est liée à l'usage de ressources. Nous avons pu voir dans le module sur l'empreinte écologique que la quantité de déchêts et de matières premières liées au numérique est préoccupante. C'est un des paris de la low-tech que de maintenir un développement technologique qui soit compatible avec les limites planétaires, et donc durable.

Les mouvements "zéro déchêts" prone ainsi une démarche en 5 points : Refuser, Réduire, Réutiliser, Redonner à la terre, Recycler. On voit ainsi que le point de départ est tout d'abord de ne pas produire de déchêts et d'adopter une attitude responsable en tant que consommateur. Si cela s'entend aisement lorsqu'il est question de produits de consommation courante, comme refuser un sac plastique à la caisse d'un magasin, il est en de même pour la technologie que l'on peut passer à un crible similaire : 
- ai-je réellement besoin de cet achat ?
- existe-t-il une alternative générant moins de déchêt, utilisant des matériaux recyclés ?
- Puis-je l'emprunter au lieu de l'acheter ?
- cet achat sera-t-il recyclable en fin de vie, et quelle est sa durée de vie ?
- ...

Autour de cette dynamique de nombreuses initiatives existes, et elles sont fréquemment associées à une démarche de "DIY" lorsqu'il s'agit de réparer ou auto-produire les objects techniques et technologiques dont l'on a besoin et qu'il n'existe pas ou peu d'alternatives non durable. Associées, elles permettent de répondre à la fois à des enjeux écologiques, économiques (réduction des coûts), et sociales (réappropriation de savoir-faire, création de lien social.... 

Une des difficultés particulièrement présente lorsqu'il s'agit de numérique est que la production de déchêts et la consommation d'énergie sont généralement invisible pour l'utilisateur. Le terme "dématérialisé" est ainsi trompeur : les procédures le sont, mais l'ensemble du traitement de ces procédures reposent encore lourdement sur une infrastructure matériel importante.

:::success
**Tipimi**

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/6283e374-d3a9-4424-ad2a-a2408827f02f.png)

Tipimi est une initiative née dans la métropole lilloise. Sur tipimi.fr, on poste ses objets et on les propose au prêt aux autres adhérents. En contrepartie, on reçoit des points qui nous permettent, à notre tour, d'emprunter les objets postés par les autres. Le projet d'appuie aussi un tiers-lieu doté d'une [objetothèque](https://objetotheque.fr/).
:::

###### 

### Design durable et eco-conception

La conception durable d'un objet nécessite de prendre en compte l'ensemble des piliers de la low-tech. Ce sont deux approches très similaires. On prendre soin de penser dès l'origine du projet à la consommation énergétique de l'object technologique lui même, mais aussi lors de son processus de fabrication, d'utiliser des matériaux locaux, recyclés et recyclables, ...

:::success
**L'analyse du cycle de vie**  

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/86cf8b4c-9c00-4ae7-af2f-aadb5b727567.png)

L'analyse du cycle de vie (ACV) est une méthode d'évaluation normaliséepermettant de réaliser un bilan environnemental multicritère et multi-étape d'un système (produit, service, entreprise ou procédé) sur l'ensemble de son cycle de vie.

Son but est de connaître et pouvoir comparer les impacts environnementaux d'un système de l'extraction des matières premières nécessaires à sa fabrication à son traitement en fin de vie (mise en décharge, recyclage…), en passant par ses phases d'usage, d'entretien et de transport. _[Source [Wikipedia](https://fr.wikipedia.org/wiki/Analyse_du_cycle_de_vie)]_
:::

La conception d'un produit dans une démarche durable et low-tech nécessite donc un effort particulier notamment en gardant en tête :  
- **Faire simple** : en limitant la complexité du projet, on réduit l'empreinte lié à sa fabrication et on augmente sa réparabilité. Ainsi de nombreux projets low-tech s'inspire de techniques anciennes où la sobriété et l'approvisionnement local était la seule solution.
- **Penser la fin de vie**, c'est à dire penser la déconstruction de l'objet technique : séparation aisée des composants, limiter le nombre de matériaux utilisé afin d'augementer la recyclabilité... 

Ces critères sont valables qu'ils s'agissent de la conception d'un object numérique (appareil ou application) ou non. Dans le cas de la conception d'un projet numérique, les questions de simplicité et d'analyse des impacts globaux (notamment énergétique) sont les principaux points de vigilence. Dans le cadre de la conception d'un objet non numérique, quelque soit le secteur (habillement, alimentation, habitat...), l'usage de la technologique est dans ce cas souvent envisagée comme un outil permettant d'optimiser et relocaliser la fabrication, ayant ainsi un impact positif sur la durabilité du projet.

> Eco-concevoir son site web : La mission interministérielle numérique responsable met à disposition le [Référentiel général d'écoconception de services numériques (RGESN)](https://ecoresponsable.numerique.gouv.fr/publications/referentiel-general-ecoconception/) pour accompagner les concepteurs de services numériques.
> [RGESN](https://ecoresponsable.numerique.gouv.fr/publications/referentiel-general-ecoconception/#qrcode)

:::success
**Une maison open-hardware**

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/15191596-74c5-44fc-9e4d-162b5cb8cb77.jpg)

[WikiHouse](https://www.wikihouse.cc/) est un projet open source pour la conception et la construction de maisons.Il s'efforce de démocratiser et de simplifier la construction d'habitations durables et légères en ressources. Le projet s'est développé pour devenir une communauté mondiale de contributeurs. 

WikiHouse permet aux utilisateurs de télécharger des fichiers sous licence Creative Commons à partir de sa bibliothèque en ligne, de les personnaliser, puis de les utiliser pour créer des pièces ressemblant à des puzzles en contreplaqué à l'aide d'une fraiseuse à commande numérique. La construction des structures WikiHouse ne nécessite aucune pièce spéciale car les morceaux de bois coupés s'emboîtent avec des connexions par coin et chevilles inspirées de l'architecture coréenne classique. La charpente d'une WikiHouse peut être assemblée en moins d'une journée par des personnes n'ayant aucune formation formelle en construction. 
:::

######

### Faciliter la réparation

La réparabilité, c'est à dire la capacité qu'un usager peut avoir à réparer lui même ou à faire réparer un objet est un point clé de la durabilité d'une technologie. 

La mise en place de critères, d'indices, mais aussi de réseaux de réparation s'est notamment développée en réaction au phénomène nommé obsolescence programmée. Ce terme désigne une pratique délibérée par laquelle un produit est conçu pour avoir une durée de vie limitée ou diminuée, ce qui encourage les consommateurs à remplacer leur produit plus fréquemment qu'ils ne le feraient normalement. A ce titre, des sociétés comme Apple ou Epson ont fait l'objet de poursuite suite aux dépôts de plainte de l'association française [Halte à l’obsolescence programmée (HOP)](https://www.halteobsolescence.org/).

:::success
**L'indice de réparabilité, Label Qualirepar et bonus réparation**

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/006e9ab4-091a-4e06-bc74-8814bd37eda8.png)

[L'indice de réparabilité](https://www.ecologie.gouv.fr/indice-reparabilite) a été déployé au 1er janvier 2021 sur cinq catégories de produit (smartphones, ordinateurs portables, téléviseurs, tondeuses à gazon, lave-linges hublot,lave-linges top, lave-vaisselles, aspirateurs, nettoyeurs haute-pression). 

[Le bonus réparation](https://www.economie.gouv.fr/particuliers/bonus-reparation-comment-ca-marche) est une aide financière de l’État qui vous encourage à réparer vos appareils électriques ou électroniques plutôt que de les remplacer. Elle concerne plusieurs appareils du quotidien avec des montants variables. Le bonus prend la forme d’un forfait variable selon le type d’appareil d’un montant allant de 15 à 60 €.

[Le label Qualirepar](https://certification.afnor.org/environnement/label-qualirepar) est destiné aux réparateurs d’équipements électriques/électroniques, leur permet d’offrir à leurs consommateurs un « bonus réparation », et de contribuer à l’allongement de la durée de vie des produits et la réduction des déchets.
:::

Dans la démarche low-tech, la notion de réparabilité s'étend à l'ensemble du cycle de vie du produit, où l'on prendra en compte ce critère dès la conception. Concernant les objets n'ayant pas anticipé ce critère, des réseaux se sont mis en place pour accompagner les usagers dans la démarche de réparation. L'initiative gouvernementale ["Epargnons nos ressources"](https://epargnonsnosressources.gouv.fr/) recense des conseils, bonnes pratiques et un annuaire de prestataires de service oeuvrant à la durabilité des objets de consommation. L'intiative [Repair Café](https://www.repaircafe.org/fr/) quant à elle existe depuis 15 ans. Elle regroupe et accompagne un réseau de lieux et d'évènements ayant pour but d'ouvrir l'accès à des ateliers collectifs de réparation, alliant échange de savoir-faire technique et lien social.

:::success
**Le réseau des Repair Café Haut-de-France**

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/10d3e06b-29c8-4d3f-82be-1e30a83d7e3a.png)

Un repair café est un atelier consacré à la réparation d'objets et organisé à un niveau local sous forme de tiers-lieu, entre des personnes qui habitent ou fréquentent un même endroit.
Le site réseau des Repairs Café Haut-de-France est un site porté par la  [Maison de l'Environnement et des Solidarités (MRES)](https://mres-asso.org/). Il recense les Repair Café régionaux et leurs évènements et vous donne accès à un ensemble important de ressources sur la réparation : tutoriel par type d'appareil, astuces et initiation à la réparation...
:::

###### 

## Travaux

:::warning
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/f3e92cbd-1ce6-4a29-a02d-a2448f1c3f92.png =20x)
Durée de la séquence : 90mn
:::


## Glossaire, interview, portfolio, ressources

Vous devez dorénavant consacrer une partie de votre temps de travail dispnible sur ce module pour avancer sur vos différents travaux. Voici une petite liste de suivi pour vous permettre de faire le point.

### Glossaire
- [ ] Y-a-t'il de nouveaux termes à intégrer?
- [ ] Puis-je améliorer une définition, lui apporter un exemple ?

### Interview
- [ ] J'ai identifié mon interlocuteur
- [ ] J'ai pris rendez-vous avec mon interlocuteur
- [ ] J'ai constituté ma trame d'interview, les questions que je souhaite poser
- [ ] Je sais quel type de restitution je veux réaliser
- [ ] J'ai conduit l'interview
- [ ] Jai finalisé la restitution

### Portfolio
- [ ] J'ai choisi mon thème
- [ ] J'ai choisi l'angle, la problématique que je traite au sein de ce thème
- [ ] Je sais quel type de restitution je veux réaliser
- [ ] J'ai commencé à produire les éléments de mon portfolio (textes, sons, images...)
- [ ] J'ai finalisé mon portfolio

### Ressources
- [ ] Y-a-t'il des ressources (livres, site web, podcast, film, ...) que je puisse partager sur https://pad.lescommuns.org/Labo-Low-Tech ?
