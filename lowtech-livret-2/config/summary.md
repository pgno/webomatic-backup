---
title: Livret 2 - L'impact des technologies (1/2) - Impact écologique
licence : CC-BY-SA
author : Centres Sociaux Connectés
cover: https://hot-objects.liiib.re/pad-lescommuns-org/uploads/38203983-4da8-4436-96c9-2d4f1cbf69f5.jpg
collection : Projet de pierre
---

- [Livret 2 - L'impact des technologies (1/2) : impact écologique](https://pad.lescommuns.org/formation-cs-low-tech-impact-ecologique)
