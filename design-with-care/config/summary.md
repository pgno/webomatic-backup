---
title: Design With Care
author : Emilie B.
cover : https://hot-objects.liiib.re/pad-lescommuns-org/uploads/5c22e981-ee09-47a3-9ce4-7c09eecfa7c4.jpg

---

- [Vue d'ensemble](https://pad.lescommuns.org/design-with-care-intro)
- [Cure versus Care : aux origines du soin](https://pad.lescommuns.org/cure-care)
- [Les éthiques du care : sources et controverses féministes](https://pad.lescommuns.org/care-ethiques-feministes)
- [Métamorphoses du design : quelques sources d'inspiration](https://pad.lescommuns.org/metamorphoses-design)
- [Design et régulation démocratique](https://pad.lescommuns.org/design-regulation-democratique)
- [Le design à l'epreuve de l'éthique du care](https://pad.lescommuns.org/design-et-care)
- [P.O.C. : Du Proof of Concept au Proof of Care](https://pad.lescommuns.org/proof-of-care)

- [Aux sources de l'approche institutionnelle](https://pad.lescommuns.org/approche-institutionnelle)
    - [Le concept de réification](https://pad.lescommuns.org/reification)
    - [L'éthique de reconnaissance chez Axel Honneth](https://pad.lescommuns.org/reconnaissance-axel-honneth)
    - [Le pouvoir psychiatrique chez Michel Foucault](https://pad.lescommuns.org/pouvoir-psychiatrique-foucault)
    - [Les risques psycho-sociaux](https://pad.lescommuns.org/risques-psycho-sociaux) 
    - [Faire clinique institutionnelle](https://pad.lescommuns.org/clinique-institutionnelle)
- [Enquête : l'enrichissement du design par les sciences humaines et sociales](https://pad.lescommuns.org/enquete)
    - [La place des émotions](https://pad.lescommuns.org/emotions)
- [Faire récit(s) pour soigner](https://pad.lescommuns.org/faire-recits)
- [L'art-thérapie et la médiation par les arts](https://pad.lescommuns.org/soin-arts)
- [L'Anthropocène : habiter le monde aujourd'hui](https://pad.lescommuns.org/habitabilite-du-monde)
- [Le soin par la nature](https://pad.lescommuns.org/soin-nature)
- [Des hotspots de la biodiversité aux hotspots de la vulnérabilité](https://pad.lescommuns.org/soin-hotspots)
- [Humaniser la santé connectée](https://pad.lescommuns.org/sante-connectee) 
- [Clinique de la dignité ?](https://pad.lescommuns.org/clinique-dignite)
- [Les conditions spatiales du prendre soin](https://pad.lescommuns.org/care-architecture)
- [Genre et Santé](https://pad.lescommuns.org/genre-sante)
- [Le séminaire Design With Care du CNAM](https://pad.lescommuns.org/design-with-care-seminaire-cnam)
- [La Charte du Verstohlen : Ce qui ne peut être volé](https://pad.lescommuns.org/charte-verstohlen)
- [Bibliographie extensive : les ouvrages que j'aimerais avoir entre mes mains](https://pad.lescommuns.org/design-with-care-bibliographie)
- [Glossaire du Verstohlen](https://pad.lescommuns.org/design-with-care-glossaire)
- [Accueil et hospitalité](https://pad.lescommuns.org/hospitalite)



