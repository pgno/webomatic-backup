# Petite histoire du concept de réification

- Notion de réification dans l'école de Francfort : Lukacs
- Marchandisation, Marx
- Technicisation
- Quantification
- Fétichisation (voir Bauman, pornocratie)

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/00250fbb-8b8a-4f78-80d7-74bdbe38eb85.jpeg)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/49109f02-0eaa-4c28-9311-af32da593c74.jpeg)


Par la technicisation et la marchandisation, la réification devient le mode d'être, le mode relationnel, quasi exclusif, de la modernité capitalistique. Cela provoquant un désanchantement inéluctable de type weberien, et un appauvrissement culturel sans commune mesure qui pas étranger à la crise de la culture chez Anders ou Arendt, ou encore à une réification de type techniciste qui n'est pas étrangère à l'arraisonnement techniciste heieggerien.

Sources :
Voir Vincent Charbonnier [La réification chez Lukacs (La madeleine et les centres)](https://shs.hal.science/ensl-00762337v6)
Voir Vincent Chanson, Alexis Cukier, Frédéric Monferrand (dir.), [La réification. Histoire et actualité d'un concept critique](https://journals.openedition.org/lectures/14567?lang=es)
