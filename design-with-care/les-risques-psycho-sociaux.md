# Fondements des risques psycho-sociaux

Le Rapport Nasse-Légeron (2008) : suivi statistique des risques, une quarantaine d'indicateurs

Voir Thomas Coutrot (DARES), Catherine Mermilliod (Drees) : [Les risques psycho-sociaux au travail : les indicateurs disponibles](https://dares.travail-emploi.gouv.fr/publications/2010-081-les-risques-psychosociaux-au-travail-les-indicateurs-disponibles), DARES Analyses, Décembre 2010 N°81

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/2ec1d3c6-ca1f-43ae-b4c6-5b7a907da2b8.jpeg)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/f239c163-e4a3-4613-8abd-aad557554cf2.jpeg)

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/bac33cea-879b-42cc-a422-644004606f12.jpeg)

**Ressources théoriques issues des sciences sociales** : 
- La philosophie morale et politique, philosophie de l'alienation sociale et de la banalité du mal (Marx, Arendt)
- La psychologie du travail (Yves Clot) et la psychothérapie institutionnelle (Tosquelles, Oury)
- La psychopathologie et la psychodynamique du travail (Dejours)
- La sociologie critique (de Bourdieu, Boltanski à Stéphane Haber, en passant par Vincent de Gaujelac), Lash (asbence de surmoi, "décomplexion")
- L'anthropologie (David Le Breton)
- Les héritages de la Théorie Critique (d'Adorno, Lukacs à Axel Honneth)
- L'éthique du Care (de Winnicot à Tronto)

## Sens du travail, harcèlement, suradaptation

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/8e0c4a9c-b272-4387-b3fb-ba1128df8dd8.jpeg)

Voir aussi Marie Pezé [Souffrance et travail](https://www.souffrance-et-travail.com/a-propos/)

## Asepsie, toxicité du milieu, ecophénoménalité
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/b4bb41a4-a4ef-4df9-8278-dcc5a11be9a5.jpeg)

## Théorie critique, les avatars de la réification
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/c2c443ca-50c4-4998-ba63-2318e701679c.jpeg)

## Le retour inégalitaire
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/105f3af7-cc51-453b-bdea-db203ef4f782.jpeg)

## Usure et acrasie
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/3c06383c-1b97-459c-b265-47435935b2e7.jpeg)

Acrasie = absence de force d'ame
La banalité du mal est une acrasie paresseuse

Acrasie sténique = zèle

Ecole Dejours a essyé de quitter le principe de positionner la souffrance à partir d'une approche psychopatho individuelle. Le premier à vouloir sortir de ce schéma qui consiste à individualiser le dysfonctionnement. Il faut basculer avec du psychodynamique. C'est un dysfonctionnement collectif. Point de départ de cette compréhension : ce sont souvent les plus intégrés qui souffrent d'usure mentale, de souffrance éthique... souffrance précisément liée à l'engagement. on se sent soi-même garant de certaines valeurs. Il y a une atteinte au sens de la cohérence, une atteinte saluto-génique. 
L'individu n'est pas le système le plus à même de protéger l'invidiu. C'est l'acion collective qui doit protéger l'individu.

L'épuisement professionnel est la conséquence d'une conduite de suradaptation. Faux self

Richard Sennett éloge de l'urbanité, du masque ; tyrannie de l'intimité ; "l'extime" (repris par Tisseron) : idée que mon débordement émotionnel a totalement sa place à un endroit... société mangée par les tyrannies de l'intimité. Cela provoque chez l'individu un sentiment de toute puissante, puis phénomène de fragilisation qui se met en place et dont ils n'ont pas tjrs conscience. Si vous exposez tous les jours votre intimité, vous finissez par tomber malade. L'intimité est faite pour être protégée et éventuellement partagée avec ceux qui sont désignés par nos affinités electives. Quand l'intimité est sans cesse mise en avant, phénomène clinique de fatigue, de salissement, de fragilisation, de contamination. Physiologiquement, la peau de protection s'abime, d'une certaine manière. C'est poreux.

On est tous en représentation. Mais plus ou moins... il y a des conduites à risques. 

## Travail et usure mentale

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/22578a7c-cf16-4e6b-8539-6bbf75162905.jpeg)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/5d58a03b-68d0-402a-bfe3-a3f95646683c.jpeg)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/7a6b8e35-dfda-4811-80ab-6ffd9e53e544.jpeg)

Réification
Dans notre société, le consentement de l'inacceptable est très bien activté. 

Alerte de Dejours sur les conduites kamikazes : conduite à risque par renversement de la pulsion mortifère pour la retourner vers l'environnement

[Les décisions absurdes, Christian Morel](https://www.cairn.info/les-decisions-absurdes-I--9782070457663.htm)
sociologie du management : qu'est-ce qui dysfonne ? comprendre les mécanismes de fonctionnement de l'absurde.

## Comment prévenir et lutter contre les risques psychosociaux ?

C'est souvent une méthodologie qui combine les outils et les stratégies
- Enquêter, diagnostiquer, auditer la réalité du malaise sans mettre en danger les individus
- Faire du plaidoyer
- Former les managers à ces questions
- Sanctionner les dysfonctionnements
- Système interne de lanceurs d'alerte
slide 1
slide 2

Faire tout cela le plus en amont possible !

- Culture de violence

**Comment promouvoir le bien-être?**

- Devenir des lieux d'expérimentation
D'autant plus que les lieux sont des lieux de vulnérabibilité
Tout ce qui peut rendre les parties prenants est bénéfique et il faut un *faire*.
Le proof of care vise un résultat mais a aussi pour objet de revivifier les équipes (le poc comme moyen)

- S'articuler à "l'université", se transformer en ?
Ca renvoie à l'enquête (Latour), au multidisciplinaire, à la nécessite du compagnonnage 
- Devenir un tiers-lieu, ouvrir
- Intégrer les ODD (COP 28 : Transtionning away)

## Liens entre environnement et santé

Intensité de liens entre les services ecosytémiques et le bien-être
services ecosystémiques = veleur instrumentale de la nature
Millenium Ecosystme Assessment (MEA) = schéma de liaison du contrat naturele et du contrat social

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/82ad15e0-7139-44a6-8f09-a01a54ce0761.jpeg)

Directement lié au "One Health" / Une seule santé

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/4dd2d4e9-3406-4cb0-956d-6546d0e87c5c.jpeg)

on reconsolide les liens inauguraux, primordiaux entre l'homme et la nature

## Les approches salutogéniques

> Un milieu social salutogénique peut être défini comme un milieu qui permet à la fois le développement d'une activité de qualité, mais également la source du pouvoir d'agir (Clot et Litim, 2008). 

