# Face à l'anthropocène : Habiter le monde aujourd'hui


![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/3f085e6f-618f-4206-a693-ed8a79b16e25.jpeg)

PJ. Crutzen
J. Diamond
P. Servigne
Dominique Bourg : les dommages transcendantaux ; il y a des irreversibles

## Questionnement des ontologies

P. Descola : 4 ontologies dans le monde, manières de différentier l'homme de ce qui n'est pas l'homme : 
- Analogisme : analogie de la ville et de la société politique avec la constitution du corps avec le nous, le tumus et le chez Platon
- Totemisme : continuité des âmes et des corps
- Animisme : discontinuité des corps et continuité des âmes
- Naturalisme (occident) : continuité des corps et discontinuité des âmes Continuité des corps = nous sommes tous reliés par la théorie de l'évolution ; discontinuité des âmes 

Le naturalisme est en train de se questionner sérieusement sur ses frontières
Constitutionnalisation du vivant
[Le fleuve qui voulait écrire, les auditions du parlement de Loire, Camille de Toledo](http://www.editionslesliensquiliberent.fr/livre-Le_fleuve_qui_voulait_%C3%A9crire-9791020910066-1-1-0-1.html)

[Michel Prieur](https://www.cairn.info/publications-de-Michel-Prieur--32743.htm) : le premier à avoir vu comment d'autres cultures repensaient une gouvernance territoriale, basée sur les communs

## Habiter le monde selon Heidegger
Lire Bâtir, conférence de Heidegger de 1951 sur l'habitabilité du monde

Hbaiter le monde, c'est activer, constuire du quadriparti
= relation avec :
- la terre, le sol
- le ciel, le sacré
- la communauté des hommes
- le lien avec les vivants et les morts

(il n'y a pas de communauté politique qui ne créé par un rituel de lien à la mort)

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/384ae962-2bcf-41ed-a7a3-fd6e38b4871f.jpeg)

## L'amnésie environnementale de Peter Kahn

L'amnésie environnementale générationnelle

## Les expériences de nature : directe, indirecte, vicariante

Utilisation de la réalité virtuelle : visualisation, enveloppe sonore...

Travail des géographes de la santé 

Voir le travail de Pierre Bidon, thèse de la Chaire "[Etudes qualitatives des soins en milieu naturel](https://chaire-philo.fr/wp-content/uploads/2022/05/public-Etudes-qualitatives-des-soins-en-milieu-naturel.pdf)"

## Les héritiers de Léopold : l'éthique de la terre

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/8dc331e1-1ddd-409e-8736-18fb4847da87.jpg)

[L'éthique de la terre](https://www.payot-rivages.fr/payot/livre/lethique-de-la-terre-9782228922685), Aldo Léopold
Définition extensive de la communauté politique dans Almanach d'un comté des sables, 1949

Ecologie profonde

Refonder un lien à la nature différent

Rewild : nouveaux protocoles d'ensauvagement des milieux naturels. Ex : Vallée de la Millière

Beaucoup de salutogénèse par lien et expérience de nature

Approche scientifique de l'approche qui n'a pas été aidée par l'appropriation new-age

Ecosophie. 
Ouvrage de Rosazck
https://www.lepassagerclandestin.fr/catalogue/precurseur-ses-decroissance/theodore-roszak-vers-une-ecopsychologie-liberatrice/

## La "rotondité de la terre" de Latour et ses héritiers

Bruno Latour Où atterir ? De l'enquête au terrain de vie

Article AOC [Imaginer les gestes-barrières contre le retour à la production d'avant crise](https://aoc.media/opinion/2020/03/29/imaginer-les-gestes-barrieres-contre-le-retour-a-la-production-davant-crise/)

## Martha Nussbaum

Propose aussi des critères de vie bonne

[Les émotions démocratiques](https://editions.flammarion.com/les-emotions-democratiques/9782081259546)

[Capabilités](https://editions.flammarion.com/capabilites/9782081270770)

Article de [La Vie des Idées](https://laviedesidees.fr/Martha-Nussbaum-ou-la-democratie) par Fabienne Brugère, 2013 ([version pdf](https://laviedesidees.fr/IMG/pdf/20130319_de_mocratie_des_capabilite_s.pdf))

## Philosophie de l'environnement

* Ralph Waldo Emerson
* Henry David Thoreau
* Elisée Reclus
* Aldo Léopold
* Arne Naess
* Hans Jonas

* [Gunther Anders](https://monoskop.org/images/a/a5/ANDERS_Gunther_-_1956_-_L%27obsolescence_de_l%27homme_Sur_l%27%C3%A2me_%C3%A0_l%27%C3%A9poque_de_la_deuxi%C3%A8me_r%C3%A9volution_industrielle.pdf)
* Ulrich Beck
* Rachel Carson
* Jacques Ellul
* Ivan Illich
* André Gorz
* Lynn Margulis
* Serge Latouche
* Eloi Laurent
* Philipe Descola
* Eduardo Viveiros De Castro
* Anna Tsing
* Jared Diamond
* James Lovelock
* Gaël Giraud
* Vandana Shiva
* Donna Haraway
* Bruno Latour