## Des hotspots de la biodiversité aux hotspots de la vulnérabilité

https://fr.wikipedia.org/wiki/Point_chaud_de_biodiversit%C3%A9

[Les hotspots de la vulnérabilité et de la résilience](https://chaire-philo.fr/wp-content/uploads/2022/10/hotspot-de-la-vulnerabilite-web.pdf)
Etude exploratoire de la Chair de philosophie à l'hôpital, 2022, S. Gleizes, Sous la direction de F. Baitinger, C. Fleury