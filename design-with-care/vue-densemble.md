---
title: Design With Care

cover : https://hot-objects.liiib.re/pad-lescommuns-org/uploads/4802b593-c21d-4993-81c3-34f036206d1b.jpg

---

# Design With Care

Quelques notions et références clés pour plonger dans les pratiques et pensées du Design With Care, en s'appuyant sur les enseignements du Diplôme Inter Universitaire "Philosophie, Ethique et Design dans les domaines de la Santé et du Soin" du CNAM de Paris et de CY Ecole de Design, portée et animée par Cynthia Fleury et Antoine Fenoglio.

Ce livret organise et synthétise de manière subjective les notes d'emilie b. issues des enseignements et de ses lectures complémentaires, dans le cadre de la promotion 2023-2024.

Ce livret est toujours en cours de transformation.
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/5c22e981-ee09-47a3-9ce4-7c09eecfa7c4.jpg)

**Sommaire** (temporaire):

*Sciences et société* *: de la pluralité des éthiques*
- De la rationalité du monde : les revers du progrès scientifique
- Cure versus Care : aux origines du soin
- Les éthiques du care : sources et controverses féministes

*L'approche par le design* : 
- Métamorphoses du design : quelques sources d’inspiration
- Design et régulation démocratique
- Le design à l’epreuve de l’éthique du care
- P.O.C. : Du Proof of Concept au Proof of Care


*Pour une clinique institutionnelle*

- Aux sources de l'approche institutionnelle
- Fondements des risques psycho-sociaux
- Faire "clinique institutionnelle"

Moyens
- L'enquête : l'enrichissement du design par les pratiques en sciences humaines et sociales
    - La place des émotions 
- Faire récits pour (se) soigner
- La médiation par les arts et l'art thérapie

*Une approche globale du soin au vivant et non vivant* : 
- De nos vulnérabilités au monde
- Vers des "vulnérabilités capacitaires"

*L'élan de Cynthia Fleury et Antoine Fenoglio*
- Le séminaire *Design With Care* du CNAM
- La Charte du Verstohlen : Ce qui ne peut être volé

*Extensions personnelles*

- Emotions et subjectivité
- Vers une clinique insitutionnelle des centres sociaux ?
- Bibliographie extensive : les ouvrages que j’aimerais avoir entre mes mains
- Glossaire du Vertsholen

**Date d'impression** : 