# Design with care : bibliographie extensive
Les ouvrages que j'aimerais avoir entre les mains...


![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/595f0adc-0f21-40e6-b9b3-932f603d8ff9.jpg)
[Images de pensée](https://www.boutiquesdemusees.fr/fr/livres-d-art/images-de-pensee/2262.html), Marie-Haude Caraes, Nicoles Marchand-Zanartu

[A l'Est des Rêves](https://www.editionsladecouverte.fr/a_l_est_des_reves-9782359251951), Nastassja Martin

[Rendre le monde indisponible](https://www.editionsladecouverte.fr/rendre_le_monde_indisponible-9782348045882), Hartmut Rosa

[Abondance et liberté](https://www.editionsladecouverte.fr/abondance_et_liberte-9782348058387), Pierre Charbonnier

[La vie des plantes, métaphysique du mélange](https://www.payot-rivages.fr/rivages/livre/la-vie-des-plantes-9782743638009), Emanuele Coccia

[L'invention du quotidien, Arts de faire](https://www.gallimard.fr/Catalogue/GALLIMARD/Folio/Folio-essais/L-invention-du-quotidien#), Michel de Certeau

[Terra Forma](https://www.ehess.fr/fr/ouvrage/terra-forma), Frédérique Ait-Touati, Alexandra Arènes et Axelle Grégoire

[Faire - anthropologie, archeologie, art et architecture](https://www.yvon-lambert.com/fr/products/tim-ingold-faire) Tim Ingold

[Cartes et lignes d'erre](https://www.fabula.org/actualites/56891/cartes-et-lignes-d-erre-traces-du-reseau-de-fernand-deligny-1969-1979.html), Fernand Deligny 
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/bab8c4c3-2422-4f29-adcb-8d3d446aebb4.jpg)



