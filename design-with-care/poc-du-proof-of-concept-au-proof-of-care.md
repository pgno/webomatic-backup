# POC : Du Proof Of Concept au Proof of Care

## Le design peut-il aider à mieux soigner ? Le concept de Proof Of Care
https://chaire-philo.fr/wp-content/uploads/2019/04/Soin_1513-Fleury_ok.pdf

* Prise en compte de la générativité du vulnérable
* Remplacer la "gestion" par le "prendre soin"
* Traiter les situations les plus critiques pour penser un système
* Allier le faire et le penser, les effets et l'*agency* des parties prenantes

- [ ] Thèse de Caroline Jobin : La preuve de concept comme outil de développement des capacités de générativité collective : modélisation,expérimentation et conditions de performance
https://pastel.hal.science/tel-03738429

