# Aux sources de l'approche intitutionnelle

Cours du Cynthia Fleury, CNAM, 11 décembre 2023

Enjeu : Comment libérer le potientiel **capacitaire** ou **soignant** d'une organisation ?
Comment un milieu développe sa capacité phorique (néologisme de Cynthia Fleury) : comment il contient, maintient, entretient, soutient...?

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/881562ca-b190-48bd-a776-eeebebc2308c.jpeg)

## Qu'est-ce qu'une institution qui prend soin ?

**Pathoplastie** [Viktor Frankl](https://www.thyma.fr/a-propos-de-viktor-frankl-1905-1997-principes-et-perspectives-de-lanalyse-existentielle-et-de-la-logotherapie/)

**Ambiance** Jean Oury [Ambiance de J.Oury](https://www.cairn.info/formes-de-transfert-et-schizophrenie--9782749242422-page-193.htm)

**Ecophénoménalité** Bruce Bégout [Le concept d'ambiance](https://www.seuil.com/ouvrage/le-concept-d-ambiance-bruce-begout/9782021432671)

**L'institution totale**  Lire *Asiles* de Goffman, ouvrage écrit du point de vue des internés, à partir d'un travail d'éthnographie non participante

Les conséquences et symptomes de l'institution totale : 

Enjeu : Produire des organisations capables de réflexivité, capables de réfléchir sur elles-mêmes. Dédider des temps de méta-cognition.

**Psychothérapie intitutionnelle** Voir [L'arrière-pays, Aux sources de la psychothérapie institutionnelle](https://www.cairn.info/l-arriere-pays--9782749267159.htm) sous la direction de P. Faugeras, R. Gentis, J. Oury

**Psychiatrie phénoménologique** Daseinanalyse 

**De la Pop Philosophie de Deleuze à la Pop Psychiatrie**


![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/df695d9d-d077-44e3-982b-d881d0231f1b.jpeg)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/e52b0b57-d66e-46ce-835d-af9f362de78d.jpeg)

## L'antipsychiatrie

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/11dd946d-1f7c-494b-bde0-d9f0bdb31281.jpeg)

## Le cas et l'apport de Mary Barnes

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/ec0c2098-a9e5-4963-8418-911e0e2bb556.jpeg)

Apport : une fiction régulatrice, comme une invitation à faire autrement
