# Le pouvoir psychiatrique chez Michel Foucault

Source : **Le pouvoir psychiatrique - Cours au Collège de France 1973-74**
([Lien vidéo](https://video.ploud.fr/w/d24441da-6a14-4f6b-8cc5-06c41c4b8914) ou [source de téléchargement](https://freefoucault.eth.link/))

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/8078ace3-e0d0-41c4-99f5-2af80cb83cee.jpeg)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/1b605db7-0d9f-4b18-ba55-07e5543ea063.jpeg)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/9309d730-336d-4ab8-9fed-580cb1588c02.jpeg)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/aa5c9d31-029b-4eff-8abf-6ebb9b847cb8.jpeg)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/62129b72-4ee9-46ba-b365-838b90f25905.jpeg)

Lire [**L'incorporation de l'hôpital dans la technologie moderne**](https://www.cairn.info/revue-hermes-la-revue-1988-2-page-30.htm?contenu=resume) : 

> L'hôpital en tant qu'instrument thérapeutique est un concept relativement moderne puisqu'il date de la fin du XVIIIe siècle. Avant : l'hôpital était essentiellement une institution d'assistance aux pauvres. Il était en même temps une institution de séparation et d'exclusion.

> L'hôpital agit sur les maladies, il peut parfois les aggraver, les multiplier ou au contaire les atténuer.
