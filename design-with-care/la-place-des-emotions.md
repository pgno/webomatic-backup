# La place des émotions

Vinciane Despret
Les émotions, un concept politisé et un facteur d’exclusion ...
https://youtu.be/X5iSohJJU5Q?si=twSaPegMa4ATROZr


>  « On fait donc plus que de manifester ses sentiments, on les manifeste aux autres, puisqu’il faut les leur manifester. On se les manifeste à soi en les exprimant aux autres et pour le compte des autres. C’est essentiellement une symbolique. »

Marcel Mauss