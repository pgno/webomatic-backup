# Le design à l'épreuve de l'éthique du care

4 étapes des éthiques du care appliquées au design

1/ **Caring about** : Se soucier de / Reconnaître ses vulnerabilités 

2/ **Taking care of** : Prendre en charge

3/ **Care giving** : Prendre soin

4/ **Care receiving** : Recevoir le soin

Voir [Le design à l’épreuve de l’éthique du care : retour réflexif pour un possible renouvellement des pratiques en design](https://www.cairn.info/revue-sciences-du-design-2022-2-page-120.htm) par Marine Royer, Denis Pellerin.
