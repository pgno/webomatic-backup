# Design et démocratie

## Naissance du design et régulation démocratique

Cours d'Antoine Fenoglio, mardi 12 décembre 2023

Les grandes dates de naissance du design sous l'angle de la régulation démocratique : 
- "rationnelle" ? / Descartes
- "contrôlée" ? / Bentham Le Panoptique
- "industrielle" ? / Crystal Palace
- "A dessein" ? / W. Morris Arts & Craft


![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/7322d867-357e-42a5-8103-7987a848aade.jpeg)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/02954ccb-7fe8-498f-8448-479f41ac6518.jpeg)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/fc309826-5e23-4db1-99c8-056eb922bf7a.jpeg)
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/4bd7086f-b9f1-4e43-a585-1f476fe1879d.jpeg)

Lire [Gilles Deleuze : Post-scriptum sur les sociétés de contrôle](https://www.cairn.info/revue-ecorev-2018-1-page-5.htm?contenu=resume) 1990

## Du "mot d'ordre" au "mot de passe"

Métaphore animalière du serpent et de la taupe

Dans les sociétés disciplinaires, c'est l'individu, le lieu qui est le marqueur; dans les sociétés de contôle, c'est le chiffre.

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/d2afba18-df9c-46db-9491-bb3e6399637f.jpeg)


## Un modèle actuel limité

Deleuze : "il n'y a pas lieu de craindre ou d'espérer mais de chercher de nouvelles armes"

Pour participer à la régulation de ce système, le design fait face à plusieurs enjeux : 
- réparer ce qui s'endomage (l'avant)
- transiter d'un système à un autre (le maintenant)
- créer de la régulation dans un nouveau système (l'après)
Les risques aujourd'hui sont connus : 
- sentiment d'impuissance, individuelle et collective
- épuisement dû à l'innovation intensive, à l'injonction du mouvement permanent
- incapacité à intégrer, *en plus*, la problématique climatique

## Vers de nouvelles régulations

Changer de mode de réprésentation, de méthode, d'imaginaire
Favoriser la fiction, les milieux naturels, l'expérimentation immédiate

Rana Hamadeh
http://pratiquesdhospitalite.com/workshops/rana-hamadeh/

Elise Morin Plante alerte
https://resonances.ladn.eu/entretiens/repenser-radicalement-notre-relation-au-monde/

Gilles Clément
Permaculture

## Passer du "double diamant" au "double Damasio"...

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/f6e2cdda-3e50-4e37-a45b-2927e0784bcb.jpeg)


> "Je cherche moins une expérience qu'une transformation d'énergie (...), qu'on en ressorte plus vivant, tout simplement."

## Vers la furtivité

Le Furtif (le furtivement/Verstohlen) est le mode qui nous permet de passer sous les radars
- hors société de contrôle
- hors mesure, pour la prochaine mesure, mesure des humanités
- pour le futur émergent, le furtif fera apparaître, phénoménologie en soi mais aussi en produisant un certain type d'évènement qu'on n'a pas vu venir

Le furtif est sans doute la réaction, disons la réponse de stabilité dans ce monde instable, liquide, impropre au durable : 
- Nous avons été contraints au furtif mais cela s'est transformé en potentialité, liberté, *pneuma*
- le furtif est aussi ce qui résiste à la propriété, l'appropriation par *commons* est possible mais pas la possession. Peu d'externalités négatives sur l'environnement.
- mode plastique et existentiel : *vitae furtiva*

Mais éviter une furtivité uniquement pensée pour "passer sous les radars", pour "échapper"

- expériences d'invisibilisation de la reconnaissance faciale, de l'IA

La liberté de se camoufler. Toujours liée à ce à quoi on cherche à échapper. Se camoufler dans la nature / dans la techno



**Les enseignements de Damasio** : 



Furtivité n'est pas fuite. Trouver des situations dans lesquelles on peut rester.

Disponibilité > Injonction / Indisponibilité > Impossibilité

Voir [Rendre le monde indisponible](https://www.editionsladecouverte.fr/rendre_le_monde_indisponible-9782348045882) d'Hartmut Rosa

- [ ] Voir Edgar Morin Passer du développement à l'enveloppement https://www.cairn.info/edgar-morin-les-cent-premieres-annees--9791037029317-page-121.htm

## De la nécessité d'être radical & paradigmatique

Voir Disbedient Object, 2014 Victoria and Albert Museum, Londres

Voir Fabienne Brugère, commissaire associée de l'exposition Les Usages du Monde http://hbaat.fr/usages-du-monde/

Victor Papanek

Eloge des dysfonctionnements avérés
Nicolas Nova

L'activisme comme mode critique et constructif

## Assumer ce qui est, les communs négatifs

Hot spots assumés
Sabu Kosho

### Subir les communs négatifs
Cours du Séminaire Design With Care avec Alexandre Monnin
https://chaire-philo.fr/heriter-et-prendre-soin-dun-monde-en-train-de-se-defaire/

### Reconnaitre, rendre visible, transformer
Studio Swine / Sea Chair Project

### Retrouver des liens entre techniques défaillantes et esthétiques ?

Emile de Visscher

Technophanie de Simondon / hierophanie d'Eliade (de libérer de la magie/aller vers la culture)

Manufacture, a Labour of Love, Capitale Mondiale du Design
https://www.reseau-canope.fr/fileadmin/user_upload/Academies-ateliers/DT_Nord_Pas-de-Calais_Picardie_NPCP/Manufacture.pdf

https://www.youtube.com/watch?v=3RvxRd0jFcc

Faire du beau avec du laid

## Sobriété nécessaire

La quête de ce qui nous manque
Markus Kayser / Solar Sinter

Faire avec ce que nous avons
Navi Radjou / Jugaad

Sobriété et liberté

Lire [Abondance et liberté, Pierre Charbonnier](https://www.editionsladecouverte.fr/abondance_et_liberte-9782348058387) 2020

2013 Naissance du Low Tech Lab
2014 L'âge des Low Tech, Bihouix

Se relier aux cycles communs des milieux naturels
Katarina Mischer & Thomas Traver / The idea of a tree

## le design face à la capacité de s'hybrider

Emanuele Coccia, La vie des plantes, méthaphysique du mélange : 

> Un instinct, un désir, une force, qui me pousse à m'identifier à n'importe quelle chose, et à ne jamais être sûr qu'il ne s'agisse pas d'un jumeau qui a temporairement perdu son apparence

## vers de nouvelles manière de représenter

**[Terra Forma](https://www.ehess.fr/fr/ouvrage/terra-forma)**
Représenter : 
- par les profondeurs
- par les mouvements
- par les points de vie
- par les périphéries
- par les pouls
- par les creux
- par les disparitions et les ruines