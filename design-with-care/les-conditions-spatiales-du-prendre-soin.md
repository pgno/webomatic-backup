# Les conditions spatiales du "prendre soin"

**Architecture, urbanisme et *care***

Philosophie de l'architecture

- [ ] Thèse en cours de [Marie Tesson](https://foap.cnam.fr/le-laboratoire/membres/marie-tesson--1404955.kjsp?RH=1304502835059#/) : "L’architecture comme pratique de care : figures, références, conditions d’émergence"

- [ ] [Prendre soin de l'avenir. 4 pistes pour une architecture du care](https://cnam.hal.science/hal-04041718), Marie Tesson
- [ ] [L’architecture comme expérience, habiter autrement avec la philosophie de John Dewey](https://chaire-philo.fr/wp-content/uploads/2023/10/article-soins-hugo-martin.pdf), Hugo Martin
- [ ] [« Quand le moindre lieu est de soin » : architecture et vulnérabilité](https://chaire-philo.fr/wp-content/uploads/2022/11/soin1948.pdf), Eric de Thoisy
- [ ] [La Preuve par 7](https://lapreuvepar7.fr/) de Patrick Bouchain : 

La Preuve par 7 est une démarche expérimentale d’urbanisme, d’architecture et de paysage. Elle accompagne et documente des projets à travers différentes échelles territoriales afin de promouvoir un droit à l’expérimentation dans l’aménagement et l’architecture ainsi que des méthodes à même de renouveler l’action territoriale, publique et citoyenne.

- [ ] [La Laboratoire des Délaissés](https://chaire-philo.fr/le-laboratoire-des-delaisses-programme-de-recherche-de-la-preuve-par-7/)
-  [ ] [L'Ecole du Terrain](https://lecoleduterrain.fr/)
- [ ] Voir notamment les projets de [Lacaton & Vassal](https://www.lacatonvassal.com/)