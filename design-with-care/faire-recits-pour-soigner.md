# Faire récits pour (se) soigner

## Paul Ricoeur

"Le sujet, c'est celui qui peut faire récit" de ses origines, de ses expériences
La biographisation, l'autobiographisation = moment clé de récupération du pouvoir. Le pouvoir, c'est d'abord pouvoir dire. Le récit est une manière de faire lien de toutes les expériences qui ont été les nôtres

([Notion d’identité narrative : Colloque Du modèle du récit à l'énonciation de soi](https://www.fabula.org/colloques/document1883.php))

## Le récit dans l'épreuve de l'accident

Voir [Pretium doloris de Cynthia Fleury](https://www.fayard.fr/livre/pretium-doloris-9782818504857/), un article sur l'épreuve de l'accident = "traverser" au sens de "en faire quelque chose".
3 étapes : 
- "le corps propre" : conscientisation de l'accident par le corps, se réapproprier son corps
- "l'histoire propre" : en faire son histoire propre, comprendre pourquoi ça m'est arrivé
- "le style propre" : phase d'invention d'une nouvelle manière d'être, d'un nouvel éthos.
Pour ne pas être broyé par l'accident, enjeu d'en faire un lieu de régénérescence.

Voir le séminaire [La Beauté du Vulnérable](https://chaire-philo.fr/calendrier/seminaire-la-beaute-du-vulnerable/)

## Clinique philosophie du burn out des soignants : le recours à l'éthique narrative

Voir la publication de la Chaire [L'éthique narrative](https://chaire-philo.fr/wp-content/uploads/2022/08/Le%CC%81thique-narrative-Vale%CC%81rie-Gateau.pdf) de Valérie Gateau

Voir la publication "Savoirs expérientiels" de la Chaire "[Quand le récit fait soin : Paroles et récits de soignants](https://chaire-philo.fr/wp-content/uploads/2022/05/CliniqueBurnOut_Chaire-Philo_WEB.pdf)"

"Clinique" au sens foucaldien du terme

Méthodo des ateliers animés par Valérie Gateau auprès des soignants (exclusivement) : 
- d'abord une "mini-cours" de philosophie
- puis des exercices d'écriture spécifiques de verbalisation et de décentrement. Ex : "vous êtes un lit d'hôpital, racontez"
- un livrable est toujours produit a posteriori

Effets observés et formulés par les participants : 
- effet catarthique 
- restauration de l'éthos du soignant
- reprise de puissance d'advocacy = c'est une praxis de résistance
- reflexivité sur les pratiques
- renforcement narcissique

Projet de colloque sur les travaux de **verbalisation philosophiques et littéraires**, qui partira d'une cartographie de toutes les personnes qui occupent des métiers liés à la philosophie, aux lettres et aux sciences humaines à l'hôpital.

Nouveaux métiers à l'hôpital : 
- Attaché littéraire hospitalier (ALHO) à Créteil ([Christian Delorenzo](https://www.linkedin.com/in/christian-delorenzo-16b42a40/))
- Philosophe salarié à l'hôpital de Charleroi ([Jérôme Bouvy](https://www.linkedin.com/in/j%C3%A9r%C3%B4me-bouvy-ab704a208))

## L'expérience des banques de récits de migrants avec Barbara Cassin

[La maison de la sagesse de Barbara Cassin](https://lejournal.cnrs.fr/articles/les-maisons-de-la-sagesse-philosophie-traduction-et-microcredit)

Elle repense l'écriture des glossaires bilingues

Hybridation musée et banque, patrimoine et

## La médecine narrative

### Médecine et récit, une longue tradition

La médecine et le récit ont toujours été liés ensemble

Le récit était ce qu'il y avait avant la technique.
Disqualification du récit = absence de technique. Mais il y a toujours besoin d'écoute.

### Le langage : résistance et restauration

Biographisation de la maladie pour la formalisation d'un savoir expérientiel

Voir Jean-Luc Nancy L'intrus

Ethique narrative : praxis de résistance des collectifs des patients, mais aussi des soignants
Outil de démocratie sanitaire

Pratique contre la désubstancialisation du langage (contre attaque à la novlangue qui crée un décalage, voire une contradiction entre le langage et la réalité)

Fonction de méta-stabilité (ex : journaux de confinement)

### La maladie dans la littérature et les SHS

Maladie comme métaphorisation d'une culture, d'une société

Retour de l'éthique narrative au pic de l'épidémie de sida

Philosophes français récents :
Fabrice Gzil
Emmanuel Hirsch
Jean Luc Nancy
Claire Marin
Ruwen Ogien
Bertrand Quentin
Pierre Zaoui

### Mobiliser les fonctions psychiques de la narration par l'écriture

- Fonction de catharsis
- Fonction de partage
- Fonction de liaison
- Fonction d'historisation

Zoom sur les journaux de confinement



Lecture et analyse de l'article : 
[La relation patient-proches-soignants en réanimation : des pratiques discursives « hors cadre » vers une éthique narrative dans la relation de soin](https://www-cairn-info.proxybib-pp.cnam.fr/revue-langage-et-societe-2020-1-page-31.htm)
par Alexandre Herbland
Dans Langage et société 2020/1 (N° 169), pages 31 à 56

### Tiers lieux en santé

[EHPAD et tiers-lieux](https://www.cnsa.fr/outils-methodes-et-territoires/un-tiers-lieu-en-etablissement/des-tiers-lieux-en-action)

https://www.cnsa.fr/documentation/fiches_signaletiques_des_projets_de_tiers_lieux_en_ehpad-compresse.pdf

[3BISF, un lieu d'asile pour la création contemporaine à la croisée des arts et des formes](https://www.3bisf.com/) : A Montperrin. L'art intégré à un hôpital psychiatrique


