# L'enquête : l'enrichissement du design par les méthodes des sciences humaines et sociales

- [ ] Tim Ingold 
**Faire. Anthropologie, archéologie, art et architecture**
https://journals.openedition.org/teth/3112

> Voici un livre paru aux belles éditions Dehors, un livre de Tim Ingold, grand professeur d’anthropologie contemporaine en Ecosse.
> C’est un livre paru aux belles éditions Dehors, un livre de Tim Ingold, grand professeur d’anthropologie contemporaine en Ecosse. Et quand on lit les premières pages de ce livre, on peut être au moins deux fois étonnés : d’abord, parce que son auteur nous encourage à ne pas lire son livre ! « N’essayez pas de le lire, nous dit-il, car il ne vous dira rien sur ce que vous avez besoin de savoir »… ; et puis, parce que son titre, « Faire. Anthropologie, archéologie, art et architecture», contraste aussi avec les 1ers propos de Tim Ingold qui portent sur l’apprentissage, sur la manière dont il a appris l’anthropologie et dont il tente de l’enseigner. On semble donc loin du « faire » annoncé sur la couverture…
> Mais après tout, pourquoi pas ? Car ces deux bizarreries nous conduisent en fait à cette question : qu’est-ce qu’un livre pourrait donc nous apprendre à faire ? Certes, on pourrait parler des manuels en tout genre ou des livres de cuisine, mais que faire de livres qui ne doivent que se lire, que faire d’un Bourdieu ou d’un Platon dans notre cuisine en revanche ? Si les livres nous apprennent donc tout un ensemble de choses, nous apprennent-ils pour autant à faire quelque chose ? De la même manière, peut-on dire que l’on apprend à faire de l’anthropologie ou de la philosophie, et peut-on dire, au contraire, que l’on pense l’architecture ou l’archéologie ?

Source : https://www.radiofrance.fr/franceculture/podcasts/deux-minutes-papillon/faire-anthropologie-archeologie-art-et-architecture-par-tim-ingold-7321316

- [ ] Vinciane Despret
