# Soin et Nature

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/fbd8c0c8-0419-4b6c-8537-cc2e1aa45a0e.jpeg)

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/5e70fa0c-706b-4e38-89dd-80dc6e77d163.jpeg)

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/b2b9f180-2b19-42db-bcd2-c77bae8c2196.jpeg)

- [ ] Voir [The Barn Garden](https://www.suestuartsmith.com/the-barn-garden)


> Garden and Growth
> 
> The garden represents a kind of timeline in our lives.
> It has grown and changed alongside the growing up of children and family.
> It started out with stone picking the fields,
> hand sowing the meadow and planting embryonic hedges and trees.
> 
> In the beginning it was an idea of a garden and over time it has formed a place
> with a unique sense of being, like a baby becoming a person.
> While you look away from it, it silently gets on with growing
> and then surprises you when on looking back you see how far it has come on.
> The trees that were once saplings are suddenly strong enough to hold a hammock
> and create a place within a place to retreat to.
> 
> The garden is peopled by memories as well as by plants:
> The water fights, the Easter egg hunts,
> the long summer lunches, the games of partridges.
> Layers of memory are grafted onto the place in our minds.
>  
> Just as memories are a resource, so the garden is a resource.
> It is a repository of beauty – something to turn to when tired and empty.
> It gives back much more than it takes. 

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/9d076801-da42-4f66-a228-88e23d2de829.png)

- [ ] Les apports du jardin thérapeutique avec le principe de viriditas

## Psychanalyse et Nature

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/9081b129-062e-496b-84c8-21a9f95fff32.jpeg)

Lien à la nature, à l'océan, à l'inaugural terrestre

"la tendance regressive thalassale"

## Poésie et Nature



Symbolisation et poétique matérielle (eau, terre, air, eau, feu)
Production de notre capacité symbolisante
Gaston Bachelard
https://www.cairn.info/magazine-sciences-humaines-2012-11-page-33.html


L'imaginaire de la forêt


## Philosophie et Nature : anthropocène à rebours

L'hypothèse de Gaïa de Lovelock

## Philosophie et Nature : quel Humanisme ?
![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/4f39200f-0129-4062-ab02-1ab9cddcdbd0.jpeg)

Binswenger : la santé mentale est indispensable d'un espace timique, dans le sens où je peux faire corps, synthèse avec cet espace, un espace qui me régule. On se trimballe avec notre aptitude à créer un espace timique.

![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/875eca80-eb4f-4c6d-af1e-a4434a6542c6.jpeg)

## Le Jardin en Mouvement de Gilles Clément

Notion de [tiers-paysage](https://www.editionsducommun.org/cdn/shop/files/Manifeste_du_Tiers_paysage_-_Presentation_Alexis_Pernet.pdf?v=9791044723152443009)
espaces de friches, délaissés par l'activité humaine, abandonnés, presque marginalisés : inversion du stigmate. Différent du rewild. Un espèce d'entre-deux. Lieu de reconcialiation, de reconnexion. Dimension orphique (de Orphée), qui vient en anti-thèse de la figure prométhéenne (techniciste, postiviste).

Pierre Hadot, spécialiste de la philosophie grecque, le plus grand spécialiste de Plotin, spécialiste des stoïciens, Marc-Aurele. 
Lire : [Les voiles d'Isis, Essai sur l’histoire de l’idée de nature](https://www.librairie-gallimard.com/livre/9782070356546-le-voile-d-isis-essai-sur-l-histoire-de-l-idee-de-nature-pierre-hadot/). Vous avez des formes de connaissance qui ne sont pas des connaissances qui dévoilent. Possibilité de travailler avec les voiles.

Dispositif de médiation scientifique avec un jardin à Chaumont sur Loire, qui parle d'adaptation au changement climatique

A Chaumont en 2023, il y avait un [jardin sec](https://domaine-chaumont.fr/fr/festival-international-des-jardins/edition-2023-jardin-resilient/le-jardin-sec).

