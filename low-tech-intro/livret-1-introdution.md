---
title: Numérique éthique, en route vers les low-tech
---

# Numérique éthique, en route vers les low-tech !

## Avant de démarrer

La formation que vous allez suivre fait partie d’une expérimentation pédagogique qui vise à développer de nouveaux formats d’apprentissage. À savoir : des formats hybrides pour plus d’autonomie, un rapport moins descendant dans la transmission des connaissances, des méthodes d’évaluation qui favorise une meilleure appropriation des contenus.

La thématique de cette formation s’y prête particulièrement mais elle demandera de votre part un engagement dans la participation aux activités différent de ce que vous avez peut-être pu connaître jusque là. Emancipateur ou déroutant, ce format à pour vocation d’impulser une dynamique tant individuelle que collective au sein de votre structure.

Vous trouverez dans ce livret la séquence d’introduction de cette formation ainsi que le travail préparatoire au premier module.

Nous espérons que vous prendrez autant de plaisir à la suivre que nous en avons eu à la concevoir. Bonne route !

> [](https://www.resiliencefactory.fr/assets/pdf/fresque-low-tech.pdf#qrcode)

[![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/6c8dfabe-00b6-4cd7-a399-4bddec7564ae.jpg)](https://www.resiliencefactory.fr/assets/pdf/fresque-low-tech.pdf)

_Vulgarisation des low-tech, financée par le CNRS, réalisée par Vito en collaboration avec Guillaume Guimbretiere et Romane Quintin. PDF avec le détail expliqué accessible en cliquant sur l'image_

###### 


# Introduction

Aborder le thème du numérique éthique à travers le prisme des low-tech, c’est faire le choix de mettre en avant le pouvoir d’agir face à un ensemble de problématiques complexes auxquels chacun de vous doit faire face au quotidien dans la pratique du numérique en Centre Social : enjeux techniques, économiques, sociaux, écologiques… Si certains peuvent être réticent à aborder un nouveau terme “tendance” pour s’attaquer à ces thématiques récurrentes, nous vous invitons à voir ce parti-pris comme une volonté de s’inscrire dans un mouvement qui va au delà des enjeux propres aux centres sociaux et ainsi de développer un regard nouveaux sur ceux-ci. Au final, qu’importe la terminologie, pourvu qu’elle permette à chacun de se réapproprier sa manière d’accompagner la transition des centres et de leurs publics !

Avant de détailler le déroulé de cette formation, nous allons accorder un peu de temps à définir ce que recouvre les low-tech aujourd’hui et à identifier les liens qui les unissent aux grands thèmes du numérique éthique.

## La low-tech en quelques mots

La low-tech, ou technologie basse, fait référence à des technologies simples, pratiques, économiques et écologiques. Elles sont généralement faciles à comprendre, à utiliser, et à réparer, ce qui les rend accessibles à un large public. Le concept de low-tech va souvent de paire avec des principes de durabilité et de réduction de l’impact environnemental. Sur certains aspects, il s’oppose aux high-tech, qui sont généralement des technologies de pointe, souvent complexes et coûteuses.

:::success
**La low-tech cherche à proposer des solutions adaptées aux besoins réels des utilisateurs tout en minimisant l’utilisation de ressources et la production de déchets.**
:::

###### 

## Pourquoi s’intéresser aux low-tech ?

La démarche Centres Sociaux Connectés accompagne la transformation du centre social avec le numérique et concourt à la montée en compétences des équipes, notamment salariées, dans le champ numérique entendu au sens large : éducation aux médias, culture numérique, pratiques collaboratives…

> La low-tech comme démarche pour aller vers un numérique plus éthique et responsable !

De plus en plus, les préoccupations écologiques et démocratiques obligent à penser un numérique le plus éthique et responsable possible. De manière plus large, les articulations entre transition numérique et transition écologique nécessitent un travail d’explication et d’illustration, notamment pour dépasser l’apparence contradictoire entre ces deux mouvements et pour intégrer la question démocratique comme pivot de ces transformations.

:::success
**Dans cette optique, la low-tech permet d’appliquer un regard particulier sur les pratiques numériques qui fait converger l’ensemble des préoccupations écologiques et démocratiques.**
:::

###### 

# La définition du Low-tech Lab

Le low-tech Lab est un acteur historique de ce mouvement. Voici quelques éléments issus de leur présentation sur leur site internet.

Son histoire commence au Bangladesh en 2010. À bord d’un navire prototype en fibre naturelle de jute, Corentin de Chatelperron entreprend une expédition pour s’essayer à l’autonomie grâce à de petits systèmes low-tech. Au fil de l’eau, il constate l’ingéniosité déployée de par le monde pour répondre de façon simple, accessible et durable aux besoins de tout un chacun… L’intérêt pour les low-tech est né. En 2014, le Low-tech Lab est lancé avec la création d’une plateforme de documentation collaborative et le départ de l’expédition Nomade des Mers : un tour du monde à la découverte de l’innovation low-tech.

> « Nous nous sommes donnés pour mission de partager les solutions et l’esprit low-tech avec le plus grand nombre, afin de donner à chacun l’envie et les moyens de vivre mieux avec moins »
> 

Au Low-tech Lab, le terme low-tech est utilisé pour qualifier des objets, des systèmes, des techniques, des services, des savoir-faire, des pratiques, des modes de vie et même des courants de pensée, qui intègrent la technologie selon trois grands principes.

## Utile

Une low-tech répond à des besoins essentiels à l’individu ou au collectif. Elle contribue à rendre possible des modes de vie, de production et de consommation sains et pertinents pour tous dans des domaines aussi variés que l’énergie, l’alimentation, l’eau, la gestion des déchets, les matériaux, l’habitat, les transports, l’hygiène ou encore la santé. En incitant à revenir à l’essentiel, elle redonne du sens à l’action.

## Accessible

La low-tech doit être appropriable par le plus grand nombre. Elle doit donc pouvoir être fabriquée et/ou réparée localement, ses principes de fonctionnement doivent pouvoir être appréhendés simplement et son coût adapté à une large part de la population. Elle favorise ainsi une plus grande autonomie des populations à tous les niveaux, ainsi qu’une meilleure répartition de la valeur ou du travail.

###### 

## Durable

Éco-conçue, résiliente, robuste, réparable, recyclable, agile, fonctionnelle : la low-tech invite à réfléchir et optimiser les impacts tant écologiques que sociaux ou sociétaux liés au recours à la technique et ce, à toutes les étapes de son cycle de vie (de la conception, production, usage, fin de vie), même si cela implique parfois, de recourir à moins de technique, et plus de partage ou de collaboration !

> [](https://lowtechlab.org/fr#qrcode)

[![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/4b089428-8e37-45f5-9bff-a937071e4607.png#full)](https://lowtechlab.org/fr)
_Page d'accueil du Low-tech Lab_


######
[![](https://hot-objects.liiib.re/pad-lescommuns-org/uploads/54c66607-7168-40a8-887d-aa3c4d166076.png#fullpage)](https://fr.wikipedia.org/wiki/Low-tech)


######

## Low-tech et numérique éthique : quels liens ?

Le lien entre la low-tech et le numérique éthique réside dans leur objectif commun de promouvoir une approche plus responsable et durable de la technologie. Voici quelques points de convergence entre ces deux concepts :

> En somme, bien que la low-tech et le numérique éthique puissent se concentrer sur différents aspects de la technologie, ils partagent une vision commune d’une utilisation plus consciente, équitable et durable de la technologie dans notre société.

1. **Durabilité et réduction de l’impact environnemental** : La low-tech vise à utiliser moins de ressources et à produire moins de déchets, tandis que le numérique éthique se concentre sur la réduction de l’empreinte écologique du secteur numérique, par exemple en encourageant l’utilisation d’énergies renouvelables et la conception de produits plus durables.

2. **Accessibilité et Inclusion** : Les deux approches cherchent à rendre la technologie accessible à tous. La low-tech, par sa simplicité et son coût abordable, permet une utilisation plus large. Le numérique éthique, de son côté, se soucie de l’accessibilité numérique, veillant à ce que les produits et services numériques soient utilisables par des personnes ayant différentes capacités.

3. **Éthique et Transparence** : Le numérique éthique met l’accent sur le respect de la vie privée, la sécurité des données et la transparence. La low-tech, en favorisant des technologies simples et compréhensibles, permet également une plus grande transparence et compréhension de la part des utilisateurs.

4. **Autonomie des Utilisateurs** : La low-tech encourage l’autonomie des utilisateurs en proposant des technologies faciles à réparer et à maintenir, ce qui rejoint l’idée du numérique éthique de donner aux individus un contrôle plus important sur leur environnement numérique.

5. **Réflexion sur la Consommation Technologique** : Tant la low-tech que le numérique éthique incitent à une réflexion critique sur la nécessité et l’impact de la consommation technologique, favorisant des choix plus conscients et mesurés.

###### 

# La formation

Après ces quelques éléments de définition, nous allons maintenant détailler le déroulé de la formation et les différents travaux que vous devrez réaliser.

## Définir son environnement personnel d’apprentissage

Avant toute chose, nous vous demandons de prendre 30mn pour définir votre environnement personnel de formation. Il s’agit des points d’attention que vous pouvez définir avant de vous engager dans le programme. Ces éléments pourront évoluer durant la formation, mais il est important à ce stade de fixer un cadre à votre pratique. Pour se faire répondez aux questions ci-dessous. Prenez-en note sur un document (papier ou numérique), sous la forme d’une carte mentale ou tout autre outil avec lequel vous êtes à l’aise. Vous devrez pouvoir consulter ce document à tout moment durant la formation.

### Mes attentes et objectifs

**Ce qui me motive à participer :**
:::info
-
-
-
:::

**Ce que je redoute :**
:::info
-
-
-
:::

**Les changements que j’aimerai à la fin du cycle :**
:::info
-
-
-
:::
 

**Les possibilités que cela peut m’ouvrir :**
:::info
-
-
-
:::

### Mon réseau interne et externe

**Personnes ressources au sein de mon lieu de travail et à l’exterieur** 
_(Par exemple les personnes avec qui vous pouvez échanger sur un théme de la formation, celles à qui vous pouvez présenter vos travaux…)_
:::info
-
-
-
:::

**Lieux ressources**
_Les espaces physiques où vous pouvez trouver un cadre propice au travail, l’accès à du matériel ou de la documentation…_
:::info
-
-
-
:::

**Ressources documentaires**
_Les ressources dont vous disposez d’ors et déjà pour alimenter vos travaux : site internet, livres, plateforme…_
:::info
-
-
-
:::

###### 

## Le format

La formation vous est proposé sur un format hybrique, c’est à dire avec des séquences en présentiel et des séquences en distanciel.

Voici pour rappel la trame de la formation :

- Jour 1, présentiel
- Jour 2, 3 et 4 : Chacune de ces journées est découpée en 2 demi-journée de 4h correspondant à 6 modules différents. Chacune de ces 1/2 journées seront découpées en :
    - 2 heures de travail personnel en autonomie : lectures, exercices et travaux d’évaluation. Vous pouvez organiser ces 2 heures comme vous le souhaitez (voir plus bas pour les recommandations) en amont de la visioconférence
    - 2 heures de visioconférence encadrée à horaire fixe : clarification des apprentissages, réflexion/débat en groupe, présentation des travaux du module, lancement de la séquence suivante,…
- Jour 5, présentiel

### Les travaux en autonomie

Concernant les heures de travaux personnels, des consignes vous sont données dans chaque module. En cas de doute sur l'exercice ou de problème technique, référez-vous à l'annuaire un peu plus bas.

### La visioconférence

Chaque module se cloture par une session en visio-conférence de 2h à heure et jour fixe. Il est important d'être présent, à l'heure et dans de bonnes conditions de travail pour y participer (connexion, matériel, salle isolée...).
Le déroulé de cette session de visioconférence pourra être adaptée en fonction des besoins de chaque module et de l'avancée des travaux, mais la trame générale sera : 
- **Tour de table au démarrage - 10mn**  
Retour individuel sur les apprentissages, la thématique du module,...
- **Clarification des contenus pédagogiques - 20mn**  
On répond aux questions des apprenants sur des élements qui auraient pu poser problème ou des difficultés d'interprétation. Il est souhaitable si cela est possible de faire part de ses difficultés via les canaux de communications asynchrones en amont de la visioconférence. Ainsi les problèmes pouvant être réglés en amont le seront et si un complément est à préparer, il pourra être disponible dès le moment de la visioconférence.
- **Présentation des travaux - 45mn**  
Chaque module donne lieu à une production personnelle qui doit être remis en amont de la visioconférence. On étudiera 2 à 3 travaux parmi l'ensemble des participants afin d'en débattre collectivement. Ces "corrections" pourront donner lieu à l'écriture de résumés (terme de glossaire, fiche) collectifs durant la session. A différents stades de la formation, il sera aussi question de faire le point sur le choix du sujet et l'avancement de la production du portfolio individuel. En cas de difficulté personnelle, un temps individuel hors visioconférence collective peut être organisé pour accompagner un apprenant.
- **Eclairage, focus, démonstration - 15mn**  
Selon les modules, un court temps de présentation pourra avoir lieu autour d'une démonstration, d'un temps de question réponse avec un invité...
- **Préparation du travail de la semaine suivante : 20mn**  
Le livret pédagogique du module suivant sera mis à disposition. On en fera une courte présentation et vérifierons ensemble que les consignes des travaux personnels sont claires pour tous.

## Comment travailler ?

### Sécuriser ses temps de travail

En dehors des 2 journées (introductives et conclusives) qui vous sont proposées, le reste de le formation aura lieu en autonomie et en distanciel.

Il est essentiel que ces temps soit sanctuarisés durant votre temps de travail. Cela signifie qu’ils sont obligatoires au titre de votre participation à la formation et que vous ne pouvez être interrompu ou sollicité pour tout autre tâche durant ces temps de formation.

Pour cela nous vous invitons à :

- Planifier/bloquer à l’avance l’ensemble des temps de visioconférence dans votre planning de travail  
- Identifier un espace de travail qui vous permettre de travailler efficacement, que cela soit pendant les visioconférences ou pendant vos temps d’apprentissage. Vous devez y trouver un environnement propice et avoir la garantie qu’il est bien compris de tous que durant ces temps, vous êtes en formation (comme si il s’agissait d’un temps présentiel à l’exterieur). Il peut s’agir d’un espace au sein ou à l’extérieur de votre lieu de travail
- Préparer à l’avance le matériel et les outils dont vous pourriez avoir besoin pour réaliser les différents travaux pour profiter pleinement de ces temps sanctuarisés

L’équipe pédagogique est à votre disposition (voir contacts plus bas) pour vous aider à aménager ces temps de formation et à la mettre en place avec votre structure.

### Segmenter ou regrouper ?

Les 2 heures de travaux en autonomie de chaque module sont basés sur différentes modalités pédagogiques. Elles sont principalement répartis en 2 catégories : les apprentissages (lectures, visionnage, etc…) qui représente environ 30 à 45mn, et les travaux/évaluations le reste du temps.

Vous pouvez ainsi prévoir par exemple :

- un créneau fixe de 2h dans la semaine pour vous consacrer à l’ensemble
- des temps segmentés pour les apprentissages, et un temps dédiés aux travaux et évaluations.

Soyez vigilent à ne pas trop morceler votre travail pour que vous puissiez vous y investir pleinement.

## Les outils

Pour accéder aux contenus de la formation et réaliser les rendus, vous êtes libre d'utiliser les outils que vous souhaitez. Dans la plupart des cas, les contenus et supports vous seront fournis au format PDF, ce qui signifie que vous pouvez aussi les consulter sur votre mobile.

Pour les travaux réalisés collaborativement, les outils conseillés vous seront présentés au fur et à mesure si nécessaire.

Les "méthodes" et outils qui suivent vous sont proposés comme outil de référence mais vous êtes libre de ces choix.

### Outils de prise de note

- **Note manuscrite :** Pour certains travaux (ou en travail préparatoire), il est tout à fait possible de travail sur feuille et de partager une photo du document. Cela exclue toutefois les collaborations. 
- **Dessin :** Idem pour le dessin au crayon sous forme de schéma ou de "sketchnote"
- **Mind map, ou carte heuristique, cartes mentales...** : Les cartes mentales sont des outils très performant pour prendre des notes, surtout si l'on a besoin de hiérarchiser au fur et à mesure ses idées.
- **Pad** : Les pads sont de simples éditeurs de texte qui utilisent généralement le langage Markdown et sont collaboratifs. Une partie de nos travaux collectifs seront réalisés par ce moyen.

###### 

### Outils pour vos rendus

Pour rendre vos travaux, vous pouvez les envoyer directement au formateur via le canal dédié de Mattermost qui vous sera indiqué.

## L’évaluation

La formation repose sur 2 types d'évaluation : 

1. **Travaux personnels** : tout au long de la formation, il vous sera demandé de produire des travaux personnels courts (note d'étonnement, analyse, synthèse, interview, ...) basé sur la thématique de chaque module. Les consignes de chaque travail vous seront donnés le moment voulu.
L'évaluation de ses travaux se fera selon différentes modalités : auto-évaluation, évaluation par les pairs, évaluation extérieure. Ces évaluations ne donnent pas lieu à une notation. Elles ont pour but de vous aider à structurer et développer une vision de votre activité qui intègre les low-tech comme démarche globale vers un numérique éthique.

2. **Les portfolios** :  Les portfolios constituent le rendu final de la formation. Ils seront constitués de 2 parties. 
- La première partie sera constitué d'un travail personnel réalisé en continu tout au long de la formation. Vous devrez dès le troisième module choisir une thématique, puis un "angle" à traiter (par exemple, sur la thématique des déchêts, vous pouvez choisir l'angle du recyclage ou du zéro déchêts...). Autour de cette problématique vous réaliserez pas à pas un mini-dossier de 2 à 4 pages prenant la forme que vous souhaitez (texte, dessin, diaporama,infographie, récit, fiction, audio, vidéo...) mettant en lumière le sujet choisi. Vous serez guidé pas à pas lors des temps hebdomadaires en visioconférence dans sa réalisation.

- La deuxième partie sera réalisée collectivement au fur et à mesure par les exercices d'écriture collective et individuelle tirés de vos travaux personnels (glossaire du numérique éthique et de la low-tech, interview)

A l'issu de la formation, chacun aura donc produit un document de synthèse incluant une partie personnelle unique à chaque participant et une partie collective identique pour chaque dossier. Ces documents de synthèse donneront lieu à une présentation lors de la journée de clôture et feront l'objet d'un débat reflexif avec des invités exterieurs à la formation.

###### 

## L’évaluation de la formation

### A mi-parcours
A mi-parcours, l'équipe pédagogique vous proposera un point individuel sur votre sentiment autour de la formation, tant sur les apprentissages, vos objectifs personnels d'évolution et votre cadre de formation.

### Evaluation à chaud
Dans le cadre de l'expérimentation et de l'amélioration continue de la qualité de ce dispositif de formation, la journée de clôture sera l'occasion d'une évaluation guidée à chaud de la formation.

### Evaluation à froid
Dans un délai de 3 à 6 moins après la formation, l'équipe pédagogique reviendra vers vous pour évaluer l'impact de la formation sur votre pratique professionnelle et sur les améliorations que l'on pourrait y apporter.


## Annuaire de la formation

A tout moment durant le cycle de formation, vous pouvez prendre contact avec l'équipe pédagogique : 

### Pour les besoins individuels, personnels, administratifs... : 
- Contactez Nadège Cresson (@nadegefd) ou postez un message sur le canal "En route vers les low-tech" 
### Pour les questions pédagogiques liés à la formation
- Contactez Pierre Trendel (@pierre.trendel) ou postez un message sur le canal "En route vers les low-tech" 
### Pour les échanges libres
- Postez un message dans le canal "Hors-sujet"