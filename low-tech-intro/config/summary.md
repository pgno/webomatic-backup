---
title: Numérique éthique, en route vers les low-tech - Livret introduction
licence : CC-BY-SA
author : Centres Sociaux Connectés
cover: https://hot-objects.liiib.re/pad-lescommuns-org/uploads/54ff4b99-ca28-4b02-8f68-1213dc19d963.png
collection : Projet de pierre
---

- [Livret 1 - Introdution](https://pad.lescommuns.org/formation-cs-low-tech-intro)